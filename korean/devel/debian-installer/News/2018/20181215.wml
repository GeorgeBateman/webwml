#use wml::debian::translation-check translation="9a091cbc67457151ac60a49ad92557120fe3f69f" maintainer="Sebul" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.
<define-tag pagetitle>데비안 설치관리자 Buster 알파 4 나옴</define-tag>
<define-tag release_date>2018-12-15</define-tag>
#use wml::debian::news

<p>
The Debian Installer <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is pleased to
announce the fourth alpha release of the installer for Debian 10
<q>Buster</q>.
</p>


<h2>서문</h2>

<p>Cyril Brulebois would like to start by thanking Christian Perrier,
who spent many years working on Debian Installer, especially on
internationalization (i18n) and localization (l10n) topics. One might
remember graphs and blog posts on Planet Debian with statistics;
keeping track of those numbers could look like a pure mathematical
topic, but having uptodate translations is a key part of having a
Debian Installer that is accessible for most users.</p>

<p>Thank you so much, Christian!</p>

<h2>이 릴리스 개선</h2>

<ul>
  <li>choose-mirror:
    <ul>
      <li>Set deb.debian.org as default HTTP mirror (<a href="https://bugs.debian.org/797340">#797340</a>).</li>
    </ul>
  </li>
  <li>debian-archive-keyring-udeb:
    <ul>
      <li>Remove wheezy keys (<a href="https://bugs.debian.org/901320">#901320</a>).</li>
      <li>Start shipping separate keyrings for each release
        (<a href="https://bugs.debian.org/861695">#861695</a>).</li>
    </ul>
  </li>
  <li>debian-installer:
    <ul>
      <li>Bump Linux kernel ABI from 4.16.0-2 to 4.18.0-3.</li>
      <li>Make netboot.tar.gz archive structure for armhf identical to
        all other architectures (<a href="https://bugs.debian.org/902020">#902020</a>).</li>
      <li>Replace ttf-freefont-udeb with fonts-freefont-udeb.</li>
      <li>Keep grub resolution in EFI boot, to avoid tiny fonts
        (<a href="https://bugs.debian.org/910227">#910227</a>).</li>
      <li>Fix not-working F10 key function in help pages of boot screen.</li>
    </ul>
  </li>
  <li>debootstrap:
    <ul>
      <li>Enable merged-/usr by default (<a href="https://bugs.debian.org/839046">#839046</a>), but disable it when
        preparing a buildd chroot (<a href="https://bugs.debian.org/914208">#914208</a>).</li>
      <li>Many other improvements/fixes, see changelog for the
        details.</li>
    </ul>
  </li>
  <li>fonts-thai-tlwg-udeb:
    <ul>
      <li>Ship OTF fonts instead of TTF ones.</li>
    </ul>
  </li>
  <li>partman-auto-lvm:
    <ul>
      <li>Add ability to limit space used within LVM VG (<a href="https://bugs.debian.org/515607">#515607</a>,
        <a href="https://bugs.debian.org/904184">#904184</a>).</li>
    </ul>
  </li>
  <li>partman-crypto:
    <ul>
      <li>Set discard option on LUKS containers (<a href="https://bugs.debian.org/902912">#902912</a>).</li>
    </ul>
  </li>
  <li>pkgsel:
    <ul>
      <li>No longer install unattended-upgrades by
        default <a href="https://lists.debian.org/debian-boot/2018/05/msg00250.html">as
          requested by the Debian security team</a>.</li>
      <li>Install new dependencies when safe-upgrade (default) is
        selected (<a href="https://bugs.debian.org/908711">#908711</a>). In particular, this ensures the latest
        linux-image package is installed.</li>
      <li>Allow update-initramfs to run normally during package
        upgrade and installation (<a href="https://bugs.debian.org/912073">#912073</a>).</li>
    </ul>
  </li>
  <li>preseed:
    <ul>
      <li>Mark 'Checksum error' strings as translatable.</li>
    </ul>
  </li>
</ul>


<h2>하드웨어 지원 바뀜</h2>

<ul>
  <li>debian-installer:
    <ul>
      <li>[armel] Disable OpenRD targets, no longer present in
        u-boot.</li>
      <li>[armhf] Remove Firefly-RK3288 image, as u-boot fails to boot
        due to <a href="https://bugs.debian.org/898520">#898520</a>.</li>
      <li>[armhf] Add image for Sinovoip_BPI_M3.</li>
      <li>[arm64] Add u-boot image for pinebook.</li>
      <li>[mips*el/loongson-3] add input modules to netboot image
        (<a href="https://bugs.debian.org/911664">#911664</a>).</li>
    </ul>
  </li>
  <li>flash-kernel:
    <ul>
      <li>Add a machine db entry for Helios-4 NAS system
        (<a href="https://bugs.debian.org/914016">#914016</a>).</li>
      <li>Add a machine db entry for the Rockchip RK3288 Tinker Board
        (<a href="https://bugs.debian.org/895934">#895934</a>).</li>
      <li>Update Firefly-RK3399 Board (<a href="https://bugs.debian.org/899091">#899091</a>).</li>
      <li>Add Rockchip RK3399 Evaluation Board (<a href="https://bugs.debian.org/899090">#899090</a>).</li>
      <li>Update entry for Marvell 8040 MACCHIATOBin (<a href="https://bugs.debian.org/899092">#899092</a>).</li>
      <li>Update Pine64+ (<a href="https://bugs.debian.org/899093">#899093</a>).</li>
      <li>Update Raspberry Pi 3 Model B (<a href="https://bugs.debian.org/899096">#899096</a>).</li>
      <li>Add entry for Raspberry PI 3 B+ (<a href="https://bugs.debian.org/905002">#905002</a>).</li>
      <li>Clearfog Pro: correct DTB name (<a href="https://bugs.debian.org/902432">#902432</a>).</li>
      <li>Add missing entries for HummingBoard variants (<a href="https://bugs.debian.org/905962">#905962</a>).</li>
      <li>Add entries for additional Cubox-i models: SolidRun Cubox-i
        Dual/Quad (1.5som), and SolidRun Cubox-i Dual/Quad
        (1.5som+emmc).</li>
    </ul>
  </li>
  <li>linux:
    <ul>
      <li>udeb: Add virtio_console to virtio-modules (<a href="https://bugs.debian.org/903122">#903122</a>).</li>
    </ul>
  </li>
  <li>parted:
    <ul>
      <li>Fix reading NVMe model names from sysfs (<a href="https://bugs.debian.org/911273">#911273</a>).</li>
    </ul>
  </li>
  <li>u-boot:
    <ul>
      <li>[armhf] u-boot-rockchip: Drop firefly-rk3288 target
        (<a href="https://bugs.debian.org/898520">#898520</a>).</li>
      <li>[arm64] u-boot-sunxi: Enable a64-olinuxino target
        (<a href="https://bugs.debian.org/881564">#881564</a>).</li>
      <li>[arm64] u-boot-sunxi: Add pinebook target.</li>
      <li>[armel] Drop openrd targets (no longer supported
        upstream).</li>
      <li>[armhf] u-boot-sunxi: Enable Sinovoip Banana Pi M3
        (<a href="https://bugs.debian.org/905922">#905922</a>).</li>
      <li>u-boot-imx: Remove mx6cuboxi4x4 target, as ram is now
        properly detected with mx6cuboxi.</li>
      <li>[armhf] u-boot-sunxi: Enable A20-OLinuXino-Lime2-eMMC
        (<a href="https://bugs.debian.org/901666">#901666</a>).</li>
    </ul>
  </li>
</ul>


<h2>지역화 상태</h2>

<ul>
  <li>76 languages are supported in this release.<br />
    Note: English wasn't counted in previous annoncements.
  </li>
  <li>Full translation for 25 of them.<br />
    Great job by Holger Wansing, our new translation coordinator, and
    by all involved translators!
  </li>
</ul>


<h2>알려진 이슈</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>피드백</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>고맙습니다</h2>

<p>
The Debian Installer team thanks everybody who has contributed to this
release.
</p>
