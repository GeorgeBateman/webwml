#use wml::debian::template title="어떻게 www.debian.org이 만들어졌는가"
#use wml::debian::toc
#use wml::debian::translation-check translation="6d9ec7201dcfa229e0cdf7f93c34b3d4a8509ebe" maintainer="Seunghun Han (kkamagui)"

<p>우리 웹 사이트를 구성하는 디렉터리와 파일의 집합인
데비안 <em>&ldquo;웹트리&rdquo;</em>는
www-master.debian.org의 <tt>/org/www.debian.org/www</tt> 디렉터리 안에 있습니다.
대부분의 페이지는 CGI 또는 PHP 같은 동적 스크립트로 만들어지지 않는 일반 정적
HTML 파일인데 왜냐면 웹 사이트가 미러되기 때문입니다.

<p>사이트는 셋 중 한 가지 방법으로 만들어집니다:
<ul>
  <li><a href="$(DEVEL)/website/using_git"><q>webwml</q> Git 저장소</a>에 있는
      WML을 사용하여 생성되는 대부분
  <li><a href="$(DOC)/vcs"><q>ddp</q> Git 저장소</a>에 있는 DocBook XML을
      사용(DebianDoc SGML는 사라지고 있음)하여 생성되거나
      <a href="#scripts">cron scripts</a>와 관련된 데비안 패키지를 사용하여
      생성되는 문서
  <li>다른 소스, 예를 들어 메일링 리스트 구독 페이지 및 구독 해제 페이지 같은
      페이지를 이용하여 생성되는 사이트 일부분
</ul>

<p>자동 업데이트는 (git 저장소와 다른 소스에서부터 웹트리까지) 하루에 여섯 번 돕니다.

<p>사이트에 기여하고 싶다면, 단순히 <code>www/</code>에 항목을 더하거나 편집하지
<strong>마세요</strong>.
<a href="mailto:webmaster@debian.org">웹 마스터</a>에게 먼저 연락하세요.

<p>모든 파일과 디렉터리는 debwww 그룹에 속하며 해당 그룹이 쓸 수 있으므로,
웹 팀은 웹 디렉터리 안의 파일을 변경할 수 있습니다.
디렉터리에서 2775 모드는 해당 디렉터리(여기서는 debwww 디렉터리) 아래에서
생성되는 어떠한 파일도 그룹(여기서는 debwww)을 상속 받는다는 것을 의미합니다.
debwww 그룹 안의 멤버는 누구나 '<code>umask 002</code>'를 설정할 것이므로,
파일들은 그룹 쓰기 권한으로 만들어집니다.

<toc-display />

<hrline />


<toc-add-entry name="look">룩 &amp; 필</toc-add-entry>

<p><a href="https://packages.debian.org/unstable/web/wml">WML</a>이
페이지에 머리말과 꼬리말을 더하는 세부작업을 함으로써, 우리는 같은 모양과 느낌,
즉 일관되게 페이지를 생성할 수 있습니다.
처음 보면 wml 페이지가 HTML처럼 보일지라도, HTML은 .wml에 사용될 수 있는 추가
정보의 한 유형일 뿐입니다.
파일에 다양한 필터를 돌려서 끝나야 최종 결과물인 진짜 HTML이 됩니다.
여러분에게 WML의 힘을 설명하자면 ,
여러분이 어떤 것이든 멋지게 할 수 있게 해주는 Perl 코드를 페이지에 포함시킬 수
있다는 것이죠.

<p>그러나 WML은 여러분의 HTML 코드의 기본적인 유효성만 점검하고, 가끔 자동 수정하니
주의하세요.
<a href="https://packages.debian.org/unstable/web/weblint">weblint</a>
그리고 혹은 또는
<a href="https://packages.debian.org/unstable/web/tidy">tidy</a>를 설치해서
여러분의 HTML 코드 유효성을 검사할 수도 있습니다.

<p>우리의 웹 페이지는 현재 <a href="http://www.w3.org/TR/html4/">HTML 4.01
Strict</a> 표준을 따릅니다.

<p>많은 페이지를 작업하고 있는 분이라면 결과가 원하는대로 나오는지 테스트할 수
있도록 WML을 설치하는 것이 좋습니다.
데비안을 실행 중이면 쉽게 <code>wml</code> 패키지를 설치할 수 있죠.
더 많은 정보를 얻으려면 <a href="using_wml">WML 사용하기</a> 페이지를 읽으세요.


<toc-add-entry name="sources">소스</toc-add-entry>

<p>웹 페이지의 소스는 Git에 저장됩니다. Git은 버전 관리 시스템이며 우리가 어떤 것을,
누가, 언제, 왜 변경했는지에 대한 로그를 남길 수 있게 해줍니다.
이는 다양한 저자들 사이에서 현재 수정 중인 소스 파일을 제어하는 안전한 방법이며,
데비안 웹 팀은 크기 때문에 우리에게 아주 중요합니다.

<p>Git 프로그램이 낯설다면, <a href="using_git">git 사용하기</a>를 읽어보고 싶을
겁니다.

<p>Git 저장소의 최상위 webwml 디렉터리에는 웹 사이트에 있는 언어명을 딴 디렉터리,
Makefile 2개, 여러 스크립트가 있습니다. 번역 디렉터리 이름은 영어 소문자입니다.
(예 korean, Korea 아닙니다)

<p>2개 Makefile 중 더 중요한 건 Makefile.common이며, 이름이 말하는 것처럼
이 파일에는 다른 makefile들에 포함하여 적용되는 공통 규칙이 들어 있습니다.

<p>각 언어 디렉터리에는 makefiles, WML source 파일, 서브디렉터리가 있습니다.
파일과 디렉터리 이름은 모든 언어에 대해 웹 사이트 링크가 정상적으로 동작해야
하므로 다르면 안 됩니다.
디렉터리는 .wmlrc 파일을 포함할 수도 있는데, 이 파일에는 WML에 유용한 정보가
들어 있습니다.

<p>webwml/english/template 디렉터리에는 특별한 WML 파일이 들어있고
우리는 그것을 template이라 부릅니다. 왜냐면 <code>#use</code> 명령을 사용하는
모든 다른 파일이 참조할 수 있기 때문입니다.

<p> 템플릿의 변경이 템플릿을 사용하는 다른 파일로 전파될 수 있도록, 다른 파일은
템플릿 파일에 makefile 의존성이 걸려있습니다.
파일의 대다수는 가장 윗 줄에 "<code>#use wml::debian::template</code>"와 같이
"template"을 사용하기 때문에, 일반적으로 바로 그 템플릿에 의존하고 있습니다.
즉, 모두를 위한 하나인 것이죠. 물론 이 법칙에 예외도 있습니다.

<toc-add-entry name="scripts">스크립트</toc-add-entry>

<p> 스크립트는 대부분 쉘(shell)이나 펄(Perl)로 작성되어 있습니다.
일부는 독립적으로 동작을 하고, 일부는 WML 소스 파일에 통합되기도 하죠. </p>

<p>메인 www-master를 다시 빌드하는 스크립트의 소스는
<a href="https://salsa.debian.org/webmaster-team/cron.git">debwww/cron Git
저장소</a>에 있습니다.
</p>

<p>packages.debian.org를 다시 빌드하는 스크립트의 소스는
<a href="https://salsa.debian.org/webmaster-team/packages">webwml/packages Git
저장소</a>에 있습니다.
</p>


<toc-add-entry name="help">돕는 방법</toc-add-entry>

<p> 우리는 데비안 사이트를 훌륭하게 만드는 것을 돕는데 관심이 있는 모든 사람을
초대합니다.
데비안 페이지가 놓치고 있는 것과 관련된 가치있는 정보가 여러분에게 있다면,
<a href="mailto:debian-www@lists.debian.org">우리에게 공유</a>해 주세요.
우리가 포함시킬 겁니다.

<p> 우리는 페이지 디자인에 그래픽 및 배치와 관련된 유용한 도구를
항상 활용합니다. HTML을 깨끗하게 유지하는데도 역시 사용합니다. 우리는 정기적으로
모든 웹 사이트에 대해서 아래 검사를 수행하거든요:</p>

<ul>
  <li><a href="https://www-master.debian.org/build-logs/urlcheck/">URL check</a>
  <li><a href="https://www-master.debian.org/build-logs/validate/">wdg-html-validator</a>
  <li><a href="https://www-master.debian.org/build-logs/tidy/">tidy</a>
</ul>

<p>위 로그를 읽고 문제를 고치는 기여는 언제나 환영합니다.</p>

<p>현재 웹 사이트의 빌드 로그는
<url "https://www-master.debian.org/build-logs/">에 있습니다.</p>

<p>여러분의 영어가 유창하다면, 우리의 페이지를 교정하고 모든 오류를
<a href="mailto:debian-www@lists.debian.org">데비안 웹 메일링리스트</a>에 알려 주세요.

<p>다른 언어를 쓴다면, 페이지를 여러분의 언어로 번역하여 우리를 도울 수 있습니다.
이미 번역되었지만 문제가 있다면, <a href="translation_coordinators">\
번역 코디네이터</a> 목록을 보세요. 그리고 여러분의 언어의
대표에게 그것을 고치는 것에 대해 얘기하세요.
여러분이 직접 번역하고 싶다면, 더 많은 정보를 위해
<a href="translating">관련 주제</a> 페이지를 보세요.

<p><a href="todo">TODO 파일</a>도 있으니 확인하세요.</p>

<toc-add-entry name="nohelp">도움 <strong>안 되는</strong> 방법</toc-add-entry>

<p><em>[질문] <var>멋진 웹 기능</var>을 www.debian.org 안에 넣고 싶은데,
해도 되나요?</em>

<p>[답변] 아뇨. 우리는 www.debian.org이 되도록 모든 브라우저에서 접근 가능하길
바랍니다. 그래서
<ul>
    <li>특정 브라우저의 "확장 기능" 사용 안 합니다.
    <li>오직 이미지에만 의존하면 안 됩니다. 이미지는 명확히 하는 데 쓸 수는 있지만,
    www.debian.org 의 정보는 lynx 같은 텍스트전용 브라우저를 통해서도 접근할
    수 있어야 합니다.
</ul>

<p><em>[질문] 좋은 생각이 있습니다. 어떤 기능을 www.debian.org의 HTTPD에서 켤
수 있나요?</em>

<p>[답변] 아뇨.
우리는 관리자들이 www.debian.org를 쉽게 미러하기를 바라기 때문에 특별한
HTTPD 기능이 없습니다.
아니, SSI조차 사용하지 않습니다.
단, 내용 협상은 예외입니다.
왜냐면, 내용 협상은 여러 언어를 지원하는 유일하고 강력한 방법이기 때문입니다.
