#use wml::debian::template title="문서"
#use wml::debian::translation-check translation="9aa7cfecde4026c09b63f9d42fa74cbe0f7e50bb" maintainer="Sebul"

<p>운영체제의 중요한 부분 중 하나는 문서, 프로그램의 사용과 운영을
설명하는 기술 매뉴얼입니다. 고품질 자유 운영체제를 만들기 위한 노력의 일환으로
데비안 프로젝트는 모든 사용자에게 적절한 문서를 쉽게 접근할 수 있는 형태로
제공하기 위해 노력합니다.</p>

<h2>빠른 시작</h2>

<p>데비안을 <em>처음</em> 사용한다면 다음 두 문서를 읽으면서 시작하기를
권합니다:</p>
<ul>
  <li><a href="$(HOME)/releases/stable/installmanual">설치 안내</a></li>
  <li><a href="manuals/debian-faq/">데비안 GNU/리눅스 FAQ</a></li>
</ul>

<p>데비안을 설치한다면 이 문서들을 가까이에 두세요. 많은 질문에 답이 될
것이고 새 데비안 시스템으로 작업하는 데 도움이 될 것입니다. 나중에는
다음 문서들을 읽으면 좋을 것입니다:</p>

<ul>
<li><a href="manuals/debian-handbook/">데비안 관리자 Handbook</a>, 포괄적인 사용자 매뉴얼</li>
<li><a href="manuals/debian-reference/">데비안 참조</a></li>
<li>업그레이드하려는 사람들은 <a href="$(HOME)/releases/stable/releasenotes">릴리스 노트</a>를 보세요.</li>
<li><a href="https://wiki.debian.org/">데비안 위키</a>는 초보자를 위한 좋은 정보원입니다.</li>
</ul>

<p>마지막으로 데비안 시스템의 가장 중요한 명령 목록인 <a
href="https://www.debian.org/doc/manuals/refcard/refcard">데비안 GNU/리눅스 참조카드</a>를 
인쇄해 가까운 곳에 두세요.</p>

<p>다른 문서들은 아래에 열거되어 있습니다.</p>

<h2>문서의 종류</h2>

<p>데비안에 포함된 대부분의 문서는 일반적으로 GNU/리눅스를 위해 작성되었습니다.
특별히 데비안을 위해 작성된 문서들도 있습니다. 
이 문서는 다음과 같이 기본 범주로 제공됩니다:</p>

<ul>
  <li><a href="#manuals">매뉴얼</a></li>
  <li><a href="#howtos">HOWTO</a></li>
  <li><a href="#faqs">FAQs</a></li>
  <li><a href="#other">다른 짧은 문서들</a></li>
</ul>

<h3 id="manuals">매뉴얼</h3>

<p>매뉴얼들은 책과 비슷한데, 왜냐면 주요한 주제를 종합적으로 설명하기 때문입니다.</p>

<h3>데비안 매뉴얼</h3>

<div class="line">
  <div class="item col50">
    <h4><a href="user-manuals">사용자 매뉴얼</a></h4>    
    <ul>
      <li><a href="user-manuals#faq">데비안 GNU/리눅스 FAQ</a></li>
      <li><a href="user-manuals#install">데비안 설치 안내</a></li>
      <li><a href="user-manuals#relnotes">데비안 릴리스 노트</a></li>
      <li><a href="user-manuals#refcard">Debian Reference Card</a></li>
      <li><a href="user-manuals#debian-handbook">데비안 관리자 Handbook</a></li>
      <li><a href="user-manuals#quick-reference">데비안 참고자료</a></li>     
      <li><a href="user-manuals#securing">Securing Debian Manual</a></li>
	  <li><a href="user-manuals#aptitude">aptitude user's manual</a></li>
<li><a href="user-manuals#apt-guide">APT User's Guide</a></li>
      <li><a href="user-manuals#apt-offline">Using APT Offline</a></li>
	  <li><a href="user-manuals#java-faq">데비안 GNU/리눅스와 자바 FAQ</a></li>
<li><a href="user-manuals#hamradio-maintguide">Debian Hamradio Maintainer’s Guide</a></li>
    </ul>

  </div>

  <div class="item col50 lastcol">
  
    <h4><a href="devel-manuals">개발자 매뉴얼</a></h4>  
    <ul>
      <li><a href="devel-manuals#policy">데비안 정책 매뉴얼</a></li>
      <li><a href="devel-manuals#devref">데비안 개발자 참고자료</a></li>
	  <li><a href="devel-manuals#debmake-doc">Guide for Debian Maintainers</a></li>
      <li><a href="devel-manuals#maint-guide">Debian New Maintainers' Guide</a></li>
	  <li><a href="devel-manuals#packaging-tutorial">Introduction to Debian packaging</a></li>
      <li><a href="devel-manuals#menu">데비안 메뉴 시스템</a></li>
	  <li><a href="devel-manuals#d-i-internals">Debian Installer internals</a></li>
      <li><a href="devel-manuals#dbconfig-common">Guide for database using package maintainers</a></li>
      <li><a href="devel-manuals#dbapp-policy">Policy for packages using databases</a></li>
    </ul>
    
    <h4><a href="misc-manuals">기타 매뉴얼</a></h4>	
    <ul>
      <li><a href="misc-manuals#history">데비안 프로젝트 역사</a></li>
    </ul>

  </div>


</div>

<p class="clr">데비안 매뉴얼와 다른 문서의 전체 목록은 <a href="ddp">데비안 문서
프로젝트</a> 웹 페이지에서 찾을 수 있습니다.</p>

<p>사용자 중심의 데비안 GNU/리눅스 매뉴얼들도 있으며,
 <a href="books">출판된 책</a>으로 구할 수 있습니다.</p>

<h3 id="howtos">HOWTO</h3>

<p>The <a href="https://www.tldp.org/HOWTO/HOWTO-INDEX/categories.html">HOWTO
문서</a>는 그 이름처럼 어떤 일을 하는 <em>방법</em>을 설명하고
대개 좀 더 특화된 주제를 다룹니다.</p>

<h3 id="faqs">FAQ</h3>

<p>FAQ는 <em>자주 묻는 질문(frequently asked questions)</em>을
뜻합니다. FAQ는 그러한 질문에 대한 대답을 정리한 문서이기도 합니다.</p>

<p><a href="http://www.tldp.org/FAQ/Linux-FAQ/">Linux FAQ</a>는
리눅스와 관련된 일반적인 정보를 담고 있습니다.</p>

<p>데비안에 관련된 질문은 
<a href="manuals/debian-faq/">데비안 FAQ</a>에서 답을 구할 수 있습니다. 별도의 <a href="../CD/faq/">데비안 CD/DVD 이미지에 대한 FAQ</a>도 있습니다.</p>


<h3 id="other">다른 짧은 문서들</h3>

<p>다음 문서는 더 빠르고 더 짧은 지침을 포함합니다:</p>

<dl>

  <dt><strong><a href="http://www.tldp.org/docs.html#man">manual pages</a></strong></dt>
    <dd>전통적으로 모든 유닉스 프로그램은 <em>manual pages</em>로
문서화됩니다. <em>manual page</em>는 <tt>man</tt> 명령으로 볼 수 있는
참고 매뉴얼입니다. 대개 초보자에게는 적합하지 않습니다. 데비안에서
    사용할 수 있는 매뉴얼 페이지는
 <a href="https://manpages.debian.org/cgi-bin/man.cgi">https://manpages.debian.org/</a>에서
    검색하고 읽을 수 있습니다.
    </dd>

  <dt><strong><a href="https://www.gnu.org/software/texinfo/manual/texinfo/html_node/index.html">info files</a></strong></dt>
    <dd>많은 GNU 소프트웨어가 매뉴얼 페이지 대신 <em>info 
        files</em>로 문서화됩니다. 이 파일들은 프로그램 자체에 대한
    상세한 정보와 옵션, 사용 예제를 제공합니다. 
 <tt>info</tt> 명령으로 사용할 수 있습니다.
    </dd>

  <dt><strong>다양한 README 파일</strong></dt>
    <dd><em>read me</em> 파일도 보편적입니다. 이 파일들은 단순한
텍스트 파일로 대개 패키지 같은 단일 항목을 설명합니다. 
자신의 데비안 시스템의 <tt>/usr/share/doc/</tt> 서브디렉터리에서 많은
README 파일을 발견할 수 있습니다. 각각의 소프트웨어 패키지에는 자신의 read
    me 파일이 들어있는 서브디렉터리가 있습니다. 그리고 설정 예제도
    제공합니다. 더 큰 프로그램들의 경우 문서는 대개 별도의 패키지로
    제공됩니다(원 패키지와 이름이 같지만 
 <em>-doc</em>으로 끝납니다).
    </dd>
  <dt><strong>quick reference cards</strong></dt>
    <dd>
        <p>Quick reference cards는 어떤 시스템의 매우 짧은 요약입니다.
대개 그런 카드는 가장 많이 쓰이는 명령을 종이 한 장에 제공합니다.
몇 가지 주목할 만한 레퍼런스 카드와 모음은 다음과 같습니다.</p>
        <dl>
          <dt><a href="https://www.debian.org/doc/manuals/refcard/refcard">Debian
              GNU/Linux Reference Card</a></dt>
	    <dd>종이 한 장에 인쇄할 수 있는 이 카드에서는 가장 중요한
    명령 목록을 제공하고 데비안에 익숙해지기 바라는 데비안 초보자에게
    좋은 참고 자료입니다. 최소한 컴퓨터, 파일, 디렉터리, 명령행에 대한
    기본 지식이 필요합니다. 초보 사용자는 
		<a href="user-manuals#quick-reference">Debian Reference</a>를 
		먼저 읽는 것이 좋습니다.</dd>
	</dl>
    </dd>

</dl>

<hrline />

<p>앞에서 언급한 참고 자료에서도 데비안과 관련된 문제에 대한 해답이나
해결 방법을 찾을 수 없다면 <a href="../support">지원 페이지</a>를
보세요.</p>
