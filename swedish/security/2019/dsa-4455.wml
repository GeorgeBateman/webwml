#use wml::debian::translation-check translation="1cd443bbff5c5f8e18b9961d25d41c997d4a4103" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i Heimdal, en implementation av
Kerberos 5 med målet att vara kompatibel med MIT Kerberos.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16860">CVE-2018-16860</a>

	<p>Isaac Boukris och Andrew Bartlett upptäckte att Heimdal var
	sårbar för Man-in-the-middle-angrepp orsakade av otillräcklig
	validering av checksummor. Detaljer om detta problem kan hittas i
	Samba-bulletinen på <a href="https://www.samba.org/samba/security/CVE-2018-16860.html">\
    https://www.samba.org/samba/security/CVE-2018-16860.html</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12098">CVE-2019-12098</a>

	<p>Man har upptäckt att misslyckande att verifiera PA-PKINIT-KX-nyckelutbyte
	på klientsidan kunde tillåta att utföra ett man-in-the-middle-angrepp.</p></li>

</ul>

<p>För den stabila utgåvan (Stretch) har dessa problem rättats i
version 7.1.0+dfsg-13+deb9u3.</p>

<p>Vi rekommenderar att ni uppgraderar era heimdal-paket.</p>

<p>För detaljerad säkerhetsstatus om heimdal vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/heimdal">\
https://security-tracker.debian.org/tracker/heimdal</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4455.data"
