#use wml::debian::template title="Vår filosofi: varför vi gör det och hur vi gör det"
#include "$(ENGLISHDIR)/releases/info"

# translators: some text is taken from /intro/about.wml

<ul class="toc">
<li><a href="#what">Vad är Debian?</a>
<li><a href="#free">Allt detta är fritt?</a>
<li><a href="#how">Hur jobbar gemenskapen som ett projekt?</a>
<li><a href="#history">Hur startade alltihop?</a>
</ul>

<h2><a name="what">Vad är Debian?</a></h2>

<p>The <a href="$(HOME)/">Debianprojektet</a> är en samling av
individer som har ett gemensamt mål att skapa ett <a href="free">gratis</a>
operativsystem. Detta operativsystem som vi har skapat är kallat
<strong>Debian</strong>.</p>

<p>Ett operativsystem är en uppsättning grundläggande program och verktyg som gör
att din dator kan köras.
Hjärtat i ett operativsystem är kärnan.
Kärnan är det mest grundläggande programmet på datorn och förvaltar
systemresurser och låter dig starta andra program.</p>

<p>Debiansystemet använder för närvarande <a href="https://www.kernel.org/">Linuxkärnan</a>
eller kärnan från <a href="https://www.freebsd.org/">FreeBSD</a>. Linux är en
mjukvara som startades av
<a href="https://en.wikipedia.org/wiki/Linus_Torvalds">Linus Torvalds</a>
och jobbas på av tusen av programmerare världen över.
FreeBSD är ett operativsystem som inkluderar en kärna samt annan programvara.</p>

<p>Arbetet pågår dock för att tillhandahålla Debian för andra kärnor,
primärt för
<a href="https://www.gnu.org/software/hurd/hurd.html">Hurd</a>.
Hurd är en samling av servrar som körs ovanpå en mikrokärna (som t.e.x
Mach) för att implementera olika funktioner. Hurd är fri programvara utvecklad av
<a href="https://www.gnu.org/">GNU-projektet</a>.</p>

<p>En stor del av de grundläggande verktygen som fyller operativsystemet kommer
från <a href="https://www.gnu.org/">GNU-projektet</a>; därav namnen:
GNU/Linux, GNU/kFreeBSD, och GNU/Hurd.
Dessa verktyg är också gratis.</p>

<p>Naturligtvis är det folk som vill ha tillämpningsprogram: program
som hjälper dom att göra det dom behöver göra, från redigering av dokument till
att driva ett företag, att spela spel eller för att skriva mer programvara. Debian kommer
med över <packages_in_stable> <a href="$(DISTRIB)/packages">paket</a> (förkompilerad
programvara som är färdigpaketerat i ett trevligt format för enkel installation på din
maskin), en pakethanterare (APT), och andra verktyg för att göra det möjligt
att hantera tusentals paket på tusentals datorer precis lika enkelt
som att bara installera ett enda program. Och allt detta är <a href="free">fritt</a>.
</p>

<p>Det är ungefär som ett höghus: Längst ned finns kärnan, ovanpå finns alla
grundläggande verktyg. Högst upp i höghuset finns Debian, sysselsatta med att
omsorgsfullt organisera och anpassa så att allting fungerar tillsammans.</p>

<h2>Allt detta är <a href="free" name="free">fritt?</a></h2>

<p>När vi använder ordet "fritt", så hänvisar vi till mjukvaru-<strong>frihet</strong>.
Du kan läsa på
<a href="free">Vad vi menar med "fri programvara"</a> och
<a href="https://www.gnu.org/philosophy/free-sw">vad Free Software
Foundation säger</a> om det ämnet.

<p>Du kanske undrar: varför skulle folk spendera timmar av deras egen tid på att skriva
programvara, omsorgsfullt paketera den, och sen <EM>ge</EM> bort allting?
Svaren är lika varierade som de som bidrar.
Vissa personer gillar att hjälpa andra,
många skriver program för att lära sig mer om datorer, och allt fler letar efter
vägar och alternativ för att undvika de uppdrivna priserna på
mjukvara.
En allt mer växande publik bidrar som ett tack för all bra fri programvara som dom
har fått från andra.
Många i skolvärlden skapar fri programvara för att hjälpa att nå ut med resultatet av
deras forskning till en bredare publik och för att öka spridning av deras forskning.
Företag hjälper att underhålla fri programvara så att de kan få ta del av hur den utvecklas --
det finns inget snabbare sätt att få  en ny funktion implementerad än att utveckla den själv!
Självklart, tycker många av oss att det är riktigt roligt också.</p>

<p>Debian har fattat en så fast ståndpunkt angående fri programvara att vi
tyckte att det skulle vara användbart om den
ståndpunkten var formaliserat i ett skriftligt dokument. Således föddes vårat
<a href="$(HOME)/social_contract">Sociala kontrakt</a></p>

<p>Fastän Debian tror på fri mjukvara, så finns det tillfällen då folk vill ha eller behöver installera
icke-fri programvara på deras maskiner. När det är möjligt så kommer Debian att stödja detta.
Det finns till och med ett växande antal av paket vars enda jobb är att installera ickefri programvara
i ett Debian system.</p>

<h2><a name="how">Hur jobbar gemenskapen som ett projekt?</a></h2>

<p>Debian är skapat av nästan tusen aktiva
utvecklare spridda
<a href="$(DEVEL)/developers.loc">över hela världen</a> vilka frivilligt
hjälper till på sin fritid.
Bara några av utvecklarna har faktiskt träffats i verkligheten.
Kommunikation sköts primärt genom e-post (sändlistor på
lists.debian.org) och IRC (#debian kanalen på irc.debian.org).
</p>

<p>Debian Projektet har en noggrant <a href="organization">organiserad
struktur</a>. För att få mer information om hur Debian ser ut från insidan,
så kan du surfa till <a href="$(DEVEL)/">Utvecklarnas' hörna</a>.</p>

<p>
De viktigaste dokumenten som förklarar hur gemenskapen fungerar är följande:
<ul>
<li><a href="$(DEVEL)/constitution">Debians konstitution</li>
<li><a href="../social_contract">Det sociala kontraktet och riktlinjerna för fri programvara</li>
<li><a href="diversity">Mångfaldsuttalandet</li>
<li><a href="../code_of_conduct">Debians uppförandekod</li>
<li><a href="../doc/developers-reference/">Utvecklarreferensen</li>
<li><a href="../doc/debian-policy/">Debian Policy</li>
</ul>
</p>

<h2><a name="history">Hur startade alltihop?</a></h2>

<p>Debian startade i Augusti 1993 av Ian Murdock, som en ny distribution
som skulle göras öppet tillgänglig, i samma samma anda som Linux och GNU. Debian var menat
att vara noggrant och samvetsgrant uppbyggt, och att upprätthålla och
stödjas med samma tanke. Dom började som en liten, tätt sammansatt grupp av
"Fri programvaru"-hackare, och växte gradvis till att bli en stor, välorganiserad
gemenskap av utvecklare och användare. Se
<a href="$(DOC)/manuals/project-history/">den detaljerade historiken</a>.</p>

<p>Eftersom många människor har frågat, Debian uttalas /&#712;de.bi.&#601;n/. Det
kommer från namnet på skaparen av Debian, Ian Murdock, och hans fru,
Debra.</p>
