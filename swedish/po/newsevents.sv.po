# translation of newsevents.po to Swedish
#
# Martin Ågren <martin.agren@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: newsevents\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: (null)\n"
"PO-Revision-Date: 2020-12-14 19:52+0100\n"
"Last-Translator: Andreas Rönnquist <andreas@ronnquist.net>\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../english/News/news.rdf.in:16
msgid "Debian News"
msgstr "Debiannyheter"

#: ../../english/News/news.rdf.in:19
msgid "Debian Latest News"
msgstr "Debians Senaste Nytt"

#: ../../english/News/press/press.tags:11
msgid "p<get-var page />"
msgstr "s.<get-var page />"

#: ../../english/News/weekly/dwn-to-rdf.pl:143
msgid "The newsletter for the Debian community"
msgstr "Nyhetsbrevet för Debiangemenskapen"

#: ../../english/events/talks.defs:9
msgid "Title:"
msgstr "Titel:"

#: ../../english/events/talks.defs:12
msgid "Author:"
msgstr "Författare:"

#: ../../english/events/talks.defs:15
msgid "Language:"
msgstr "Språk:"

#: ../../english/events/talks.defs:19
msgid "Date:"
msgstr "Datum:"

#: ../../english/events/talks.defs:23
msgid "Event:"
msgstr "Händelse:"

#: ../../english/events/talks.defs:26
msgid "Slides:"
msgstr "Presentationsbilder:"

#: ../../english/events/talks.defs:29
msgid "source"
msgstr "källkod"

#: ../../english/events/talks.defs:32
msgid "PDF"
msgstr "PDF"

#: ../../english/events/talks.defs:35
msgid "HTML"
msgstr "HTML"

#: ../../english/events/talks.defs:38
msgid "MagicPoint"
msgstr "MagicPoint"

#: ../../english/events/talks.defs:41
msgid "Abstract"
msgstr "Sammanfattning"

#: ../../english/template/debian/events_common.wml:8
msgid "Upcoming Attractions"
msgstr "Kommande tilldragelser"

#: ../../english/template/debian/events_common.wml:11
msgid "link may no longer be valid"
msgstr "länken kan vara gammal"

#: ../../english/template/debian/events_common.wml:14
msgid "When"
msgstr "När"

#: ../../english/template/debian/events_common.wml:17
msgid "Where"
msgstr "Var"

#: ../../english/template/debian/events_common.wml:20
msgid "More Info"
msgstr "Ytterligare information"

#: ../../english/template/debian/events_common.wml:23
msgid "Debian Involvement"
msgstr "Debians involvering"

#: ../../english/template/debian/events_common.wml:26
msgid "Main Coordinator"
msgstr "Huvudsamordnare"

#: ../../english/template/debian/events_common.wml:29
msgid "<th>Project</th><th>Coordinator</th>"
msgstr "<th>Projekt</th><th>Samordnare</th>"

#: ../../english/template/debian/events_common.wml:32
msgid "Related Links"
msgstr "Relaterade länkar"

#: ../../english/template/debian/events_common.wml:35
msgid "Latest News"
msgstr "Senaste nytt"

#: ../../english/template/debian/events_common.wml:38
msgid "Download calendar entry"
msgstr "Hämta kalenderpost"

#: ../../english/template/debian/news.wml:9
msgid ""
"Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/\">Debian "
"Project homepage</a>."
msgstr ""
"Tillbaka till: andra <a href=\"./\">Debiannyheter</a> || <a href=\"m4_HOME/"
"\">Debianprojektets ingångssida</a>."

#. '<get-var url />' is replaced by the URL and must not be translated.
#. In English the final line would look like "<http://broken.com (dead.link)>"
#: ../../english/template/debian/news.wml:17
msgid "<get-var url /> (dead link)"
msgstr "<get-var url /> (död länk)"

#: ../../english/template/debian/projectnews/boilerplates.wml:35
msgid ""
"Welcome to this year's <get-var issue /> issue of DPN, the newsletter for "
"the Debian community. Topics covered in this issue include:"
msgstr ""
"Välkommen till detta års <get-var issue /> utgåva av DPN, nyhetsbrevet för "
"Debiangemenskapen. Ämnen som täcks i denna utgåva innefattar:"

#: ../../english/template/debian/projectnews/boilerplates.wml:43
msgid ""
"Welcome to this year's <get-var issue /> issue of DPN, the newsletter for "
"the Debian community."
msgstr ""
"Välkommen till detta års <get-var issue /> utgåva av DPN, nyhetsbrevet för "
"Debiangemenskapen."

#: ../../english/template/debian/projectnews/boilerplates.wml:49
msgid "Other topics covered in this issue include:"
msgstr "Andra ämnen som täcks i denna utgåva innefattar:"

#: ../../english/template/debian/projectnews/boilerplates.wml:69
#: ../../english/template/debian/projectnews/boilerplates.wml:90
msgid ""
"According to the <a href=\"https://udd.debian.org/bugs.cgi\">Bugs Search "
"interface of the Ultimate Debian Database</a>, the upcoming release, Debian  "
"<q><get-var release /></q>, is currently affected by <get-var testing /> "
"Release-Critical bugs. Ignoring bugs which are easily solved or on the way "
"to being solved, roughly speaking, about <get-var tobefixed /> Release-"
"Critical bugs remain to be solved for the release to happen."
msgstr ""
"Enligt <a href=\"https://udd.debian.org/bugs.cgi\">Felsökningsgränssnittet i "
"den Ultimata Debiandatabasen (UDD, Ultimate Debian Database)</a>, påverkas "
"för närvarande den kommande Debianutgåvan <q><get-var release /></q> av <get-"
"var testing /> utgåvekritiska fel. Om man ignorerar fel som är lätta att "
"rätta eller på väg att rättas så återstår uppskattningsvis, <get-var "
"tobefixed /> utgåvekritiska fel som måste rättas innan utgåvan kan ske."

#: ../../english/template/debian/projectnews/boilerplates.wml:70
msgid ""
"There are also some <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats"
"\">hints on how to interpret</a> these numbers."
msgstr ""
"Det finns även några <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats"
"\">tips om hur du tolkar</a> dessa siffror."

#: ../../english/template/debian/projectnews/boilerplates.wml:91
msgid ""
"There are also <a href=\"<get-var url />\">more detailed statistics</a> as "
"well as some <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats\">hints "
"on how to interpret</a> these numbers."
msgstr ""
"Det finns även <a href=\"<get-var url />\">mer detaljerad statistik</a> så "
"väl som <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats\">tips om hur "
"du tolkar</a> dessa siffror."

#: ../../english/template/debian/projectnews/boilerplates.wml:115
msgid ""
"<a href=\"<get-var link />\">Currently</a> <a href=\"m4_DEVEL/wnpp/orphaned"
"\"><get-var orphaned /> packages are orphaned</a> and <a href=\"m4_DEVEL/"
"wnpp/rfa\"><get-var rfa /> packages are up for adoption</a>: please visit "
"the complete list of <a href=\"m4_DEVEL/wnpp/help_requested\">packages which "
"need your help</a>."
msgstr ""
"<a href=\"<get-var link />\">För närvarande</a> är <a href=\"m4_DEVEL/wnpp/"
"orphaned\"><get-var orphaned /> paket övergivna </a>, och <a href=\"m4_DEVEL/"
"wnpp/rfa\"><get-var rfa /> paket är tillgängliga för adoption</a>: Vänligen "
"se den fullständiga listan på <a href=\"m4_DEVEL/wnpp/help_requested\">paket "
"som behöver din hjälp</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:127
msgid ""
"Please help us create this newsletter. We still need more volunteer writers "
"to watch the Debian community and report about what is going on. Please see "
"the <a href=\"https://wiki.debian.org/ProjectNews/HowToContribute"
"\">contributing page</a> to find out how to help. We're looking forward to "
"receiving your mail at <a href=\"mailto:debian-publicity@lists.debian.org"
"\">debian-publicity@lists.debian.org</a>."
msgstr ""
"Vänligen hjälp oss att skapa detta nyhetsbrev. Vi behöver fortfarande fler "
"frivilliga författare som övervakar Debiangemenskapen och rapporterar vad "
"som är på gång. Vänligen se <a href=\"https://wiki.debian.org/ProjectNews/"
"HowToContribute\">sidan för bidragslämnare</a> för att få reda på hur du "
"hjälper till. Vi ser fram emot att ta emot ditt mail på <a href=\"mailto:"
"debian-publicity@lists.debian.org\">debian-publicity@lists.debian.org</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:188
msgid ""
"Please note that these are a selection of the more important security "
"advisories of the last weeks. If you need to be kept up to date about "
"security advisories released by the Debian Security Team, please subscribe "
"to the <a href=\"<get-var url-dsa />\">security mailing list</a> (and the "
"separate <a href=\"<get-var url-bpo />\">backports list</a>, and <a href="
"\"<get-var url-stable-announce />\">stable updates list</a>) for "
"announcements."
msgstr ""
"Vänligen notera att detta är ett urval av de mer viktiga "
"säkerhetsbulletinerna från de senaste veckorna. Om du behöver hålla dig "
"uppdaterad rörande säkerhetsbulletiner som släpps av Debians säkerhetsgrupp, "
"vänligen prenumerera på <a href=\"<get-var url-dsa />\">säkerhetssändlistan</"
"a> (och den separata <a href=\"<get-var url-bpo />\">sändlistan för "
"bakåtanpassningar</a>, och <a href=\"<get-var url-stable-announce />"
"\">listan för stabila uppdateringarstable updates list</a>) för "
"tillkännagivanden."

#: ../../english/template/debian/projectnews/boilerplates.wml:189
msgid ""
"Please note that these are a selection of the more important security "
"advisories of the last weeks. If you need to be kept up to date about "
"security advisories released by the Debian Security Team, please subscribe "
"to the <a href=\"<get-var url-dsa />\">security mailing list</a> (and the "
"separate <a href=\"<get-var url-bpo />\">backports list</a>, and <a href="
"\"<get-var url-stable-announce />\">stable updates list</a> or <a href="
"\"<get-var url-volatile-announce />\">volatile list</a>, for <q><get-var old-"
"stable /></q>, the oldstable distribution) for announcements."
msgstr ""
"Vänligen notera att detta är ett urval av de mer viktiga "
"säkerhetsbulletinerna från de senaste veckorna. Om du behöver hålla dig mer "
"uppdaterad rörande säkerhetsbulletiner som släpps av Debians säkerhetsgrupp, "
"vänligen prenumerera på <a href=\"<get-var url-dsa />\">säkerhetssändlistan</"
"a> (och den separata <a href=\"<get-var url-bpo />\">sändlistan för "
"bakåtanpassningar</a>, och <a href=\"<get-var url-stable-announce />"
"\">sändlistan för stabila uppdateringar</a> eller <a href=\"<get-var url-"
"volatile-announce />\">volatilesändlistan</a>, för <q><get-var old-stable /"
"></q>, den gamla stabila utgåvan) för tillkännagivanden."

#: ../../english/template/debian/projectnews/boilerplates.wml:198
msgid ""
"Debian's Stable Release Team released an update announcement for the "
"package: "
msgstr ""
"Debians grupp för stabila utgåvor släppte ett tillkännagivande för paketet: "

#: ../../english/template/debian/projectnews/boilerplates.wml:200
#: ../../english/template/debian/projectnews/boilerplates.wml:213
#: ../../english/template/debian/projectnews/boilerplates.wml:226
#: ../../english/template/debian/projectnews/boilerplates.wml:357
#: ../../english/template/debian/projectnews/boilerplates.wml:371
msgid ", "
msgstr ", "

#: ../../english/template/debian/projectnews/boilerplates.wml:201
#: ../../english/template/debian/projectnews/boilerplates.wml:214
#: ../../english/template/debian/projectnews/boilerplates.wml:227
#: ../../english/template/debian/projectnews/boilerplates.wml:358
#: ../../english/template/debian/projectnews/boilerplates.wml:372
msgid " and "
msgstr " och "

#: ../../english/template/debian/projectnews/boilerplates.wml:202
#: ../../english/template/debian/projectnews/boilerplates.wml:215
#: ../../english/template/debian/projectnews/boilerplates.wml:228
msgid ". "
msgstr ". "

#: ../../english/template/debian/projectnews/boilerplates.wml:202
#: ../../english/template/debian/projectnews/boilerplates.wml:215
#: ../../english/template/debian/projectnews/boilerplates.wml:228
msgid "Please read them carefully and take the proper measures."
msgstr "Vänligen läs dessa noggrant och vidta lämpliga åtgärder."

#: ../../english/template/debian/projectnews/boilerplates.wml:211
msgid "Debian's Backports Team released advisories for these packages: "
msgstr "Debians Bakåtanpassningsgrupp släppte bulletiner för följande paket: "

#: ../../english/template/debian/projectnews/boilerplates.wml:224
msgid ""
"Debian's Security Team recently released advisories for these packages "
"(among others): "
msgstr ""
"Debians säkerhetsgrupp släppte nyligen bulletiner för (bland annat) dessa "
"paket: "

#: ../../english/template/debian/projectnews/boilerplates.wml:253
msgid ""
"<get-var num-newpkg /> packages were added to the unstable Debian archive "
"recently."
msgstr ""
"<get-var num-newpkg /> paket lades till det ostabila Debianarkivet nyligen."

#: ../../english/template/debian/projectnews/boilerplates.wml:255
msgid " <a href=\"<get-var url-newpkg />\">Among many others</a> are:"
msgstr " <a href=\"<get-var url-newpkg />\">Bland många andra</a> hittas:"

#: ../../english/template/debian/projectnews/boilerplates.wml:282
msgid "There are several upcoming Debian-related events:"
msgstr "Det finns fler kommande Debian-relaterade händelser:"

#: ../../english/template/debian/projectnews/boilerplates.wml:288
msgid ""
"You can find more information about Debian-related events and talks on the "
"<a href=\"<get-var events-section />\">events section</a> of the Debian web "
"site, or subscribe to one of our events mailing lists for different regions: "
"<a href=\"<get-var events-ml-eu />\">Europe</a>, <a href=\"<get-var events-"
"ml-nl />\">Netherlands</a>, <a href=\"<get-var events-ml-ha />\">Hispanic "
"America</a>, <a href=\"<get-var events-ml-na />\">North America</a>."
msgstr ""
"Du kan få mer information om Debianrelaterade evenemang och föreläsningar på "
"<a href=\"<get-var events-section />\">evenemangavdelning</a> av Debians "
"webbplats, eller genom att prenumerera på våran sändlista för evenemang för "
"olika regioner: <a href=\"<get-var events-ml-eu />\">Europa</a>, <a href="
"\"<get-var events-ml-nl />\">Holland</a>, <a href=\"<get-var events-ml-ha />"
"\">Latinamerika</a>, <a href=\"<get-var events-ml-na />\">Nordamerika</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:313
msgid ""
"Do you want to organise a Debian booth or a Debian install party? Are you "
"aware of other upcoming Debian-related events? Have you delivered a Debian "
"talk that you want to link on our <a href=\"<get-var events-talks />\">talks "
"page</a>? Send an email to the <a href=\"<get-var events-team />\">Debian "
"Events Team</a>."
msgstr ""
"Vill du organisera en Debianmonter eller en Installationsfest för Debian? "
"Känner du till andra kommande Debianrelaterade evenemang? Har du givit ett "
"Debianföredrag som du vill länka på våran <a href=\"<get-var events-talks />"
"\">föredragssida</a>? Skicka ett meddelande till <a href=\"<get-var events-"
"team />\">Debians Evenemangsgrupp</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:335
msgid ""
"<get-var dd-num /> applicants have been <a href=\"<get-var dd-url />"
"\">accepted</a> as Debian Developers"
msgstr ""
"<get-var dd-num /> ansökande har blivit <a href=\"<get-var dd-url />"
"\">antagna</a> som Debianutvecklare"

#: ../../english/template/debian/projectnews/boilerplates.wml:342
msgid ""
"<get-var dm-num /> applicants have been <a href=\"<get-var dm-url />"
"\">accepted</a> as Debian Maintainers"
msgstr ""
"<get-var dm-num /> ansökande har blivit <a href=\"<get-var dm-url />"
"\">antagna</a> som Debian Maintainers"

#: ../../english/template/debian/projectnews/boilerplates.wml:349
msgid ""
"<get-var uploader-num /> people have <a href=\"<get-var uploader-url />"
"\">started to maintain packages</a>"
msgstr ""
"<get-var uploader-num />personer har <a href=\"<get-var uploader-url />"
"\">börjat att underhålla paket</a>"

#: ../../english/template/debian/projectnews/boilerplates.wml:394
msgid ""
"<get-var eval-newcontributors-text-list /> since the previous issue of the "
"Debian Project News. Please welcome <get-var eval-newcontributors-name-list /"
"> into our project!"
msgstr ""
"<get-var eval-newcontributors-text-list /> sedan föregående utgåvan avDebian "
"Project News. Välkomna <get-var eval-newcontributors-name-list /> till vårat "
"projekt!"

#: ../../english/template/debian/projectnews/boilerplates.wml:407
msgid ""
"The <get-var issue-devel-news /> issue of the <a href=\"<get-var url-devel-"
"news />\">miscellaneous news for developers</a> has been released and covers "
"the following topics:"
msgstr ""
"Utgåva <get-var issue-devel-news /> av <a href=\"<get-var url-devel-news />"
"\">blandade nyheter för utvecklare</a> har blivit utgiven och täcker "
"följande ämnen:"

#: ../../english/template/debian/projectnews/footer.wml:6
msgid ""
"To receive this newsletter in your mailbox, <a href=\"https://lists.debian."
"org/debian-news/\">subscribe to the debian-news mailing list</a>."
msgstr ""
"För att få det här nyhetsbrevet i din inbox, <a href=\"https://lists.debian."
"org/debian-news/\">prenumerera på sändlistan debian-news</a>."

#: ../../english/template/debian/projectnews/footer.wml:10
#: ../../english/template/debian/weeklynews/footer.wml:10
msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
msgstr ""
"<a href=\"../../\">Tidigare utgåvor</a> av nyhetsbrevet finns att tillgå."

#. One editor name only
#: ../../english/template/debian/projectnews/footer.wml:15
msgid ""
"<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"Redaktör för Debian Project News är <a href=\"mailto:dwn@debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/projectnews/footer.wml:20
msgid ""
"<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"Redaktörer för Debian Project News är <a href=\"mailto:dwn@debian.org\">%s</"
"a>."

#. One editor name only
#: ../../english/template/debian/projectnews/footer.wml:25
msgid ""
"<void id=\"singular\" />This issue of Debian Project News was edited by <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"Detta nummer av Debian Project News redigerades av <a href=\"mailto:"
"dwn@debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/projectnews/footer.wml:30
msgid ""
"<void id=\"plural\" />This issue of Debian Project News was edited by <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"Detta nummer av Debian Project News redigerades av <a href=\"mailto:"
"dwn@debian.org\">%s</a>."

#. One translator only
#. One translator only
#: ../../english/template/debian/projectnews/footer.wml:35
#: ../../english/template/debian/weeklynews/footer.wml:35
msgid "<void id=\"singular\" />It was translated by %s."
msgstr "Det översattes av %s."

#. Two ore more translators
#. Two ore more translators
#: ../../english/template/debian/projectnews/footer.wml:40
#: ../../english/template/debian/weeklynews/footer.wml:40
msgid "<void id=\"plural\" />It was translated by %s."
msgstr "Det översattes av %s."

#. One female translator only
#. One female translator only
#: ../../english/template/debian/projectnews/footer.wml:45
#: ../../english/template/debian/weeklynews/footer.wml:45
msgid "<void id=\"singularfemale\" />It was translated by %s."
msgstr "Det översattes av %s."

#. Two ore more female translators
#. Two ore more female translators
#: ../../english/template/debian/projectnews/footer.wml:50
#: ../../english/template/debian/weeklynews/footer.wml:50
msgid "<void id=\"pluralfemale\" />It was translated by %s."
msgstr "Det översattes av %s."

#: ../../english/template/debian/weeklynews/footer.wml:6
msgid ""
"To receive this newsletter weekly in your mailbox, <a href=\"https://lists."
"debian.org/debian-news/\">subscribe to the debian-news mailing list</a>."
msgstr ""
"För att få det här nyhetsbrevet tillsänt dig per e-post varje vecka, <a href="
"\"https://lists.debian.org/debian-news/\">prenumerera på sändlistan debian-"
"news</a>."

#. One editor name only
#: ../../english/template/debian/weeklynews/footer.wml:15
msgid ""
"<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
"dwn@debian.org\">%s</a>."
msgstr ""
"Redaktör för Debian Weekly News är <a href=\"mailto:dwn@debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/weeklynews/footer.wml:20
msgid ""
"<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
"dwn@debian.org\">%s</a>."
msgstr ""
"Redaktörer för Debian Weekly News är <a href=\"mailto:dwn@debian.org\">%s</"
"a>."

#. One editor name only
#: ../../english/template/debian/weeklynews/footer.wml:25
msgid ""
"<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
"href=\"mailto:dwn@debian.org\">%s</a>."
msgstr ""
"Detta nummer av Debian Weekly News redigerades av <a href=\"mailto:"
"dwn@debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/weeklynews/footer.wml:30
msgid ""
"<void id=\"plural\" />This issue of Debian Weekly News was edited by <a href="
"\"mailto:dwn@debian.org\">%s</a>."
msgstr ""
"Detta nummer av Debian Weekly News redigerades av <a href=\"mailto:"
"dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "To receive this newsletter bi-weekly in your mailbox, <a href=\"https://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "För att få det här nyhetsbrevet tillsänt dig per e-post varannan vecka, "
#~ "<a href=\"https://lists.debian.org/debian-news/\">prenumerera på "
#~ "sändlistan debian-news</a>."

#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "Åter till <a href=\"./\">Debians föredragshållaröversikt</a>."

#~ msgid "List of Speakers"
#~ msgstr "Föredragshållarförteckning"

#~ msgid "Topics:"
#~ msgstr "Ämnen:"

#~ msgid "Location:"
#~ msgstr "Plats:"

#~ msgid "Languages:"
#~ msgstr "Språk:"

#~ msgid "Previous Talks:"
#~ msgstr "Tidigare föredrag:"

#~ msgid "Email:"
#~ msgstr "E-post:"

#~ msgid "Name:"
#~ msgstr "Namn:"
