#use wml::debian::template title="Ottenere Debian"
#use wml::debian::translation-check translation="50283624a3deec87adfe5a87644c4aad93d91c53" maintainer="Giuseppe Sacco"
#include "$(ENGLISHDIR)/releases/images.data"

<p>Debian è distribuita <a href="../intro/free">liberamente</a>
attraverso Internet ed è completamente scaricabile da uno qualsiasi dei
<a href="ftplist">mirror</a>.
Il <a href="../releases/stable/installmanual">manuale di installazione</a>
contiene informazioni dettagliate per l'installazione.
Le note di rilascio possono essere trovate <a
href="../releases/stable/releasenotes">qui</a>.</p>

<p>Se si vuole un metodo semplice per installare Debian, si può adottare
uno di questi modi:</p>

<div class="line">
 <div class="item col50">
  <h2><a href="netinst">Scaricare un file immagine</a></h2>
  <p>A seconda delle connessione Internet di cui si dispone, 
  si può scaricare uno dei seguenti file:</p>
  <ul>
      <li>Un <a href="netinst"><strong>piccolo file immagine di
      installazione</strong></a>:
      è rapido da scaricare e va copiato su un supporto rimovibile.
      Per usare questo tipo di file immagine, si necessiterà di una
      macchina con accesso ad Internet.
      <ul class="quicklist downlist">
	  <li><a title="scarica l'installer per PC 64-bit"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">netinst
		 iso 64-bit PC</a></li>
	  <li><a title="scarica l'installer per i normali PC 32-bit"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">netinst
		 iso 32-bit PC</a></li>
	</ul>
      </li>
      <li>Un <a href="../CD/"><strong>file immagine di installazione
      completo</strong></a>: più grande rispetto al precedente, contiene
      un maggior numero di pacchetti e rende quindi più semplice
      l'installazione di Debian su macchine prive di una connessione a
      Internet.
	<ul class="quicklist downlist">
	  <li><a title="scarica i torrent per il DVD di installazione per PC 64-bit"
	         href="<stable-images-url/>/amd64/bt-dvd/">torrent per PC 64-bit (DVD)</a></li>
	  <li><a title="scarica i torrent per il DVD di installazione per PC 32-bit"
		 href="<stable-images-url/>/i386/bt-dvd/">torrent per PC 32-bit (DVD)</a></li>
	  <li><a title="scarica i torrent per i CD di installazione per PC 64-bit"
	         href="<stable-images-url/>/amd64/bt-cd/">torrent per PC 64-bit (CD)</a></li>
	  <li><a title="scarica i torrent per i CD di installazione per PC 32-bit"
		 href="<stable-images-url/>/i386/bt-cd/">torrent per PC 32-bit (CD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Usare un'immagine
		Debian in cloud</a></h2>
    <ul>
      <li>Un'<a href="https://cloud.debian.org/images/cloud/"><strong>immagine
			in cloud</strong> ufficiale</a>: può essere usata direttamente dal
			nostro provider, creata dal Debian Cloud Team.
        <ul class="quicklist downlist">
          <li><a title="Immagine OpenStack per Intel e AMD 64-bit Qcow2"
href="https://cloud.debian.org/cdimage/openstack/current-10/debian-10-openstack-amd64.qcow2">\
AMD/Intel 64-bit OpenStack (Qcow2)</a></li>
          <li><a title="Immagine OpenStack per ARM 64-bit Qcow2"
href="https://cloud.debian.org/cdimage/openstack/current-10/debian-10-openstack-arm64.qcow2">\
ARM 64-bit OpenStack (Qcow2)</a></li>
        </ul>
      </li>
    </ul>
    <h2><a href="../CD/live/">Prova Debian live prima di installarla</a></h2>
    <p>
     È possibile provare Debian avviando un sistema live da un CD, un DVD
     o una chiave USB senza dover installare nessun file sul computer.
     Quando si è pronti, si potrà avviare l'installazione direttamente
     dal sistema live. Questo metodo di installazione potrebbe essere
     quello adeguato qualora l'immagine live coincida con i propri
     requisiti di spazio, lingua e selezione dei pacchetti.
     Puoi consultare <a href="../CD/live#choose_live">ulteriori
     informazioni su questo metodo</a>.
    </p>
    <ul class="quicklist downlist">
      <li><a title="scarica i torrent per Debian Live per PC 64-bit"
	     href="<live-images-url/>/amd64/bt-hybrid/">torrent per Debian Live per PC 64-bit</a></li>
      <li><a title="scarica i torrent per Debian Live per PC 32-bit"
	     href="<live-images-url/>/i386/bt-hybrid/">torrent per Debian Live per PC 32-bit</a></li>
    </ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2>È anche possibile <a href="../CD/vendors/">acquistare un set di
    CD o DVD da uno dei rivenditori di CD Debian</a></h2>

   <p>
      Molti di questi venditori ti permetteranno di acquistare la
      distribuzione per meno di 5 dollari USA (poco meno di 5 Euro), 
      più le spese di spedizione (consultare il sito web del venditore
      per verificare che effettui spedizioni internazionali).
      <br />
      In alcuni casi, i <a href="../doc/books">libri su Debian</a>
      vengono venduti con CD di installazione acclusi.
   </p>

   <p>Ecco alcuni vantaggi dei CD:</p>

   <ul>
     <li>l'installazione da CD è più semplice</li>
     <li>è possibile installare Debian su macchine prive di connessione a
     Internet</li>
     <li>si può installare Debian (su quante macchine si desidera)
      senza dover scaricarsi tutti i pacchetti</li>
     <li>il CD di installazione può essere usato come disco di ripristino
     su un sistema danneggiato.</li>
   </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="pre-installed">Comprare un computer con Debian già
    installata</a></h2>
   <p>Vi sono una serie di vantaggi da considerare legati a questo
   metodo:</p>
   <ul>
    <li>non si deve installare Debian</li>
    <li>l'installazione è già configurata per l'hardware</li>
    <li>il rivenditore può fornire supporto tecnico</li>
   </ul>
    </div>
</div>
   
