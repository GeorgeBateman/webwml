#use wml::debian::translation-check translation="3ec1d4afe3a4c6cd22acffb80bef238ec4798b0e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le National Cyber Security Centre (NCSC) du Royaume-Uni a découvert que
Xerces-C, une bibliothèque d’analyse pour la validation d’XML pour C++, contient
une erreur d’utilisation de mémoire après libération déclenchée lors de
l’analyse d’un DTD externe. Un attaquant pourrait causer un déni de service
et éventuellement réaliser une exécution de code à distance. Ce défaut n’a pas
été corrigé dans la version en cours de la bibliothèque et n’a pas de mitigation
totale. Une première correction est faite par cette mise à jour qui fixe la
vulnérabilité d’utilisation de mémoire après libération au détriment d’une fuite
de mémoire. Une deuxième correction est de désactiver le traitement du DTD, ce
qui peut être réalisé à l’aide de DOM en utilisant une fonction d’analyse
standard ou à l’aide de SAX en utilisant la variable d’environnement
XERCES_DISABLE_DTD.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 3.1.4+debian-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xerces-c.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de xerces-c, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/xerces-c">https://security-tracker.debian.org/tracker/xerces-c</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2498.data"
# $Id: $
