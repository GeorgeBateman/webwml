#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs bogues ont été découverts dans PostgreSQL, un système de
serveur de bases de données relationnelles. Cette mise à jour corrige
divers problèmes de stabilité.</p>

<p>La version 9.1.24 marque la fin de vie de la branche PostgreSQL 9.1. Il
n'y aura plus de publication réalisée par le PostgreSQL Global Development
Group.</p>

<p>Les utilisateurs de PostgreSQL 9.1 devraient envisager une mise à niveau
vers une publication plus récente de PostgreSQL. Les options sont :</p>

<ul>

<li>Mise à niveau vers Debian 8 (Jessie), fournissant postgresql-9.4.</li>

<li><p>L'utilisation du dépôt apt.postgresql.org est préconisée,
fournissant des paquets pour toutes les branches actives de PostgreSQL
(de 9.2 à 9.6 au moment de la rédaction de l'annonce).</p>

<p>Consultez <a href="https://wiki.postgresql.org/wiki/Apt">https://wiki.postgresql.org/wiki/Apt</a>
pour plus d'informations sur le dépôt.</p>

<p>Un script d'assistance pour activer le dépôt est fourni dans
/usr/share/doc/postgresql-9.1/examples/apt.postgresql.org.sh.gz.</p></li>

<li><p>Dans Debian, une version LTS de 9.1 est en préparation qui couvrira
la durée de vie de wheezy-lts. Des mises à jour seront réalisées au mieux
de nos moyens. Les utilisateurs peuvent tirer avantage de cela, mais
devraient toutefois envisager de mettre à niveau PostgreSQL vers de
nouvelles versions dans les prochains mois.</p>

<p>Consultez <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>
pour plus d'informations sur Debian LTS.</p></li>

</ul>

<p>Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été corrigés dans la
version 9.1.24-0+deb7u1 de postgresql-9.1.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-709.data"
# $Id: $
