#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
 <p>Une vulnérabilité a été découverte dans Asterisk, une boîte à outils PBX et
de téléphonie au code source ouvert qui pourrait aboutir dans un épuisement de
ressources ou un déni de service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17090">CVE-2017-17090</a>

<p>Fuite de mémoire pour chan_skinny.
Si le pilote de périphérique chan_skinny (protocole SCCP) est submergé par
certaines requêtes, il peut faire que les processus asterisk utilisent un
montant excessif de mémoire virtuelle, provoquant l’arrêt du traitement par
asterisk de requêtes de toutes sortes. Le pilote chan_skinny driver a été mis
à jour pour fournir des allocations de mémoire de manière adéquate, par là même
empêchant toute possibilité d’épuisement de mémoire.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1:1.8.13.1~dfsg1-3+deb7u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets asterisk.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1225.data"
# $Id: $
