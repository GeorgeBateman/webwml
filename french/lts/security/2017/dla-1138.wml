#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Martin Thomson a découvert que nss, la bibliothèque du service de sécurité
réseau de Mozilla, est prédisposée à une vulnérabilité d’utilisation de mémoire
après libération dans l’implémentation de TLS 1.2 lors de la génération des
hachages d’initialisation de connexion. Un attaquant distant peut exploiter ce
défaut pour provoquer le plantage d’une application utilisant la bibliothèque
nss, aboutissant à un déni de service ou éventuellement à l’exécution de code
arbitraire.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2:3.26-1+debu7u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nss.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1138.data"
# $Id: $
