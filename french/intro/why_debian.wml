#use wml::debian::template title="Raisons pour choisir Debian" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7141cf5f25b03b1aa3c02c1afe2a5ca8cab2f672" maintainer="Jean-Pierre Giraud"

<p>Il existe beaucoup de raisons pour lesquelles les utilisateurs choisissent Debian comme système d'exploitation.</p>

<h1>Raisons principales</h1>

# try to list reasons for end users first

<dl>
  <dt><strong>Debian est un logiciel libre.</strong></dt>
  <dd>
    Debian est une distribution faite de logiciels libres et au code source
    ouvert et sera toujours 100 % <a href="free">libre</a> — libre à chacun
    de l'utiliser, de la modifier et de la distribuer. C'est notre engagement
    principal vis à vis de <a href="../users">nos utilisateurs</a>. Elle est
    aussi gratuite.
  </dd>
</dl>

<dl>
  <dt><strong>Debian est un système d'exploitation stable et sûr basé sur Linux.</strong></dt>
  <dd>
    Debian est un système d'exploitation pour une large gamme d'appareils
    depuis les ordinateurs portables jusqu'aux ordinateurs de bureau et aux
    serveurs. Ses utilisateurs apprécient sa stabilité et sa fiabilité
    depuis 1993. Elle fournit une configuration par défaut correcte pour
    chaque paquet. Les développeurs Debian fournissent des mises à jour de
    sécurité pour tous les paquets et pour toute leur durée de vie chaque
    fois que cela est possible.
  </dd>
</dl>

<dl>
  <dt><strong>Debian dispose d'une vaste prise en charge matérielle.</strong></dt>
  <dd>
    L'essentiel des matériels est déjà pris en charge par le noyau Linux.
    Lorsque les logiciels libres ne sont pas suffisants, des pilotes
    propriétaires sont disponibles pour les matériels.
  </dd>
</dl>

<dl>
  <dt><strong>Debian offre des mises à niveau en douceur.</strong></dt>
  <dd>
    Debian est réputée pour ses mises à niveau faciles et en douceur au cours
    du cycle de la publication, mais aussi pour la publication majeure
    suivante.
  </dd>
</dl>

<dl>
  <dt><strong>Debian est le germe et la base de nombreuses autres distributions.</strong></dt>
  <dd>
    Beaucoup des distributions Linux les plus populaires, telles qu'Ubuntu,
    Knoppix, PureOS, SteamOS ou Tails, ont choisi Debian comme base pour leurs
    logiciels. Debian fournit tous les outils de telle sorte que tout le monde
    puisse accroître ses paquets logiciels en partant de l'archive Debian avec
    ses propres paquets pour répondre à ses besoins.
  </dd>
</dl>

<dl>
  <dt><strong>Le projet Debian est une communauté.</strong></dt>
  <dd>
    Debian n'est pas seulement un système d'exploitation basé sur Linux. Les
    logiciels sont la coproduction de centaines de volontaires du monde entier.
    Vous pouvez faire partie de la communauté Debian même si vous n'êtes pas
    un programmeur ou un administrateur système. Debian est communautaire,
    basée sur le consensus, et possède une <a href="../devel/constitution">structure
    de gouvernance démocratique</a>. Dans la mesure où tous les développeurs
    Debian ont des droits égaux, elle ne peut pas être contrôlée par une
    entreprise unique. Nous avons des développeurs dans plus de 60 pays et
    nous prenons en charge la traduction de l'installateur Debian dans plus
    de 80 langues.
  </dd>
</dl>

<dl>
  <dt><strong>Debian dispose de plusieurs options d'installateur.</strong></dt>
  <dd>
    Les utilisateurs peuvent utiliser un
    <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">CD autonome (« live »)</a>
    qui fournit l'installateur Calamares, facile à utiliser, qui ne demande que
    très peu de saisie ou de connaissances préalables. Les utilisateurs plus
    expérimentés peuvent utiliser notre installateur unique et complet tandis
    que les experts peuvent peaufiner leur installation ou même utiliser un
    outil d'installation automatisée par le réseau.
  </dd>
</dl>

<br>

<h1>Environnement d'entreprise</h1>

<p>
  Si vous avez besoin de Debian dans un environnement professionnel, vous
  pouvez bénéficier d'avantages supplémentaires :
</p>

<dl>
  <dt><strong>Debian est fiable.</strong></dt>
  <dd>
    Debian démontre sa fiabilité tous les jours dans des milliers de scénarios
    du monde réel qui vont de l'ordinateur portable à utilisateur unique aux
    grands collisionneurs, bourses des valeurs ou industrie automobile. Elle est
    aussi répandue dans le monde universitaire, dans la recherche et dans le
    secteur public.
  </dd>
</dl>

<dl>
  <dt><strong>Debian compte de nombreux experts.</strong></dt>
  <dd>
    Les responsables de paquet ne font pas que s'occuper de l'empaquetage
    Debian et de l'incorporation des nouvelles versions amont. Souvent, ce
    sont des experts des logiciels amont et contribuent directement au
    développement. Parfois, ils appartiennent à l'équipe amont.
  </dd>
</dl>

<dl>
  <dt><strong>Debian est sûre.</strong></dt>
  <dd>
    Debian prend en charge la sécurité de ses versions stables. Beaucoup
    d'autres distributions et de chercheurs dans le domaine de la sécurité
    s'appuient sur le suivi de sécurité de Debian.
  </dd>
</dl>

<dl>
  <dt><strong>Prise en charge à long terme.</strong></dt>
  <dd>
    Il existe une <a href="https://wiki.debian.org/LTS">prise en charge à long terme</a>
    (Long Term Support – LTS) gratuite. Elle apporte une prise en charge
    étendue pour la version stable pour 5 ans et plus. Au-delà, il existe aussi
    l'initiative <a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a>
    qui étend la prise en charge d'un ensemble limité de paquets à plus de
    5 ans.
  </dd>
</dl>

<dl>
  <dt><strong>Images « cloud ».</strong></dt>
  <dd>
    Des images officielles pour le nuage sont disponibles pour toutes les
    principales plateformes de nuage. Nous fournissons aussi les outils et les
    configurations pour que vous puissiez construire votre propre image
    personnalisée pour le nuage. Il est aussi possible d'utiliser Debian dans
    des machines virtuelles sur le bureau ou dans un conteneur.
  </dd>
</dl>

<br>

<h1>Développeurs</h1>
<p>Debian est largement utilisée par les développeurs de toutes sortes de
logiciels et matériels.</p>

<dl>
  <dt><strong>Un système de suivi des bogues accessible au public.</strong></dt>
  <dd>
    Le <a href="../Bugs">système de suivi des bogues</a> (Bug tracking system
    – BTS) de Debian est accessible publiquement à tous au moyen d'un
    navigateur web. Nous ne cachons pas les bogues de nos logiciels et il est
    facile de soumettre de nouveaux rapports de bogue.
  </dd>
</dl>

<dl>
  <dt><strong>Internet des objets et périphériques embarqués.</strong></dt>
  <dd>
    Debian prend en charge une large gamme d'appareils tels que le Raspberry
    Pi, des variantes de QNAP, des appareils mobiles, des routeurs domestiques
    et beaucoup d'ordinateurs monocartes (Single Board Computers – SBC).
  </dd>
</dl>

<dl>
  <dt><strong>De multiples architectures matérielles.</strong></dt>
  <dd>
    Debian prend en charge une <a href="../ports">longue liste</a>
    d'architectures de processeur dont amd64, i386, plusieurs versions d'ARM
    ainsi que MIPS, POWER7, POWER8, IBM System z et RISC-V. Debian est aussi
    disponible pour des architectures de niche anciennes ou spécifiques.
  </dd>
</dl>

<dl>
  <dt><strong>Un nombre immense des paquets logiciels disponibles.</strong></dt>
  <dd>
    Debian possède le plus grand nombre de paquets préparés disponibles
    (actuellement <packages_in_stable>). Les paquets utilisent le format deb
    qui est réputé pour sa qualité exceptionnelle.
  </dd>
</dl>

<dl>
  <dt><strong>Un choix de différentes versions.</strong></dt>
  <dd>
    En plus de la version stable, on peut obtenir les versions les plus
    récentes des logiciels en utilisant les versions <em>testing</em> ou
    <em>unstable</em>.
  </dd>
</dl>

<dl>
  <dt><strong>Une qualité élevée grâce aux outils de développement et à la charte.</strong></dt>
  <dd>
    Plusieurs outils de développement contribuent à conserver un standard élevé
    de qualité et notre <a href="../doc/debian-policy/">charte</a> définit les
    les exigences techniques que chaque paquet doit satisfaire afin d'être
    inclus dans la distribution. L'intégration continue se sert du logiciel
    autopkgtest, piuparts est notre outil de test d'installation, de mise à
    niveau et de suppression de paquet et lintian un vérificateur complet des
    paquets Debian.
  </dd>
</dl>

<br>

<h1>Que disent nos utilisateurs</h1>

<ul style="line-height: 3em">
  <li>
    <q><strong>
      Pour moi, Debian a un niveau parfait de facilité d'utilisation et de
      stabilité. J'ai utilisé au fil des ans plusieurs distributions
      différentes, mais Debian est la seule qui fonctionne, tout simplement.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Solide comme un roc. Des tonnes de paquets. Une communauté excellente.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Debian est pour moi un symbole de stabilité et de facilité d'utilisation.
    </strong></q>
  </li>
</ul>
