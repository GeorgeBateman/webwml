#use wml::debian::template title="Unsere Philosophie: warum und wie wir es tun"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="f51e6aa024def7d85259682733914f77e90a92e1"

# Updated: Holger Wansing <hwansing@mailbox.org>, 2020.

<ul class="toc">
<li><a href="#what">WAS ist Debian eigentlich?</a>
<li><a href="#free">Alles frei?</a>
<li><a href="#how">Wie arbeitet die Gemeinschaft als Projekt zusammen?</a>
<li><a href="#history">Wie fing alles an?</a>
</ul>

<h2><a name="what">WAS ist Debian eigentlich?</a></h2>

<p>Das <a href="$(HOME)/">Debian-Projekt</a> ist ein Zusammenschluss von
Einzelpersonen, die gemeinschaftlich ein <a href="/intro/free">freies</a>
Betriebssystem entwickeln. Dieses Betriebssystem, das wir entwickelt haben,
wird <strong>Debian</strong> genannt.</p>

<p>Ein Betriebssystem ist eine Menge von
grundlegenden Programmen, die Ihr Rechner zum Arbeiten
benötigt. Der wichtigste Teil eines Betriebssystems ist der
<em>Kernel</em>. Der Kernel ist das Programm, das für alle
Basisaufgaben und das Starten von anderen Programmen zuständig
ist.</p>

<p>Debian-Systeme verwenden im Augenblick den
<a href="https://www.kernel.org/">Linux</a>- oder den
<a href="https://www.freebsd.org/">FreeBSD</a>-Kernel. Linux ist ein von
<a href="https://en.wikipedia.org/wiki/Linus_Torvalds">Linus Torvalds</a>
ins Leben gerufenes
Stück Software, das von tausenden Programmierern weltweit
unterstützt wird.
FreeBSD ist ein Betriebssystem, das einen Kernel sowie weitere Software
enthält.
</p>

<p>Es ist jedoch auch Arbeit im Gange, Debian für andere Kernel
anzubieten, hauptsächlich für den <a
href="https://www.gnu.org/software/hurd/hurd.html">Hurd</a>. Der Hurd ist
eine Sammlung von Servern, die auf einem Mikro-Kernel
laufen (wie Mach) und verschiedene Funktionalitäten implementieren.
Der Hurd ist Freie Software entwickelt vom
<a href="https://www.gnu.org/">GNU-Projekt</a>.</p>

<p>Ein großer Teil der grundlegenden Werkzeuge, die das Betriebssystem
ausmachen, stammt vom <a
href="https://www.gnu.org/">GNU-Projekt</a>; daher auch die Namen: 
GNU/Linux, GNU/kFreeBSD und GNU/Hurd.
Diese Werkzeuge sind ebenfalls frei.</p>

<p>Was Benutzer benötigen, sind natürlich Anwendungsprogramme:
Programme, die ihnen helfen, das zu erledigen, was sie erreichen
möchten, von der Bearbeitung von Dokumenten über Business-Software und
Spiele bis hin zur Entwicklung weiterer Software. Debian besteht aus
mehr als <packages_in_stable> <a href="$(DISTRIB)/packages">Paketen</a>
(vorkompilierte Software, die in einem praktischen Format für eine
einfache Installation auf Ihrem Rechner zusammengestellt ist),
einem Paketmanager (APT) und weiteren Werkzeugen, die die Verwaltung
tausender Pakete auf tausenden Rechnern so einfach machen wie die
Installation eines einzigen Programms. Und das alles völlig
<a href="free">frei</a>.</p>

<p>Es ist wie bei einem Turm. Ganz unten ist der Kernel. Darüber
kommen alle Basiswerkzeuge. Als Nächstes folgen die
Anwendungen, die Sie auf Ihrem Computer laufen lassen. An der Spitze des
Turms befindet sich Debian &ndash; Debian organisiert und passt alles
sorgfältig an, so dass alles gut zusammenarbeitet.</p>

<h2>Alles <a href="free" name="free">frei?</a></h2>

<p>Wenn wir das Wort <q>frei</q> verwenden, beziehen wir uns auf die
Software-<strong>Freiheit</strong>.
Erfahren Sie mehr darüber, <a href="free">was
wir mit <q>freier Software</q> meinen</a> und
<a href="https://www.gnu.org/philosophy/free-sw">was die Free Software
Foundation darüber sagt</a>.</p>

<p>Sie wundern sich vielleicht: Warum sollten Leute viele Stunden
ihrer Zeit dazu verwenden, Programme zu entwickeln, sorgfältig zu
verpacken und sie dann einfach so <em>weiterzugeben</em>? 
Die Gründe sind so verschieden wie die Leute, die etwas dazu beitragen.
Einige wollen anderen helfen.
Viele schreiben Programme, um mehr über Computer zu lernen.
Mehr und mehr Leute suchen nach Wegen, die aufgeblasenen Preise von Software
zu vermeiden.
Eine wachsende Menge will sich durch eigene Beiträge für die viele großartige
freie Software bedanken, die sie von anderen bekommen hat.
Viele in der Wissenschaft erstellen freie Software, um die breite
Verwendung ihrer Forschungsergebnisse zu unterstützen.
Firmen helfen, freie Software zu warten, damit sie ein Mitspracherecht bei der
weiteren Entwicklung haben &ndash; es gibt keinen schnelleren Weg, eine neue
Fähigkeit zu erhalten, als sie selbst einzubauen!
Natürlich macht es vielen von uns auch einfach Spaß.</p>

<p>Debian fühlt sich so stark der freien Software verpflichtet,
dass wir der Meinung waren, diese Verbundenheit sollte in einem
Dokument formalisiert werden. So entstand unser
<a href="$(HOME)/social_contract">Gesellschaftsvertrag</a>.</p>

<p>Obwohl Debian an freie Software glaubt, gibt es Fälle, in denen
Leute nicht-freie Software auf ihrem Computer installieren wollen
oder müssen. Wo immer das möglich ist, wird Debian das
unterstützen. Es gibt sogar eine wachsende Anzahl von Paketen,
deren einzige Aufgabe es ist, nicht-freie Software auf einem
Debian-System zu installieren.</p>

<h2><a name="how">Wie arbeitet die Gemeinschaft als Projekt zusammen?</a></h2>

<p>Debian wird erstellt von fast eintausend aktiven Entwicklern, die
<a href="$(DEVEL)/developers.loc">über die gesamte
Erde</a> verteilt sind, und dafür ihre Freizeit opfern. Die wenigsten von
ihnen haben sich jemals persönlich getroffen. Die Kommunikation wird
hauptsächlich über E-Mail (Mailinglisten auf
lists.debian.org) und <acronym lang="en" title="Internet Relay Chat">\
IRC</acronym> (Kanal #debian auf irc.debian.org) abgewickelt.
</p>

<p>Das Debian-Projekt hat eine sorgfältig <a href="organization">\
organisierte Struktur</a>. Für weitere Informationen darüber, wie Debian intern
funktioniert, besuchen Sie gerne die <a href="$(DEVEL)/">Entwickler-Ecke</a>.</p>

<p>
Die Hauptdokumente, in denen beschrieben ist, wie die Gemeinschaft arbeitet, sind
folgende:
<ul>
<li><a href="$(DEVEL)/constitution">die Satzung</li>
<li><a href="../social_contract">der Gesellschaftsvertrag und Debians Richtlinien für Freie Software (DFSG)</li>
<li><a href="diversity">Stellungnahme zur Vielfalt</li>
<li><a href="../code_of_conduct">der Verhaltenskodex</li>
<li><a href="../doc/developers-reference/">die Entwicklerreferenz</li>
<li><a href="../doc/debian-policy/">das Debian Policy-Handbuch</li>
</ul>

<h2><a name="history">Wie fing alles an?</a></h2>

<p>Debian wurde im August 1993 von Ian Murdock als neue Distribution
mit offener Entwicklung gegründet, getreu dem Geist von Linux
und GNU. Debian sollte vorsichtig und umsichtig zusammengestellt
und mit der gleichen Sorgfalt gepflegt werden. Es begann als
kleine, engverbundene Gruppe von Freie-Software-Hackern, wuchs
dann Schritt für Schritt zu einer großen, gut organisierten
Gemeinschaft von Entwicklern und Anwendern heran. Lesen Sie auch die
<a href="$(DOC)/manuals/project-history/">detaillierte Historie von Debian</a>.</p>

<p>Da viele Leute gefragt haben: Debian wird /&#712;de.bi.&#601;n/ (mit einem
kurzen <q>e</q>) ausgesprochen. Es leitet sich vom Namen des Debian-Gründers,
Ian Murdock, und dem Namen seiner Frau Debra ab.</p>
