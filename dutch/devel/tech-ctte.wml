#use wml::debian::template title="Het Technisch Comité van Debian" BARETITLE="true"
#use wml::debian::toc
#use wml::debian::translation-check translation="702611d45a5f393e4cdef21408c1696c76074fb5"

<p>Het Technisch Comité werd ingesteld door sectie 6 van de
<a href="constitution">statuten van Debian</a>. Het is het orgaan dat binnen
het Debian-project de finale beslissing neemt bij technische geschillen.
</p>

<toc-display/>

<toc-add-entry name="referquestions">Hoe u een vraag doorverwijst naar het comité</toc-add-entry>

<ol>

<li>Voor u een beslissing doorverwijst naar het Technisch Comité, zou u de zaak
    zelf moeten proberen oplossen. Ga een constructieve discussie aan en tracht
    het standpunt van de andere persoon te begrijpen. Als u na bespreking een
    technische vraag heeft gevonden waarover u het niet eens kunt worden, dan
    kunt u die voorleggen aan het comité:
</li>

<li>Maak een samenvattende beschrijving van het meningsverschil, tracht bij
    voorkeur het hierover eens te worden met de tegenpartij en stuur de
    beschrijving naar het bugvolgsysteem <em>als een nieuwe bug</em> tegen het
    pseudo-pakket <tt>tech-ctte</tt>. Vermeld in uw samenvatting het nummer van
    eventuele relevante bestaande bugs en ook URL's van mailinglijstarchieven.
</li>

<li>Stuur een e-mail naar alle relevante partijen en nodig hen uit om in te
    tekenen op de bug. Indien er over de kwestie openstaande bugs bestaan, stel
    dan in dat de nieuwe tech-ctte-bug deze blokkeert (maak u geen zorgen als u
    niet weet hoe u dit moet doen - dan doen wij het voor u.)

<li>Het comité zal uw vraag bespreken in de tech-ctte-bug. Over het algemeen
    sturen we geen Cc van de bespreking naar individuele deelnemers, tenzij we
    hen willen uitnodigen om deel te nemen aan de bespreking om hen een
    specifieke vraag te kunnen stellen. Iedereen die in de kwestie
    geïnteresseerd is, zou via het BTS moeten intekenen op de bug.

<li>Het comité zal ernaar streven om zo snel mogelijk een beslissing te nemen.
    In de praktijk zal dit proces waarschijnlijk verschillende weken in beslag
    nemen, misschien zelfs meer. Indien het een bijzonder dringende vraag
    betreft, zou u dit moeten vermelden.
</li>

<li>Soms gebeurt het dat tijdens het beraad van het comité, de een of de andere
    partij overtuigd geraakt van de waarde van de argumenten van de andere
    partij. Dit is een goede zaak! Indien dit gebeurt, moet het comité geen
    formele beslissing nemen en kan het bugrapport gesloten worden of opnieuw
    worden toegewezen, naargelang het geval.
</li>

</ol>

<h3>Enkele kanttekeningen bij het contacteren van het comité</h3>

<ul>

<li>Een goed en krachtig debat is belangrijk om ervoor te zorgen dat alle
    aspecten van een zaak ten volle worden verkend. Wanneer u technische vragen
    bespreekt met andere ontwikkelaars moet u erop voorbereid zijn dat u
    uitgedaagd zult worden. U moet ook bereid zijn om overtuigd te worden! Het
    is geen schande om de waarde van goede argumenten in te zien.
</li>

<li>Voer uw technische discussies met andere ontwikkelaars op een kalme en
    beschaafde manier. Gebruik geen beledigingen en stel de competentie van de
    ander niet in vraag. Richt u in plaats daarvan op de argumenten van uw
    tegenstander.
</li>

<li>Het comité heeft enkel de bevoegdheid om technische beslissingen te nemen.
    Indien u het gevoel heeft dat iemand zich misdragen heeft, kan het comité u
    wellicht weinig helpen. Mogelijk wenst u daarover de Projectleider
    <tt>leader@debian.org</tt> te spreken.
</li>

<li>Het bugverkeer verschijnt ook op de mailinglijst van het comité <a href="https://lists.debian.org/debian-ctte/">debian-ctte@lists.debian.org</a>.
    Iedereen die dit wil kan op de mailinglijst debian-ctte intekenen en onze
    beraadslaging volgen. Stuur echter geen berichten in verband met specifieke
    kwesties rechtstreeks naar de lijst.
</li>

<li>Om op de mailinglijst van het comité te kunnen posten moet u ofwel
    ingetekend hebben op de lijst met het adres waarmee u post of moet u uw
    bericht met PGP ondertekenen. Dit is een maatregel om spam te vermijden. We
    verontschuldigen ons voor het ongemak, maar deze instelling laat de leden
    van het comité toe om de juiste aandacht te besteden aan de mails van de
    lijst.
</li>

</ul>

<toc-add-entry name="membership">Lidmaatschap</toc-add-entry>

<p>De huidige samenstelling van het comité wordt vermeld op de pagina over de
<a href="$(HOME)/intro/organization#tech-ctte">Organisatiestructuur van Debian</a>.
</p>

<toc-add-entry name="status">Archieven en status</toc-add-entry>

<p>De <a href="https://lists.debian.org/debian-ctte/">mailinglijst van het
comité wordt gearchiveerd</a>.</p>

<p><a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=tech-ctte">Vragen
waarover nog geen beslissing genomen werd</a>,
kunnen worden bekeken in het bugvolgsysteem.</p>

<toc-add-entry name="repository">VCS-opslagplaats</toc-add-entry>

<p>Soms maakt het TC gebruik van zijn
<a href="https://salsa.debian.org/debian/tech-ctte">\
gedeelde git-opslagplaats</a>
om samen te werken.</p>

<h3>Formele technische beslissingen met inbegrip van aanbevelingen en adviezen</h3>

<p> De onderdelen met de besluitvormingsgeschiedenis zijn niet noodzakelijk up-to-date.
  (<a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=tech-ctte;archive=yes">Oudere
  vragen en beslissingen</a> kunnen bekeken worden in het bugvolgsysteem.)</p>

<ul>
  <li><a href="https://lists.debian.org/debian-devel-announce/2019/03/msg00001.html">05-03-2019</a>
    <a href="https://bugs.debian.org/914897">Bug #914897:</a>Het
    technisch comité besliste om niet in te gaan op de vraag om de beslissing
    van de
    <a href="https://wiki.debian.org/Debootstrap">debootstrap</a>-beheerders
    om op nieuw geïnstalleerde systemen standaard "samenvoeging van /usr" te
    activeren, teniet te doen. Het besluit verduidelijkte ook de wenselijke
    oplossing in verband met de toestand van "/usr-samenvoeging" op het
    ogenblik van de Bullseye-release van Debian.</li>
  <li>
    <a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00004.html">13-11-2018</a>
    <a href="https://bugs.debian.org/904302">Bug #904302:</a>Het technisch
    comité heeft besloten dat elk gebruik voor pakketten in het Debian-archief
    van de reeks van leveranciersspecifieke patches van dpkg, een bug is en dat
    een dergelijke functionaliteit in het Debian archief verboden zal worden na
    de uitgave van de Buster-release van Debian.</li>
  <li>
    <a href="https://lists.debian.org/debian-devel-announce/2018/02/msg00004.html">16-02-2018</a>
    <a href="https://bugs.debian.org/883573">Bug #883573:</a>Het
    technisch comité herroept de op 15-11-2014 in
    <a href="https://bugs.debian.org/746578">Bug #746578</a> genomen
    beslissing en bevrijdt de pakketten welke door het pakket libpam-systemd
    vereist worden, van specifieke ordeningsvoorwaarden.</li>
  <li>
    <a href="https://lists.debian.org/debian-devel-announce/2017/07/msg00006.html">31-07-2017</a>
    <a href="https://bugs.debian.org/862051">Bug #862051:</a>Het
    technisch comité herroept de op 12-07-2012 in
    <a href="https://bugs.debian.org/614907">Bug #614907</a>
    genomen beslissing en staat het pakket nodejs toe om te voorzien
    in /usr/bin/node in regelingen voor achterwaartse compatibiliteit.</li>
  <li>04-09-2015
    <a href="https://bugs.debian.org/741573">Bug #741573:</a>Het technisch
    comité neemt de beleidswijzigingen met betrekking tot menu-items die door
    Charles Plessy zijn voorgesteld, aan en beslist daarnaast dat pakketten die
    in een desktop-bestand voorzien niet ook nog eens een menu-bestand mogen
    aanbieden.
</li>
<li>19-06-2015
  <a href="https://bugs.debian.org/750135">Bug #750135:</a>Het
    technisch comité moedigt Christian Perrier aan om zijn voorstel
    over het onderhouden van het Aptitude-project te implementeren.</li>
<li>2014-11-15
  <a href="https://bugs.debian.org/746578">Bug #746578:</a>Het
    comité besliste dat systemd-shim als eerste alternatieve vereiste
    van libpam-systemd vermeld moet worden in plaats van systemd-sysv.</li>
<li>01-08-2014
  <a href="https://bugs.debian.org/746715">Bug #746715</a>: Het
    technisch comité verwacht dat pakketbeheerders de verschillende beschikbare
    init-systemen blijven ondersteunen.</li>
<li>01-08-2014
  <a href="https://bugs.debian.org/717076">Bug #717076</a>: Het
     comité besliste dat libjpeg-turbo de standaard libjpeg implementatie
     moet zijn.</li>
<li>11-02-2014
  <a href="https://bugs.debian.org/727708">Bug #727708</a>: Het
     comité besliste dat het standaard init-systeem voor Linux-architecturen
     in jessie systemd moet zijn.</li>
<li>06-03-2013
  <a href="https://bugs.debian.org/698556">Bug #698556</a>: Het
      comité wijst het standpunt van de beheerder van isdnutils af om de opname
      van code voor het aanmaken van isdn-apparaten door isdnutils te vereisen.
</li>
<li>21-12-2012
    <a href="https://bugs.debian.org/688772">Bug #688772</a>:
    Het comité verwerpt dat meta-gnome network-manager vereist,
    terwijl de in <a href="https://bugs.debian.org/681834#273">§4 van de
    beslissing uit #681834</a> geuite bekommernissen onbeantwoord blijven.</li>
<li>05-10-2012
    <a href="https://bugs.debian.org/573745">Bug #573745</a>:
    Het comité weigert om de beheerder van de python-pakketten in Debian te
    veranderen.</li>
<li>14-09-2012
    <a href="https://bugs.debian.org/681834">Bug #681834</a>: gnome-core
    zou Recommends: network-manager moeten gebruiken; het standpunt van de
    beheerder wordt afgewezen.</li>
<li>24-08-2012
    <a href="https://bugs.debian.org/681783">Bug #681783</a>: Het beleid in
    verband met Recommends is correct; Recommends mag in metapakketten
    gebruikt worden.</li>
<li>14-08-2012
    <a href="https://bugs.debian.org/681687">Bug #681687</a>:
    Het feit dat evince geen item heeft voor mime-type PDF is een RC-bug
    (weigering om het standpunt van het release team te verwerpen).</li>
<li>12-07-2012
    <a href="https://bugs.debian.org/614907">Bug #614907</a>:
    nodejs moet /usr/bin/nodejs gebruiken, node moet veranderd worden naar
    ax25-node en moet voorzien in /usr/sbin/ax25-node, en er werden
    overgangspakketten en verouderde pakketten gedefinieerd.</li>
<li>05-04-2012
    <a href="https://bugs.debian.org/640874">Bug #640874</a>: weigering om
    het standpunt van de beheerders van de beleidsrichtlijnen te verwerpen.
    debian/rules moet een Makefile zijn.</li>
<li>21-03-2012
    <a href="https://bugs.debian.org/629385">Bug #629385</a>:
    dpkg-buildpackage zal het testen van build-arch implementeren met
    make -qn.</li>
<li>27-02-2012
    <a href="https://bugs.debian.org/607368">Bug #607368</a>: weigering om het
    ABI-nummeringsbeleid van het team kernelbeheerders te verwerpen.</li>
<li>05-02-2012
    <a href="https://bugs.debian.org/658341">Bug #658341</a>: dpkg waarin
    multi-arch geactiveerd werd, mag door Raphaël Hertzog naar experimental en
    unstable geüpload worden zonder te moeten wachten op het code-nazicht door
    de primaire beheerder.</li>
<li>01-12-2010
    <a href="https://bugs.debian.org/587886">Bug #587886</a>:
    lilo moet in unstable blijven. Matt Arnold en Joachim
    Wiedorn zullen gezamenlijk lilo beheren.</li>
<li>04-09-2009
    <a href="https://bugs.debian.org/535645">Bug #535645</a>:
    weigering om de verwijdering door het ftp-team van ia32-libs-tools af te
    wijzen; herbevestiging van de bevoegdheid van het ftp-team om pakketten te
    verwijderen; beveelt het verduidelijken van de redenen voor verwijdering
    aan en mechanismen voor een herintroductie in het archief.</li>
<li>27-08-2009
    <a href="https://bugs.debian.org/510415">Bug #510415</a>:
    Qmail wordt toegelaten in Debian na het oplossen van het probleem in
    verband met delayed-bounce, met een RC-bug om de overgang voor een maand
    te blokkeren.</li>
<li>30-07-2009
    <a href="https://bugs.debian.org/539158">Bug #539158</a>: weigering om het
    standpunt van de udev-beheerder te verwerpen; er wordt gesuggereerd om
    printf te documenteren als een vereiste ingebouwde functie in policy.</li>
<li>25-07-2009
    <a href="https://bugs.debian.org/484841">Bug #484841</a>: standaard kan
    de groep staff niet schrijven in /usr/local; een wijziging kan
    geïmplementeerd worden na een transitieplan dat systeembeheerders toelaat
    om het huidige gedrag te behouden.</li>
<li>10-12-2007
    <a href="https://bugs.debian.org/412976">Bug #412976</a>:
    behoud van het huidige gedrag en het bestaande beleid in verband met het
    gebruik van /etc/default door mixmaster.</li>

<li>22-06-2007
    <a href="https://bugs.debian.org/367709">Bug #367709</a>:
    er mag geen udeb met libstdc++ gemaakt worden.</li>

<li>19-06-2007
    <a href="https://bugs.debian.org/341839">Bug #341839</a>:
    de uitvoer van <code>md5sum</code> mag niet wijzigen.</li>

<li>09-04-2007
    <a href="https://bugs.debian.org/385665">Bug #385665</a>:
    <code>fluidsynth</code> blijft in main.</li>

<li>09-04-2007
    <a href="https://bugs.debian.org/353277">Bug #353277</a>,
    <a href="https://bugs.debian.org/353278">Bug #353278</a>:
    <code>ndiswrapper</code> blijft in main.</li>

<li>27-03-2007
    <a href="https://bugs.debian.org/413926">Bug #413926</a>:
    <code>wordpress</code> moet opgenomen worden in etch.</li>

<li>24-06-2004
    <a href="https://bugs.debian.org/254598">Bug #254598</a>:
    <code>amd64</code> is een goede naam voor deze architectuur.
    <a href="https://lists.debian.org/debian-ctte/2004/debian-ctte-200406/msg00115.html">Volledige tekst</a>.
    Voorstanders: Wichert, Raul, Guy, Manoj, Ian.
    De stemperiode werd vervroegd beëindigd; er werden geen ander stemmen
    uitgebracht.</li>
<li>05-06-2004
    <a href="https://bugs.debian.org/164591">Bug #164591</a>,
    <a href="https://bugs.debian.org/164889">Bug #164889</a>:
    <code>md5sum &lt;/dev/null</code> moet de naakte md5sum-waarde opleveren.
    <a href="https://lists.debian.org/debian-ctte/2004/debian-ctte-200406/msg00032.html">Volledige tekst</a>.
    Voorstanders: Guy, Ian, Manoj, Raul.
    Geen andere stemmen.</li>
<li>06-10-2002
    <a href="https://bugs.debian.org/104101">Bug #104101</a>,
    <a href="https://bugs.debian.org/123987">Bug #123987</a>,
    <a href="https://bugs.debian.org/134220">Bug #134220</a>,
    <a href="https://bugs.debian.org/161931">Bug #161931</a>:
    De standaard kernel moet VESA framebuffer-ondersteuning hebben.
    <a href="https://lists.debian.org/debian-ctte/2002/debian-ctte-200211/msg00043.html">Volledige tekst</a>.
    Voorstanders: Ian, Jason, Raul; tegenstander: Manoj.
    Geen andere stemmen.</li>
<li>19-07-2002 <a href="https://bugs.debian.org/119517">Bug #119517</a>:
    Pakketten kunnen soms binaire bestanden bevatten met bibliotheken
    waarnaar enkel in Suggests verwezen wordt.
    <a href="https://lists.debian.org/debian-ctte/2002/debian-ctte-200207/msg00017.html">Volledige
    tekst</a>. Voorstanders: Ian, Wichert; tegenstanders: Bdale,
    Manoj; niemand anders stemde en Ian gebruikte zijn beslissende stem.</li>
</ul>

<p>Merk op dat beslissingen die dateren van voor 1 april 2002 hier nog niet
   opgenomen werden.</p>

<h3>Formele niet-technische en procedurele beslissingen</h3>

<ul>
<li>05-03-2015 Keurde de kandidaturen van Sam Hartman, Tollef Fog Heen en
    Didier Raboud voor het comité goed.
    (<a href="https://lists.debian.org/debian-ctte/2015/03/msg00023.html">Volledige
    tekst</a>. Voorstanders: Don, Bdale, Andreas, Colin, Steve, Keith.
    Aanstelling goedgekeurd door de DPL op 08-03-2015;
    <a href="https://lists.debian.org/debian-devel-announce/2015/03/msg00003.html">Volledige
    tekst</a>).</li>
<li>07-11-2013 Keurde Keith Packard goed als lid van het technisch comité (<a href="https://lists.debian.org/debian-ctte/2013/11/msg00041.html">resolutie</a>)</li>
<li>24-08-2011 Keurde Colin Watson goed als lid van het technisch comité (<a href="https://lists.debian.org/debian-devel-announce/2011/08/msg00004.html">voor de benoeming</a>)</li>
<li>11-01-2009 Keurde Russ Allbery en Don Armstrong goed als leden van het technisch comité (<a href="https://lists.debian.org/debian-ctte/2009/01/msg00053.html">samenvatting</a>)</li>
<li>11-04-2006 Verkoos Bdale als voorzitter (<a href=https://lists.debian.org/debian-ctte/2006/04/msg00042.html>voor de stemming</a>)</li>
<li>27-02-2006 Verkoos Steve als voorzitter (<a href=https://lists.debian.org/debian-ctte/2006/02/msg00085.html>voor de samenvatting</a>)</li>
<li>20-12-2005 Keurde de kandidaturen voor het comité van Steve Langasek,
    Anthony Towns en Andreas Barth goed.
    (<a href="https://lists.debian.org/debian-ctte/2005/12/msg00042.html">Volledige
    tekst</a>.  Voorstanders: Bdale, Manoj.  Steunbetuigingen,
    met verontschuldigingen, na het einde van de stemperiode: Ian, Raul.
    Geen tegenstanders of onthoudingen;  Aanstelling goedgekeurd door de DPL op
    05-01-2006;
    <a href="https://lists.debian.org/debian-project/2006/01/msg00013.html">Volledige
    tekst</a>).</li>
<li>20-12-2005 Stelde de verwijdering uit het comité van Wichert, Guy, en Jason
    voor.
    (<a href="https://lists.debian.org/debian-ctte/2005/12/msg00000.html">Tekst van de motie</a>; <a href="https://lists.debian.org/debian-ctte/2005/12/msg00028.html">resultaten</a>.  Voorstanders: Manoj, Raul.  Guy: voorstander van zijn eigen verwijdering; voorts geen mening.  Ian: voorstander van de verwijdering van Jason; voorts tegen het voorstel.
    Verwijdering goedgekeurd door de DPL op 05-01-2006;
    <a href="https://lists.debian.org/debian-project/2006/01/msg00013.html">Volledige
    tekst</a>.)</li>
<li>05-07-2002 Gaf de kwestie van het juiste gebruik van de ernstigheidsgraden
    in het bugsysteem (<a href="https://bugs.debian.org/97671">Bug #97671</a>)
    door aan de BTS-beheerders en de projectleider.
    (<a href="https://lists.debian.org/debian-ctte/2002/debian-ctte-200207/msg00002.html">Volledige
    tekst</a>.  Voorstanders: Ian, Jason, Bdale; geen tegenstanders en geen
    onthoudingen.)</li>

<li>31-01-2002 Stelde Ian Jackson aan als voorzitter, na het aftreden van Raul
    uit zijn functie.  (Voorstanders: Dale, Ian, Manoj, Raul, Wichert;
    geen tegenstanders en geen onthoudingen.)</li>

</ul>

<p>Merk op dat beslissingen die dateren van voor 31 januari 2002 hier nog niet
   opgenomen werden.</p>

<toc-add-entry name="retiredmembers">Leden die aftraden</toc-add-entry>

Met dank aan de volgende personen die gediend hebben in het comité:

<ul>
<li>Tollef Fog Heen (05-03-2015 - 31-12-2019)</li>
<li>Didier Raboud (05-03-2015 - 31-12-2019)</li>
<li>Keith Packard (29-11-2013 - 31-12-2017)</li>
<li>Sam Hartman (08-03-2015 - 09-11-2017)</li>
<li>Don Armstrong (11-09-2009 - 31-12-2016)</li>
<li>Andreas Barth (04-01-2006 - 31-12-2016)</li>
<li>Steve Langasek (04-01-2006 - 31-12-2015)</li>
<li>Bdale Garbee (17-04-2001 - 31-12-2015)</li>
<li>Colin Watson (24-08-2011 - 05-03-2015)</li>
<li>Ian Jackson (tot 19-11-2014)</li>
<li>Russ Allbery (11-01-2009 - 16-11-2014)</li>
<li>Manoj Srivasta (tot 12-08-2012)</li>
<li>Anthony Towns (04-01-2006 - 05-01-2009)</li>
<li>Raul Miller (tot 30-04-2007)</li>
<li>Wichert Akkerman (tot 05-01-2006)</li>
<li>Jason Gunthorpe (tot 05-01-2006)</li>
<li>Guy Maor (tot 05-01-2006)</li>
<li>Dale Scheetz (tot 02-09-2002)</li>
<li>Klee Dienes (tot 21-05-2001)</li>

</ul>
