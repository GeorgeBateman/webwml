#use wml::debian::translation-check translation="35022c48a0167d79ad83f69c33eea08bda088d57"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el intérprete del
lenguaje de programación Ruby.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10663">CVE-2020-10663</a>

    <p>Jeremy Evans informó de una vulnerabilidad de creación insegura de objetos en el
    gem json incluido con Ruby. Al analizar sintácticamente ciertos documentos JSON, se
    puede obligar al gem json a crear objetos arbitrarios en el
    sistema objetivo.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10933">CVE-2020-10933</a>

    <p>Samuel Williams informó de un defecto en la biblioteca de sockets que puede dar lugar
    a la revelación de datos del intérprete que podrían ser sensibles.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 2.5.5-3+deb10u2.</p>

<p>Le recomendamos que actualice los paquetes de ruby2.5.</p>

<p>Para información detallada sobre el estado de seguridad de ruby2.5, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/ruby2.5">https://security-tracker.debian.org/tracker/ruby2.5</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4721.data"
