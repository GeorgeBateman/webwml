#use wml::debian::translation-check translation="139ced0522f792565594fd4bc65bf27ae29bd20d" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В BIND, реализации DNS-сервера, было обнаружено несколько
уязвимостей.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6477">CVE-2019-6477</a>

    <p>Было обнаружено, что TCP-конвейеризированные запросы могут обходить ограничения
    для tcp-клиентов, что приводит к отказу в обслуживании.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8616">CVE-2020-8616</a>

    <p>Было обнаружено, что BIND недостаточно ограничивает число вызовов, выполняемых
    при обработке рефералов. Злоумышленник может использовать эту уязвимость
    для вызова отказа в обслуживании (ухудшение производительности) или
    использовать обратный сервер в атаке отражением с высоким коэффициентом
    усиления.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8617">CVE-2020-8617</a>

    <p>Было обнаружено, что логическая ошибка в коде, проверяющем правильность
    TSIG, может использоваться для вызова ошибки утверждения, что приводит
    к отказу в обслуживании.</p></li>

</ul>

<p>В предыдущем стабильном выпуске (stretch) эти проблемы были исправлены
в версии 1:9.10.3.dfsg.P4-12.3+deb9u6.</p>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 1:9.11.5.P4+dfsg-5.1+deb10u1.</p>

<p>Рекомендуется обновить пакеты bind9.</p>

<p>С подробным статусом поддержки безопасности bind9 можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4689.data"
