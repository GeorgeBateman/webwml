#use wml::debian::template title="Contrato Social Debian" BARETITLE=true
#use wml::debian::translation-check translation="c505e01dd6ca2b53d9a229a691d0c2b20c48b36b"

{#meta#:
<meta name="keywords" content="social contract, dfsg, social contract,
dfsg, dfsg, debian social contract, dfsg">
:#meta#}

#  Original document: contract.html
#  Author           : Manoj Srivastava ( srivasta@tiamat.datasync.com )
#  Created On       : Wed Jul  2 12:47:56 1997

<p>
  Versão 1.1 ratificada em 26 de Abril de 2004. Substitui a
  <a href="social_contract.1.0">Versão 1.0</a> ratificada em 5 de Julho de 1997.
</p>

<p>O Projeto Debian, produtor do sistema Debian, criou o
<strong>Contrato Social Debian</strong>. A
<a href="#guidelines">Definição Debian de Software Livre (DFSG)</a>,
parte do contrato, inicialmente concebido como um conjunto de compromissos
públicos que concordamos em respeitar, foi adotada pela
comunidade de software livre como a base para a
<a href="https://opensource.org/docs/osd">Definição Open Source</a>.</p>

<hr />
   <h2><q>Contrato Social perante a Comunidade de Software Livre</q></h2>

   <ol>
     <li>
       <strong>O Debian permanecerá 100% livre</strong>
       <p>
         Disponibilizamos as definições que usamos para determinar se
	 um software é <q><em>livre</em></q> no documento intitulado
	 <q><cite>Definição Debian de Software Livre (DFSG)</cite></q>.
	 Prometemos que o sistema Debian e todos seus componentes serão
	 livres de acordo com essas definições. Iremos fornecer
	 suporte às pessoas que desenvolvem ou usam software livre
	 e não livre no Debian. Nunca faremos o sistema depender
	 de um componente não livre.
       </p>
     </li>
     <li><strong>Iremos retribuir à comunidade software livre</strong>
       <p>
         Quando escrevermos novos componentes do sistema Debian, nós
	 o licenciaremos de um modo consistente com a Definição Debian
	 de Software Livre. Iremos fazer o melhor sistema que pudermos,
	 de modo que o software livre seja amplamente distribuído e utilizado.
	 Iremos comunicar aos(às) <em>autores(as) originais</em>
         (<q>upstream</q>) dos componentes usados em nosso sistema coisas como
         correções de bugs, aperfeiçoamentos e solicitações de usuários(as).
       </p>
     </li>
     <li><strong>Não esconderemos problemas</strong>
       <p>
         Iremos manter nosso banco de dados de relatório de bugs
	 aberto para a visualização pública todo o tempo. Os relatórios
	 que as pessoas preenchem online ficarão visíveis imediatamente
	 para todos as outras pessoas.
       </p>
     </li>
     <li><strong>Nossas prioridades são nossos(as) usuários(as) e o software
     livre</strong>
       <p>
         Nos guiaremos pelas necessidades de nossos(as) usuários(as) e da
	 comunidade software livre. Colocaremos seus interesses em primeiro
	 lugar nas nossas prioridades. Iremos fornecer suporte às
	 necessidades de nossos(as) usuários(as) para que o sistema funcione em diversos
	 tipos de ambientes computacionais. Não faremos objeção a softwares
	 não livres destinados a rodar em sistemas Debian, nem tentaremos
	 cobrar taxa alguma às pessoas que criarem ou utilizarem estes
	 softwares. Permitiremos que outras pessoas criem distribuições
	 contendo o sistema Debian e outros softwares, sem cobrar taxa alguma.
	 Para atender a estes objetivos, disponibilizaremos um sistema
	 integrado, com materiais de alta qualidade, e sem restrições legais
	 que possam impedir tais usos do mesmo.
       </p>
     </li>
     <li><strong>Programas que não atendem nossos padrões de software livre</strong>
       <p>
         Reconhecemos que alguns de nossos(as) usuários(as) precisam usar
	 softwares que não atendem à Definição Debian de Software Livre. Criamos
	 as áreas <q><code>contrib</code></q> e <q><code>non-free</code></q>
	 em nossos repositórios para estes softwares. Os pacotes contidos
	 nessas áreas não são parte do sistema Debian, embora tenham sido
	 configurados para funcionar no Debian. Iincentivamos os fornecedores
	 de CDs a ler as licenças dos pacotes armazenados nessas áreas,
	 a fim de determinar se podem distribuí-los em seus CDs. Portanto,
	 embora softwares não livres não sejam parte do Debian, oferecemos
	 suporte à sua utilização e disponibilizamos infraestrutura para
	 pacotes não livres (como nosso sistema de controle de bugs e listas
	 de discussão).
       </p>
     </li>
   </ol>
<hr />
<h2 id="guidelines">A Definição Debian de Software Livre (DFSG - <em>Debian Free Software Guidelines</em>)</h2>
<ol>
   <li><p><strong>Redistribuição livre</strong></p>
     <p>A licença de um componente Debian não pode restringir nenhuma parte
     interessada em vender ou distribuir o software como parte de uma
     distribuição agregada de software contendo programas de diversas fontes
     diferentes. A licença não pode exigir um royalty (direito autoral) ou outra
     taxa por esta venda.</p></li>
   <li><p><strong>Código-fonte</strong></p>
     <p>O programa deve incluir código-fonte e deve permitir a distribuição em
     código-fonte, bem como em formato compilado.</p></li>
   <li><p><strong>Trabalhos Derivados</strong></p>
     <p>A licença deve permitir modificações e trabalhos derivados, e deve
     permitir que estes sejam distribuídos sob os mesmos termos da licença
     do software original.</p></li>
   <li><p><strong>Integridade do código-fonte do(a) autor(a)</strong></p>
     <p>A licença pode restringir o código-fonte de ser distribuído de forma
     modificada <strong>somente</strong>  se a licença permitir a
     distribuição de <q><tt>arquivos patch</tt></q> com o código-fonte com o
     propósito de modificar o programa em tempo de compilação. A licença
     deve permitir explicitamente a distribuição de software compilado
     a partir do código-fonte modificado. A licença pode exigir que trabalhos
     derivados tenham um nome ou número de versão diferente do software
     original (<em>Isso é um compromisso. O grupo Debian encoraja todos(as)
     os(as) autores(as) a não restringir nenhum arquivo, fonte ou binário, de
     ser modificado</em>).</p></li>
   <li><p><strong>Não à discriminação contra pessoas ou grupos.</strong></p>
     <p>A licença não pode discriminar nenhuma pessoa ou grupo de
     pessoas.</p></li>
   <li><p><strong>Não à discriminação contra fins de utilização</strong></p>
     <p>A licença não pode restringir ninguém de fazer uso do programa para um
     fim específico. Por exemplo, ela não pode restringir o programa de ser
     usado no comércio, ou de ser usado para pesquisa genética.</p></li>
   <li><p><strong>Distribuição de licença</strong></p>
     <p>Os direitos atribuídos ao programa devem aplicar-se a todos aqueles
     para quem o programa é redistribuído, sem a necessidade de execução de
     uma licença adicional por essas partes.</p></li>
   <li><p><strong>A licença não pode ser específica para o Debian</strong></p>
      <p>Os direitos atribuídos ao programa não podem depender do programa ser
      parte de um sistema Debian. Se o programa for extraído do Debian e usado
      ou distribuído sem o Debian, dentro dos termos da licença do programa,
      os mesmos direitos garantidos em conjunto ao sistema Debian deverão ser
      garantidos àqueles que o utilizam.</p></li>
   <li><p><strong>A licença não deve contaminar outros softwares.</strong></p>
      <p>A licença não poderá restringir outro software que é
      distribuído juntamente com o software licenciado. Por exemplo, a
      licença não pode insistir que todos os outros programas distribuídos
      na mesma mídia sejam software livre.</p></li>
   <li><p><strong>Licenças exemplo</strong></p>
      <p>As licenças <q><strong><a href="https://www.gnu.org/copyleft/gpl.html">GPL</a></strong></q>,
      <q><strong><a href="https://opensource.org/licenses/BSD-3-Clause">BSD</a></strong></q> e
      <q><strong><a href="https://perldoc.perl.org/perlartistic.html">Artistic</a></strong></q>
      são exemplos de licenças que consideramos <q><em>livres</em></q>.</p></li>
</ol>

<p><em>O conceito de declarar nosso <q>contrato social para a comunidade de
software livre</q> foi sugerido por Ean Schuessler. O rascunho deste documento
foi escrito por Bruce Perens, refinado por outros(as) desenvolvedores(as) Debian
durante uma conferência via e-mail que durou um mês, em junho de 1997, e então
<a href="https://lists.debian.org/debian-announce/debian-announce-1997/msg00017.html">aceita</a>
como uma política do Projeto Debian, declarada publicamente.</em></p>

<p><em>Mais tarde, Bruce Perens removeu as referências específicas do Debian
da Definição Debian de Software Livre para criar a
<a href="https://opensource.org/docs/definition.php"><q>Definição de
Código Aberto</q></a>.</em></p>

<p><em>Outras organizações podem fazer derivações deste documento. Por favor,
dê o crédito ao Projeto Debian se você fizer isso.</em></p>
