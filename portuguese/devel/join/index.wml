#use wml::debian::template title="Como você pode participar"
#use wml::debian::translation-check translation="6403b81ac51ae4a449df5d367694633c21274e04"

<p>O Projeto Debian consiste de voluntários(as), e nossos produtos são desenvolvidos
inteiramente por voluntários(as). Nós geralmente estamos
<a href="$(HOME)/intro/diversity">buscando por novos(as) contribuidores(as)</a> que têm
algum interesse em software livre, e algum tempo livre.</p>

<p>Se você não fez ainda, você deveria ler a maioria das páginas web para ter
uma melhor compreensão do que nós estamos tentando fazer. Dê uma particular
atenção para <a href="$(HOME)/social_contract#guidelines">A Definição Debian de
Software Livre</a> e o nosso
<a href="$(HOME)/social_contract">Contrato Social</a>.</p>

<p>Um monte de comunicações no projeto acontece em
<a href="$(HOME)/MailingLists/">listas de discussão</a>. Se você quer ter uma
ideia sobre o funcionamento interno do Projeto Debian, você deve pelo menos se
inscrever nas listas debian-devel-announce e debian-news. Ambas têm um volume
muito baixo e registram (em inglês) o que está acontecendo na comunidade. As
notícias do Projeto Debian (publicadas na debian-news) resumem as recentes
discussões das listas e blogs relacionados ao Debian e fornecem links para eles.
Como um(a) potencial desenvolvedor(a), você também deve se inscrever na debian-mentors,
um fórum aberto que tenta ajudar novos(as) mantenedores(as) (e também, embora
menos frequentemente, pessoas que são novas no Projeto e querem ajudar em outras
coisas além da manutenção de pacotes).
Outras listas interessantes são a debian-devel, a debian-project, a debian-release,
a debian-qa, e dependendo do seu interesse, várias outras. Veja a página de
<a href="$(HOME)/MailingLists/subscribe">inscrição em listas de discussão</a>
para uma lista completa.
# removi os parenteses no próxima parágrafo porque não fazem sentido em português
Para aquelas pessoas que desejam reduzir o número de mensagens recebidas, existem
as listas "-digest" apenas para leitura, que são versões resumidas de algumas
listas com alto tráfego.
Também vale a pena saber que você pode usar as páginas dos
<a href="https://lists.debian.org/">arquivos das listas de discussão</a> para
ler as as mensagens de várias listas no seu navegador de internet.</p>

<p>
<b>Contribuindo.</b>
Se você está interessado(a) na manutenção de pacotes, então você deve dar uma
olhada na nossa lista de
<a href="$(DEVEL)/wnpp/">pacotes que precisam de trabalho e futuros pacotes (WNPP - Work-Needing and Prospective Packages)</a>
para ver quais pacotes que precisam de mantenedores(as). Cuidar de um pacote
abandonado é a melhor maneira de começar como um(a) mantenedor(a) &ndash; isso
não apenas ajuda o Debian a manter os seus pacotes bem cuidados, mas dá a você a
oportunidade de aprender com o(a) mantenedor(a) anterior.</p>

<p>Você também pode ajudar contribuindo para a
<a href="$(HOME)/doc/">elaboração de documentação</a>, fazendo a
<a href="$(HOME)/devel/website/">manutenção do site web</a>,
<a href="$(HOME)/intl/">traduzindo </a> (i18n &amp; l10n), fazendo publicidade,
dando suporte legal ou outras funções na comunidade Debian.
Nosso site de <a href="https://qa.debian.org/">controle de qualidade (QA - Quality Assurance)</a>
lista várias outras possibilidades. </p>

<p>Você não precisa ser um(a) desenvolvedor(a) oficial Debian para realizar
praticamente todas essas tarefas. Existem desenvolvedores(as) Debian atuando como
<a href="newmaint#Sponsor">padrinhos/madrinnhas (sponsors)</a> que podem integrar seu
trabalho no projeto. Geralmente é melhor tentar encontrar um(a) desenvolvedor(a)
que está trabalhando na mesma área que você e tem interesse no que você tem
feito.</p>

<p>Finalmente, o Debian provê várias
<a href="https://wiki.debian.org/Teams">equipes</a> de desenvolvedores(as) que
trabalham juntos(as) em tarefas semelhantes. Qualquer pessoa pode participar de
uma equipe, seja um(a) desenvolvedor(a) oficial Debian ou não. Trabalhar junto
com uma equipe é uma excelente maneira de ganhar experiência antes de iniciar o
<a href="newmaint">processo de novo(a) membro(a)</a> e é um dos melhores lugares para
achar padrinhos/madrinnhas de pacotes. Então, encontre uma equipe que
se encaixa no seu interesse e junte-se a ela. </p>

<p>
<b>Participando</b>
Depois de você ter contribuído por algum tempo e as outras pessoas estão certas
do seu envolvimento no Projeto Debian, você pode participar do Debian em uma
função mais oficial. Existem duas funções diferentes em que você pode se juntar
ao Debian:
</p>

<ul>
<li>Mantenedor(a) Debian (<em>DM - Debian Maintainer</em>): o primeiro passo em
que você mesmo(a) pode subir os seus próprios pacotes para o repositório do Debian.</li>
<li>Desenvolvedor(a) Debian (<em>DD - Debian Developer</em>): a tradicional
 função de participação completa no Debian. Um(a) DD pode participar das
 eleições no Debian.
 DDs <em>uploading</em> podem subir qualquer pacote para o repositório.
Antes de se candidatar com um(a) DD <em>uploading</em>, você deve ter uma trilha
   de registros de manutenção de pacotes por pelo menos seis meses. Por exemplo,
   enviando pacotes como um(a) DM, trabalhando dentro de uma equipe, ou mantendo
   pacotes que são enviados por <em>padrinhos/madrinnhas</em>.
DDs <em>non-uploading</em> tem os mesmos direitos de empacotamento que os(as)
mantenedores(as) Debian.
Antes de se candidatar como um(a) DD <em>non-uploading</em>, você deve ter uma
visibilidade e uma significante trilha de registro de contribuições dentro do
projeto.</li>
</ul>

<p>Apesar do fato de que muitos dos direitos e responsabilidades de um(a) DM e de
um(a) DD são idênticos, existem atualmente processos independentes para se
candidatar para cada função. Veja a
<a href="https://wiki.debian.org/DebianMaintainer">página wiki do(a)
mantenedor(a) Debian</a> para detalhes sobre como se tornar um(a) mantenedor(a)
Debian. E veja a página do <a href="newmaint">canto de novos(as) membros(as)</a>
para pesquisar como se candidatar para ter o status de desenvolvedor(a) oficial
Debian.</p>

<p>Note que durante grande parte da história do Debian, a função de
desenvolvedor(a) Debian era a única função; a função de mantenedor(a) Debian foi
criada em 5 de agosto de 2007. Esse é o motivo pelo qual você vê o termo
"mantenedor(a)" usado em um sentido histórico onde o termo desenvolvedor(a) Debian
seria mais preciso. Por exemplo, o processo de candidatura para se tornar um(a)
desenvolvedor(a) Debian ainda era conhecido como o processo de "novo(a)
mantenedor(a) Debian" até 2011, quando ele foi renomeado para o processo de
"novo(a) membro(a) do Debian".</p>

<p>Independentemente de qual função você escolher para se candidatar, você deve
estar familiarizado com os procedimentos do Debian, então é recomendado ler a
<a href="$(DOC)/debian-policy/">política Debian</a> e a
<a href="$(DOC)/developers-reference/">referência para desenvolvedores(as)</a>
antes de se candidatar.</p>

<p>Além dos(as) muitos desenvolvedores(as), há muitos áreas que você
<a href="$(HOME)/intro/help">pode ajudar o Debian</a>, incluindo testes,
documentação, portabilidade, <a href="$(HOME)/donations">doações</a> de
dinheiro e o uso de máquinas para o desenvolvimento e conectividade.
Nós estamos constantemente buscando por <a href="$(HOME)/mirror/">espelhos</a>
em algumas partes do mundo.</p>
