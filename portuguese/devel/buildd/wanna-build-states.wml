#use wml::debian::template title="Estados wanna-build: uma explicação" BARETITLE="true"
#use wml::debian::translation-check translation="b8114b588961778dbd04974c1464a2f388a90c28"

    <p>Esta página tenta explicar o que significa cada estado wanna-build e o
      que acontecerá com um pacote quando ele
      estiver nesse estado. Sua audiência-alvo é mantenedores(as) de
      pacotes Debian que tentam entender porque seu pacote foi, ou não foi,
      construído para uma determinada arquitetura. Além disso, é dada uma
      explicação dos diferentes resultados de log.</p>

    <p>Finalmente, uma versão em fluxograma dos estados wanna-build está
      <a href="#graphlink">disponível</a>, mas note que ele não fala sobre
      tudo que é mencionado neste documento.</p>

<h2>Os estados wanna-build</h2>
<p>Para toda arquitetura suportada pelo Debian, existe um banco de  dados
wanna-build instalado em buildd.debian.org, com todos os pacotes e seus estados
atuais de compilação. Existem 8 estados: <em>needs-build</em>,
<em>building</em>, <em>uploaded</em>, <em>dep-wait</em>,
<em>BD-Uninstallable</em>, <em>failed</em>, <em>not-for-us</em> e
<em>installed</em>.</p>

<p>Seus significados:</p>
    <dl>
      <dt><a name="needs-build">needs-build</a></dt>
      <dd>Um pacote marcado <em>needs-build</em> teve uma
        nova versão enviada pelo(a) mantenedor(a), mas para uma arquitetura
        diferente daquela que este banco de dados wanna-build dirige-se;
        como tal, ele precisa de uma reconstrução. Se o estado é
	<em>needs-build</em>, ele ainda não foi pego por um autobuilder, mas
	será (quando um esteja disponível no momento que tal
        pacote esteja próximo ao topo da lista). As pessoas normalmente dizem
        que <q>um pacote está na fila para reconstruir</q> quando elas estão
	falando sobre um pacote no estado <em>needs-build</em>.<br />
	Pode ser interessante notar que a fila <em>needs-build</em>
	não é uma fila FIFO; ao contrário, a ordenação utilizada é baseada
	nos seguintes critérios:
	<ol>
	  <li>Estados anteriores de compilação dos pacotes; os pacotes que
	    foram previamente construídos recebem prioridade sobre novos
	    pacotes.
	  </li>
	  <li>prioridades (pacotes com prioridade <em>required</em> - requerido
            - são construídos antes que pacotes com prioridade <em>extra</em>)
	  </li>
	  <li>A seção em que um pacote está. Este ordenamento é baseado naquilo
	    que se considera mais importante sobre os pacotes; por exemplo, a
            seção <em>games</em> é construída depois da seção <em>base</em>, e
            a seção <em>libs</em> é construída antes de <em>devel</em>.
	  </li>
	  <li>uma ordem "asciibética" pelo nome do pacote.</li>
	</ol>
        Adicionalmente, sob certas condições, pode acontecer que um buildd
        não receba pacotes do começo da fila; por exemplo,
        quando um buildd não consegue encontrar a fonte de um dado pacote, ele
        o colocará de volta na fila (onde então ele será novamente colocado
        em sua posição anterior, isto é, no começo da fila), mas ele ignorará
        o pacote por algumas horas. Outro exemplo onde isto poderia acontecer
        é quando uma arquitetura tem múltiplos autobuilders;
        neste caso, as pessoas que portam a arquitetura podem escolher construir
        pacotes maiores em seus autobuilders mais rápidos e deixar os
        pacotes menores para as máquinas mais lentas do grupo. Um buildd também
        pode, em tese, requisitar explicitamente uma ordenação de seção
        diferente, mas isto geralmente não é feito.<br />
	Poderiam existir outras situações onde a ordem da fila parece ser
        ignorada; mas note que são sempre exceções.
      </dd>
      <dt><a name="building">building</a></dt>
      <dd>Um pacote é marcado com <em>building</em> do momento em
	que um autobuilder o pega do topo da fila wanna-build
	até o momento em que a administração do autobuilder responde ao log.
	Como os pacotes não são escolhidos um a um, isto significa que um pacote
	pode ser (e geralmente é) marcado com <em>building</em> antes que a
	construção tenha realmente iniciado; contudo, como o buildd constrói
	pacotes em sua fila local com base em FIFO, não deve demorar muito.
	Além disso, note que o estado de um pacote <strong>não</strong> é
        modificado um vez que a construção foi completada; somente
	quando a administração do autobuilder aparece para responder aos
	logs.</dd>
      <dt><a name="uploaded">uploaded</a></dt>
      <dd>Quando uma tentativa de construção é bem sucedida, um log de
	construção é enviado para a administração do autobuilder e para
	buildd.debian.org. Então o(a) mantenedor(a) do autobuilder assina o
	arquivo .changes que está embutido no log de construção e o envia para
	o autobuilder. Como reação, o autobuilder fará o upload do
	pacote e definirá seu estado para <em>uploaded</em>. Como tal, um
	pacote neste estado pode ser encontrado na fila do repositório de
	entrada (incoming) (em algum lugar).<br />
	Um autobuilder não tocará mais em um pacote uma vez que seu estado
	é <em>uploaded</em>, ou pelo menos não o fará até o próximo upload
	ou até que a equipe de porte manualmente modifique o estado do pacote.
      </dd>
      <dt><a name="dep-wait">dep-wait</a></dt>
      <dd>Quando um pacote falha devido à ausência de dependências durante o
	tempo de construção (build-time), o(a) mantenedor(a) do autobuilder
	enviará um e-mail para o aautobuilder, instruindo-o a remover as
	fontes do pacote e a marcar o pacote como <em>dep-wait</em>
	para as dependências de construção (build-dependencies). Um pacote em
	tal estado, automaticamente e sem
	intervenção humana, será marcado com needs-build quando essas
	dependências estiverem disponíveis.<br />
        Originalmente, um pacote tinha que passar por uma tentativa de
        construção antes que o(a) mantenedor(a) pudesse colocá-lo manualmente
        no estado <em>dep-wait</em>. Contudo, em agosto de 2005, alguns códigos
        foram adicionados para o wanna-build, os quais fazem com que um pacote
        se mova do estado <em><a href='#installed'>installed</a></em>
	diretamente para o estado <em>dep-wait</em> se isso for
        apropriado.<br />
        Existem dois casos específicos em que pode acontecer que um
        pacote fique marcado como dep-wait para sempre; quando um erro de
        digitação ocorre ao especificar as dependências <em>dep-wait</em>
        (de modo que o pacote é marcado como dep-wait para um pacote que não
        existe e nunca existirá) e quando uma dependência de tempo de construção
        é declarada em um pacote que está marcado como <em>not-for-us</em>,
	que está na lista <em>packages-arch-specific</em>.<br />
        Como um exemplo deste último caso, considere três pacotes: um pacote
  	<tt>foo</tt>, que existe para <tt>i386</tt> somente; um pacote
	<tt>bar</tt>, que existe para <tt>m68k</tt> somente (e que realiza
	aproximadamente a mesma função); e um pacote <tt>baz</tt>, que pode ser
	construído com um deles, <tt>foo</tt> ou <tt>bar</tt>. Se o(a)
	mantenedor(a) do pacote <tt>baz</tt> esquecer de adicionar <tt>bar</tt>
	ao Build-Depends, e se ela ou ele adicioná-lo quando notificar-se que
	<tt>baz</tt> está em <em>dep-wait</em> para um pacote <tt>foo</tt> não
	existente para <tt>m68k</tt>, então o estado <em>dep-wait</em> para
	<tt>m68k</tt> terá que ser manualmente retirado pela equipe de porte
        do <tt>m68k</tt>.
      </dd>
      <dt><a name="bd-uninstallable">BD-Uninstallable</a></dt>
      <dd>Durante a DebConf9, o <a
	href='https://lists.debian.org/debian-wb-team/2009/07/msg00089.html'>Joachim
	Breitner teve a ideia</a> de usar edos-debcheck para verificar
        a instalabilidade de dependências de construção (build-dependency) dos
        pacotes que, de outro modo, iriam para o estado Needs-Build. Naquele
        momento, o wanna-build já tinha a capacidade de verificar a
        disponibilidade imediata de dependências de construção; mas se um
        pacote não pudesse ser instalado porque, na construção, ele depende de
        a, que depende de b, que depende de c (&gt;=1.2.3), e c ainda está na
        versão 1.2.2, isto não seria detectado e a construção logo falharia
        devido a dependências de construção não disponíveis. Essa descoberta
        era um processo manual para a administração do buildd e, geralmente,
        era um processo demorado. Com o patch BD-Uninstallable, não é mais
        um problema. Quando seu pacote está em BD-Uninstallable, significa que
        uma das dependências de construção não é instalável (imediatamente ou
        porque parte de sua árvore de dependências não está disponível).
        Infelizmente, o patch BD-Uninstallable não fornece
        informações sobre qual pacote, exatamente, está faltando;
        por favor, use edos-debcheck para descobrir. Entretanto, este problema
        será resolvido por si mesmo uma vez que as dependências ausentes estejam
        de fato disponíveis e, neste momento, seu pacote automaticamente se
        moverá para Needs-Build novamente.
      </dd>
      <dt><a name="wanna-build-state-failed">failed</a></dt>
      <dd>Se uma tentativa de construção falhou, e o(a) mantenedor(a) do
	autobuilder decidir que é mesmo uma falha cuja tentativa não deve ser
	repetida, um pacote é marcado como <em>failed</em>. Um pacote
	não sairá deste estado até que alguém do porte decida que isso ocorra ou
	até que uma nova versão esteja disponível. Contudo, quando uma nova
	versão de um pacote está disponível, o qual foi marcado como
	<em>failed</em> em uma versão anterior, o autobuilder perguntará para
	sua administração se o pacote deve ser tentado novamente ou não; isso
	é feito para que os pacotes que obviamente falharão novamente não gastem
	o tempo do buildd. Embora falhar um pacote antes de tentar uma
	construção dificilmente seja a coisa certa a se fazer, a opção está
	disponível para a administração do autobuilder.<br />
	Note que o pacote <strong>nunca</strong> será marcado como
	<em>failed</em> sem intervenção humana
      </dd>
      <dt><a name="not-for-us">not-for-us</a></dt>
      <dd>Certos pacotes são específicos a uma arquitetura; por
        exemplo, o <tt>lilo</tt>, um boot loader do i386, não deve ser
        reconstruído em alpha, m68k ou s390. Contudo, o <em>wanna-build</em> não
        olha para o arquivo control de um pacote quando cria seu banco
        de dados; somente para o nome e a seção do pacote, o estado anterior
        de construção e sua prioridade. Desse modo, quando ocorre o primeiro
        upload de um pacote de arquitetura específica que não deve ser
        construído em outras arquiteturas, uma tentativa de construção é
        ainda assim realizada (mas falha mesmo antes que as dependências
        de tempo de construção sejam baixadas e/ou instaladas).<br />
        Já que os autobuilders não devem perder tempo tentando
        construir pacotes que não são requisitados para sua arquitetura,
        não existe necessidade de uma listagem de pacotes os quais uma
        única tentativa de construção não é requerida. A primeira solução
        para este problema foi o <em>not-for-us</em>; contudo,
        como é difícil de manter, o <em>not-for-us</em> está atualmente
        obsoleto; mantenedores(as) de autobuilders devem usar
	<em>packages-arch-specific</em>, que é uma lista de pacotes específicos
        para uma ou mais arquiteturas em vez de um estado wanna-build.<br />
        Um pacote em <em>not-for-us</em> ou <em>packages-arch-specific</em>
	<strong>não</strong> sairá deste estado automaticamente; se previamente
        seu pacote excluiu uma dada arquitetura em seu arquivo control,
        mas agora inclui mais arquiteturas, ele deve ser
	<strong>manualmente</strong> recolocado na fila.<br />
        Se alguma vez você se encontrar em uma posição em que tenha que pedir
        para que isto aconteça, você deve fazê-lo perguntando ao(à)
        mantenedor(a) do buildd relevante. Eles(as) podem ser encontrados(as)
        em $arch@buildd.debian.org.
      </dd>
      <dt><a name="installed">installed</a></dt>
      <dd>Como o nome sugere, um pacote marcado com <em>installed</em>
        (instalado) está compilado para a arquitetura apontada pelo banco de
        dados wanna-build. Antes do Woody ter sido lançado, um estado de pacote
        mudava de <em>uploaded</em> para <em>installed</em> após a execução
        diária do katie. Com a implementação do <a
	  href="https://lists.debian.org/debian-devel-announce/2002/debian-devel-announce-200206/msg00002.html">Accepted-autobuild</a>,
	contudo, isto não é mais verdade; hoje em dia, um pacote vai
	do estado <em>uploaded</em> para <em>installed</em> quando ele é
	aceito no repositório. Isto significa que um pacote é geralmente
	marcado como <em>installed</em> após 15 minutos, na
	média.
      </dd>
    </dl>
    <p>Além desses oito estados, o <em>wanna-build</em> também
    conhece dois estados -removed, que realmente são casos
    excepcionais. Esses dois estados são <em>dep-wait-removed</em> e
    <em>failed-removed</em>. Eles se relacionam aos seus respectivos estados
    <q>normais</q> dessa forma: quando um pacote no estado <em>failed</em> ou
    <em>dep-wait</em> não aparece em um novo arquivo Packages, que é
    alimentado para <em>wanna-build</em> &ndash; quando aparece, já foi
    removido  &ndash; a informação sobre aquele pacote não é jogada
    fora como poderia ser feito, já que o pacote que não aparece no
    arquivo Packages é somente uma pequena falha temporária, ou aquele
    pacote foi temporariamente removido por alguma razão (mas vai reaparecer
    no repositório depois de algum tempo). Em vez disso, em tal caso, um
    pacote é movido para um estado <em>-removed</em> para que a
    informação sobre o porquê ele falhou ou o que está aguardando possa
    ser retida. Caso o pacote reapareça em um próximo arquivo Packages
    que é alimentado no wanna-build, ele então será movido de volta de
    <em>failed-removed</em> para <em>failed</em>, ou de volta de
    <em>dep-wait-removed</em> para <em>dep-wait</em> antes de novos
    processamentos.</p>
    <p>
      Não é possível acessar o banco de dados wanna-build diretamente;
      este banco de dados é instalado em ftp-master.debian.org, que é um
      host restrito, e somente autobuilders tem uma chave SSH que
      permite que eles acessem o banco de dados wanna-build para suas
      arquiteturas.  Isto tem sido a norma desde antes de ftp-master ter sido
      restringido; como o wanna-build faz um bloqueio em nível de banco de
      dados quando acessado, mesmo na leitura de dados, você tem que estar
      no grupo correto (wb-&lt;arch&gt;) para ser capaz de acessar diretamente
      um banco de dados wanna-build.
    </p>
    <p>Dito isso, você pode ver qual estado um pacote está
      na <a href="https://buildd.debian.org/stats/">página de
      estatísticas do buildd</a>, exceto se ele está no estado
      <em>installed</em> (bem, é possível, se você não liga de escavar
        multimegabytes através de arquivos "&lt;arch&gt;-all.txt" ...).
    </p>
    <h2>Os resultados do log de construção</h2>
    <p>
      Quando um pacote é construído por sbuild (o componente do buildd que
      realmente faz a construção), um log com o resultado da construção é
      enviado, por e-mail, para a administração do autobuilder e para
      logs@buildd.debian.org (de modo que ele chegue até
      https://buildd.debian.org). O resultado do log de construção pode ser
      <em>successful</em>, <em>attempted</em> (antes conhecido por
      <em>failed</em>), <em>given-back</em> ou
      <em>skipped</em>. Note que na <a
      href="https://buildd.debian.org/">página de síntese do log do
      buildd</a>, o prefixo <em>maybe-</em> é adicionado, porque entre
      outras coisas, o fato de que uma construção possa ser marcada com
      <em>failed</em> para coisas que não são <em>realmente</em> uma
      falha causou confusão no passado (ou, por outro lado,
      algumas vezes um pacote que aparentemente construiu com sucesso está
      realmente quebrado e precisa ser reconstruído).</p>
    <p>O significado dos resultados do log estão a seguir:</p>
    <dl>
      <dt><a name="successful">successful</a></dt>
      <dd>A construção foi bem sucedida. Quando o(a) mantenedor(a) do
        autobuilder recebe este log, ele(a) extrairá o arquivo
        <code>.changes</code> embutido, assinará e o enviará de volta para
	o autobuilder, que fará o upload do pacote.</dd>
      <dt><a name="failed">attempted</a> (antes: failed)</dt>
      <dd>A construção terminou com um estado de saída não zero, indicando que
	provavelmente falhou. Como pode haver um bom número de razões da falha
	da construção, enumerá-las todas seria tedioso, então não vamos tentar
        fazer isso aqui. Se um pacote seu está marcado com
        <em>(maybe-)failed</em>, você vai querer ler o que escrevemos acima e
	verificar o atual estado wanna-build.
      </dd>
      <dt><a name="given-back">given-back</a></dt>
      <dd>A construção falhou devido a um problema temporário com o
	autobuilder; exemplos incluem problemas de rede, a
	indisponibilidade do fonte do pacote com o atual
	sources.list, pouco espaço em disco, e outros.<br />
	Um pacote que está com <em>given-back</em> é marcado com
	<em><a href="#needs-build">needs-build</a></em> novamente; como
	tal, ele será automaticamente escolhido por um autobuilder
	diferente quando estiver pronto.
      </dd>
      <dt><a name="skipped">skipped</a></dt>
      <dd>No período entre o pacote ter sido escolhido por um
	autobuilder e marcado com <em><a
	    href="#building">building</a></em>, e a tentativa de construção,
	    uma nova versão para este pacote foi enviada, ou a equipe de porte
	    manualmente modificou o estado wanna-build por alguma outra razão.
	    Quando estiver pronto, um e-mail é enviado para o
            autobuilder, que marcará o pacote para não ser construído; o
            sbuild verá e ignorará a construção (embora o log de construção
            com este resultado seja enviado, descrevendo o fato que fez isso
            acontecer).
      </dd>
    </dl>

<h2><a name="graphlink">A versão gráfica</a></h2>
<p>Para ilustrar o que foi escrito acima, nós também providenciamos uma <a
href="wanna-build.png">versão-fluxograma</a> deste procedimento. Novamente,
note que ele não contém tudo o que foi mencionado neste documento.
</p>
