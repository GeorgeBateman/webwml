#use wml::debian::template title="Debian 11 -- Relatando problemas" BARETITLE=true
#use wml::debian::translation-check translation="c1221b640653be886b43ce18e8e72c5516aa770f"

# Tradutores: cópia de buster/reportingbug

<h2><a name="report-release">Com as notas de lançamento</a></h2>

<p>Erros nas <a href="releasenotes">notas de lançamentos</a> devem ser
<a href="$(HOME)/Bugs/Reporting">reportados como bug</a> contra o
pseudopacote <tt>release-notes</tt>. A discussão desse documento é
coordenada através da lista principal do debian-doc em
<a href="mailto:debian-doc@lists.debian.org">\
&lt;debian-doc@lists.debian.org&gt;</a>. Se você tiver problema com o documento
que não é apropriado para um bug, você deve enviar um e-mail para a lista.
</p>

<h2><a name="report-installation">Com a instalação</a></h2>

<p>Se você tiver algum problema com o sistema de instalação, por favor relate o
bug contra o pacote <tt>installation-reports</tt>. Preencha o
<a href="$(HOME)/releases/stable/i386/ch05s04#submit-bug">modelo de relatório</a>
para garantir que você incluiu todas as informações necessárias.</p>

<p>Se você tiver sugestões ou correções para o
<a href="installmanual">manual de instalação</a>, você deve
<a href="$(HOME)/Bugs/Reporting">enviá-las
como bugs</a> contra o pacote <tt>installation-guide</tt>, que é o
pacote-fonte onde essa documentação é mantida.</p>

<p>Se você tiver problemas com o sistema de instalação que não sejam
apropriados para um bug (por exemplo, você não tem certeza se realmente é um bug
ou não, a parte do sistema com o problema não está clara, etc),
você provavelmente deve mandar enviar um e-mail (em inglês) para a
lista de discussão,
<a href="mailto:debian-boot@lists.debian.org">\
&lt;debian-boot@lists.debian.org&gt;</a>.</p>

<h2><a name="report-upgrade">Com uma atualização</a></h2>

<p>Se você tiver problemas ao atualizar seu sistema a partir de versões
anteriores, por favor registre um bug contra o pacote <tt>upgrade-reports</tt>,
que é o pseudopacote usado para acompanhar essas informações. Para mais
informações sobre como enviar relatórios de atualização, por favor leia as
<a href="releasenotes">notas de lançamentos</a>.</p>

<h2><a name="report-package">Quaisquer outros problemas</a></h2>

<p>Se você tiver problemas com o sistema após a instalação, tente
encontrar qual é o pacote com problemas e
<a href="$(HOME)/Bugs/Reporting">registrar um bug</a> contra este pacote.</p>
