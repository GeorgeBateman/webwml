#use wml::debian::template title="Portes"
#use wml::debian::translation-check translation="c7cb55879eeda07cd289f646598ca02afb6133cc"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::toc

<toc-display/>

<toc-add-entry name="intro">Introdução</toc-add-entry>
<p>
 Como a maioria de vocês sabe, o <a href="https://www.kernel.org/">Linux</a>
 é apenas um kernel (núcleo). E durante muito tempo
 o kernel Linux funcionou somente nas máquinas da série x86 da Intel, desde
 o 386.
</p>
<p>
 No entanto, atualmente a realidade é outra. O kernel Linux tem
 sido portado para uma grande e crescente lista de arquiteturas.
 E, sem demora, nós também temos portado a distribuição Debian para essas
 arquiteturas. Em geral, este é um processo com um início lento (enquanto
 colocamos a libc e o ligador dinâmico para funcionar tranquilamente), para
 então passarmos para um trabalho relativamente rotineiro, embora demorado,
 de tentar recompilar todos os nossos pacotes nas novas arquiteturas.
</p>
<p>
 O Debian é um sistema operacional (SO), não um kernel (na verdade, é mais do
 que um SO, pois inclui milhares de programas aplicativos). Nesse sentido,
 enquanto a maioria dos portes Debian são baseados no Linux, também existem
 portes baseados nos kernels FreeBSD, NetBSD e Hurd.
</p>

<div class="important">
<p>
 Esta é uma página em progresso. Nem todos os portes possuem
 páginas e a maioria está em sites externos. Estamos trabalhando para
 coletar informações sobre todos os portes, para então serem espelhadas
 juntamente com o site do Debian.
 Mais portes estão <a href="https://wiki.debian.org/CategoryPorts">listados</a>
 na wiki do Debian.
</p>
</div>

<toc-add-entry name="portlist-released">Lista de portes oficiais</toc-add-entry>
<br />

<table class="tabular" summary="">
<tbody>
<tr>
<th>Porte</th>
<th>Arquitetura</th>
<th>Descrição</th>
<th>Estado</th>
</tr>
<tr>
<td><a href="amd64/">amd64</a></td>
<td>PC de 64 bits (amd64)</td>
<td>Lançado oficialmente pela primeira vez para o Debian 4.0. Porte para os
processadores x86 de 64 bits. O objetivo é oferecer suporte para programas do
espaço de usuário em 32 bits e 64 bits nesta arquitetura. Este porte dá suporte
para os processadores de 64 bits Opteron, Athlon e Sempron da AMD e
processadores da Intel com suporte a Intel 64, incluindo o Pentium D e várias
séries Xeon e Core.
<td><a href="$(HOME)/releases/stable/amd64/release-notes/">lançado</a></td>
</tr>
<tr>
<td><a href="arm/">arm64</a></td>
<td>ARM de 64 bits (AArch64)</td>
<td>A versão 8 da arquitetura ARM inclui o AArch64, um novo sistema
com conjunto de instruções em 64 bits. Desde o Debian 8.0, o porte arm64 foi
incluso no sistema a fim de oferecer suporte às novas instruções contidas em
processadores como o Applied Micro X-Gene, AMD Seattle e Cavium ThunderX.</td>
<td><a href="$(HOME)/releases/stable/arm64/release-notes/">lançado</a></td>
</tr>
<tr>
<td><a href="arm/">armel</a></td>
<td>EABI ARM</td>
<td>O mais antigo dos portes ARM do Debian atualmente oferece suporte a
CPUs ARM little-endian compatíveis com o conjunto de instruções v5te.</td>
<td><a href="$(HOME)/releases/stable/armel/release-notes/">lançado</a></td>
</tr>
<tr>
<td><a href="arm/">armhf</a></td>
<td>Hard Float ABI ARM</td>
<td>Muitas placas e dispositivos ARM de 32 bits modernos são fornecidos com uma
unidade de ponto flutuante (FPU), mas o porte armel do debian não tira muita
vantagem dele. O porte armhf foi iniciado para melhorar essa situação e também
ter vantagem em outros recursos das CPUs ARM mais recentes. O porte
armhf do Debian requer pelo menos uma CPU AMRv7 com suporte às unidades
de ponto flutuante Thumb-2 e VFPv3-D16.</td>
<td><a href="$(HOME)/releases/stable/armhf/release-notes/">lançado</a></td>
</tr>
<tr>
<td><a href="i386/">i386</a></td>
<td>PC de 32 bits (i386)</td>
<td>A primeira arquitetura, e não estritamente um porte. O Linux foi
originalmente desenvolvido para processadores Intel 386, por isso o nome
abreviado. O Debian oferece suporte a todos os processadores IA-32, fabricados
pela Intel (incluindo toda a série Pentium e as máquinas Core Duo recentes em
modo 32-bit), AMD (K6 e todas as séries Athlon e séries Athlon64 em modo
32-bit), Cyrix e outros fabricantes.</td>
<td><a href="$(HOME)/releases/stable/i386/release-notes/">lançado</a></td>
</tr>
<tr>
<td><a href="mips/">mipsel</a></td>
<td>MIPS (modo little-endian)</td>
<td>Lançado oficialmente pela primeira vez com o Debian 3.0. O Debian está sendo
portado para a arquitetura MIPS que é usada em máquinas SGI (debian-mips —
big-endian) e Digital DECstations (debian-mipsel — little-endian).</td>
<td><a href="$(HOME)/releases/buster/mipsel/release-notes/">lançado</a></td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/mips64el">mips64el</a></td>
<td>MIPS (modo little-endian de 64 bits)</td>
<td>
Este porte é little-endian, utiliza a ABI N64, o MIPS64r1 ISA e hardware de ponto
flutuante. Parte do lançamento oficial desde o Debian 9.
</td>
<td><a href="$(HOME)/releases/stable/mips64el/release-notes/">lançado</a></td>
</tr>
<tr>
<td><a href="powerpc/">ppc64el</a></td>
<td>POWER7+, POWER8</td>
<td>Lançado oficialmente com o Debian 8.0. Porte little-endian de ppc64
que utiliza a nova Open Power ELFv2 ABI.</td>
<td><a href="$(HOME)/releases/stable/ppc64el/release-notes/">lançado</a></td>
</tr>
<tr>
<td><a href="s390x/">s390x</a></td>
<td>System z</td>
<td>Lançado oficialmente com o Debian 7.0. Um espaço de usuário de 64 bits para
mainframes IBM System z.</td>
<td><a href="$(HOME)/releases/stable/s390x/release-notes/">lançado</a></td>
</tr>
</tbody>
</table>

<toc-add-entry name="portlist-other">Lista de outros portes</toc-add-entry>

<div class="tip">
<p>
 Existem imagens de instalação não oficiais disponíveis para alguns dos
 seguintes portes no
 <url "https://cdimage.debian.org/cdimage/ports"/>.
 Essas imagens são mantidas pelos times dos portes correspondentes do Debian.
</p>
</div>

<table class="tabular" summary="">
<tbody>
<tr>
<th>Porte</th>
<th>Arquitetura</th>
<th>Descrição</th>
<th>Estado</th>
</tr>
<tr>
<td><a href="alpha/">alpha</a></td>
<td>Alpha</td>
<td>Lançado oficialmente pela primeira vez com o Debian 2.1.
Ele não atendeu aos critérios de inclusão no lançamento do Debian 6.0 <q>squeeze</q>
e, portanto, foi removido dos arquivos.
</td>
<td>descontinuado</td>
</tr>
<tr>
<td><a href="arm/">arm</a></td>
<td>OABI ARM</td>
<td>Este porte é executado em uma variedade de hardware embutidos, como
roteadores ou dispositivos NAS. O porte arm foi lançado pela primeira vez com o
Debian 2.2 e o suporte foi incluído até o Debian 5.0, onde foi substituído pelo
armel.
</td>
<td>substituído pelo armel</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20130326061253/http://avr32.debian.net/">AVR32</a></td>
<td>Atmel RISC de 32 bits</td>
<td>Porte para a arquitetura Atmel RISC de 32 bits, AVR32. </td>
<td>descontinuado</td>
</tr>
<tr>
<td><a href="hppa/">hppa</a></td>
<td>HP PA-RISC</td>
<td>Lançado oficialmente com o Debian 3.0 <q>woody</q>, este é o porte
para a arquitetura PA-RISC da Hewlett-Packard.
Ele não atendeu aos critérios de inclusão no lançamento do Debian 6.0 <q>squeeze</q>
e, portanto, foi removido dos arquivos.
</td>
<td>descontinuado</td>
</tr>
<tr>
<td><a href="hurd/">hurd-i386</a></td>
<td>PC de 32 bits (i386)</td>
<td> O GNU Hurd é um sistema operacional novo que é desenvolvido
pelo grupo GNU.
O Debian GNU/Hurd será
um (possivelmente o primeiro) sistema operacional GNU. O projeto atual é
baseado na arquitetura i386.
</td>
<td>em progresso</td>
</tr>
<tr>
<td><a href="ia64/">ia64</a></td>
<td>Intel Itanium IA-64</td>
<td>Lançada oficialmente pela primeira vez com o Debian 3.0. Este é um porte da
primeira arquitetura de 64 bits da Intel. Nota: este não deve ser confundido com
as últimas extensões de 64 bits da Intel para os processadores Pentium 4 e Celeron,
chamadas Intel 64; para estes veja o porte AMD64. Com o Debian 8, o porte para
ia64 foi removido da versão devido ao suporte insuficiente de desenvolvimento.</td>
<td>descontinuado</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-amd64</a></td>
<td>PC de 64 bits (amd64)</td>
<td>Lançado oficialmente pela primeira vez com o Debian 6.0 como uma prévia
da tecnologia, também foi o primeiro porte não Linux lançado pelo Debian. O
porte do sistema Debian GNU para o kernel FreeBSD não faz mais parte do
lançamento oficial desde o Debian 8.</td>
<td>em progresso</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-i386</a></td>
<td>PC de 32 bits (i386)</td>
<td>Lançado oficialmente pela primeira vez com o Debian 6.0 como uma prévia
da tecnologia, também foi o primeiro porte não Linux lançado pelo Debian. O
porte do sistema Debian GNU para o kernel FreeBSD não faz mais parte do
lançamento oficial desde o Debian 8.</td>
<td>em progresso</td>
</tr>
<tr>
<td><a href="http://www.linux-m32r.org/">m32</a></td>
<td>M32R</td>
<td>Porte para o microprocessador RISC de 32 bits da Renesas Technology.</td>
<td>encerrado</td>
</tr>
<tr>
<td><a href="m68k/">m68k</a></td>
<td>Motorola 68k</td>
<td>Lançado oficialmente pela primeira vez com o Debian 2.0. O porte não atendeu
aos critérios de lançamento para o Debian 4.0 e, portanto, não foi incluído no
lançamento do Etch e das versões posteriores. Foi movido para o debian-ports
depois disso.
O porte Debian m68k é executado em uma ampla variedade de
computadores baseados na série de processadores Motorola m68k — em particular,
as estações de trabalho Sun3, os computadores pessoais Apple Macintosh e os
computadores pessoais Atari e Amiga.</td>
<td>em progresso</td>
</tr>
<tr>
<td><a href="mips/">mips</a></td>
<td>MIPS (modo big-endian)</td>
<td>Lançado oficialmente pela primeira vez com o Debian 3.0. O Debian está sendo
portado para a arquitetura MIPS que é usada em máquinas SGI (debian-mips —
big-endian) e Digital DECstations (debian-mipsel — little-endian).
O porte do Debian para a arquitetura MIPS foi descontinuado após o lançamento do
<a href="$(HOME)/releases/stable/mips/release-notes/">Debian 10 (Buster)</a>.</td>
<td><a href="https://lists.debian.org/debian-release/2019/08/msg00582.html">descontinuado</a></td>
</tr>
<tr>
<td><a href="netbsd/">netbsd-i386</a></td>
<td>PC de 32 bits (i386)</td>
<td>Um porte do sistema operacional Debian completo com apt, dpkg, e espaço de
usuário GNU, para o kernel NetBSD. O porte nunca foi lançado e
foi abandonado.</td>
<td>encerrado</td>
</tr>
<tr>
<td><a href="netbsd/alpha/">netbsd-alpha</a></td>
<td>Alpha</td>
<td>Um porte do sistema operacional Debian completo com apt,
dpkg, e espaço de usuário GNU para o kernel NetBSD. O porte nunca foi lançado e
foi abandonado.</td>
<td>encerrado</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20150905061423/http://or1k.debian.net/">or1k</a></td>
<td>OpenRISC 1200</td>
<td>Um porte para a CPU de código aberto <a href="https://openrisc.io/">OpenRISC</a> 1200.</td>
<td>encerrado</td>
</tr>
<tr>
<td><a href="powerpc/">powerpc</a></td>
<td>Motorola/IBM PowerPC</td>
<td>Lançado oficialmente pela primeira vez com o Debian 2.2. Este porte é
executado em vários modelos Apple Macintosh PowerMac e em máquinas de arquitetura
aberta CHRP e PReP. Não faz mais parte do lançamento oficial desde o Debian 9.</td>
<td>descontinuado</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/PowerPCSPEPort">powerpcspe</a></td>
<td>PowerPC Signal Processing Engine</td>
<td>
Um porte para o hardware "Signal Processing Engine" presente em processadores de
32 bits "e500" de baixo consumo da Freescale e IMB.
</td>
<td>em progresso</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/RISC-V">riscv64</a></td>
<td>RISC-V (little endian de 64 bits)</td>
<td>Porte para o <a href="https://riscv.org/">RISC-V</a>, um barramento ISA
livre/aberto, em particular para a variante little-endian de 64 bits.</td>
<td>em progresso</td>
</tr>
<tr>
<td><a href="s390/">s390</a></td>
<td>S/390 e zSeries</td>
<td>Lançado oficialmente pela primeira vez com o Debian 3.0. Este é um porte para
os servidores S/390 da IBM. Foi substituído pelo s390x com o lançamento do Debian 8.</td>
<td>substituído pelo s390x</td>
</tr>
<tr>
<td><a href="sparc/">sparc</a></td>
<td>Sun SPARC</td>
<td>Lançado oficialmente pela primeira vez com o Debian 2.1, este porte é
executado na série de máquinas de trabalho UltraSPARC, bem como alguns de seus
sucessores nas arquiteturas sun4. Desde o lançamento do Debian 8 Sparc não
existem mais lançamentos para essa arquitetura, devido ao suporte insuficiente
de desenvolvimento. No entanto, ele será substituído pelo Sparc64 em breve.
</td>
<td>será substituído pelo sparc64</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/Sparc64">sparc64</a></td>
<td>SPARC de 64 bits</td>
<td>
Um porte para processadores SPARC de 64 bits.
</td>
<td>em progresso</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/SH4">sh4</a></td>
<td>SuperH</td>
<td>
Um porte para processadores Hitachi SuperH. Também suporta o processador de
código aberto <a href="https://j-core.org/">J-Core</a>.
</td>
<td>em progresso</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/X32Port">x32</a></td>
<td>PC de 64 bits com ponteiros de 32 bits</td>
<td>
X32 é uma ABI para CPUs amd64/x86_64 que utilizam ponteiros de 32 bits.
A ideia é combinar o maior conjunto de registros
de x86_64 com a menor memória e o cache resultante dos ponteiros de 32 bits.
</td>
<td>em progresso</td>
</tr>
</tbody>
</table>



<div class="note">
<p>Muitos dos nomes de computadores e processadores mencionados acima
são marcas comerciais e marcas registradas de seus respectivos fabricantes.
</p>
</div>
