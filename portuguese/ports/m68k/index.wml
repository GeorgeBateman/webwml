#use wml::debian::template title="Porte Motorola 680x0" NOHEADER="yes"
#use wml::debian::toc
#use wml::debian::translation-check translation="40581c4d7249c0d28f7574d15e7614b10f2752b4"

<toc-display/>

<toc-add-entry name="about">Debian no Motorola 680x0</toc-add-entry>

<p>A série de processadores Motorola 680x0 alimentou computadores pessoais e
estações de trabalho desde meados de 1980. O Debian atualmente roda em
processadores 68020, 68030, 68040 e 68060: este é um porte não oficial
significando que não existe mais um porte oficial e lançado para m68k, mas
este porte está em desenvolvimento.</p>

<p>Por favor, note que uma
<a href="http://foldoc.org/memory+management+unit">memory management unit (MMU)</a>
é <em>necessária</em>; isso exclui as variantes "EC" desses
processadores. A emulação de ponto flutuante está disponível; contudo, não
está funcional em alguns modelos mac devido a um bug em algumas revisões do
processador 68LC040 (processadores 68LC040 em outras subarquiteturas estão ok;
somente os Macintoshes parecem ter sido enviados com processadores 68LC040
quebrados).</p>

<toc-add-entry name="status">Status</toc-add-entry>

<p>O porte m68k do Debian foi oficialmente lançado pela primeira vez com o
Debian 2.0 (hamm) e foi um porte oficial até o Debian 4.0 (etch). Agora
existem esforços para reviver este porte.</p>

<p>Atualmente, o porte Debian/m68k suporta Atari, Amiga, VMEbus e alguns
sistemas Macintosh.</p>

<p>Para mais informações sobre o estado atual do porte m68k, por favor
visite nossa <a href="https://wiki.debian.org/M68k/Status">página wiki</a>.</p>

<p>Ajuda é sempre necessária e bem-vinda! Em particular, kernels e imagens de
boot suportando outros portes do <a href="http://www.linux-m68k.org/">\
kernel Linux/m68k</a>, como Q40/Q60 e Sun 3, seria legal.</p>

<p>Se você está querendo ajudar, nós mantemos uma lista de tarefas na <a
href='https://wiki.debian.org/'>wiki do Debian</a> para o <a
href='https://wiki.debian.org/M68k/Porting'>sistema Debian/m68k</a> e para o
<a href='https://wiki.debian.org/DebianInstaller/M68k'>porte do Debian-installer
para a arquitetura m68k</a>.</p>


<p>O <a href="http://unstable.buildd.net/index-m68k.html">sistema de
autobuild do Debian/68k</a> contém informação atualizada sobre os
esforços de portabilidade. No caso de questões e/ou problemas relacionados ao
sistema de autobuild, por favor entre em contato em
<email "debian-68k@lists.debian.org" /> com uma tag "[buildd]" no campo
assunto.</p>

<toc-add-entry name="credits">Créditos</toc-add-entry>

<p>
Esta é uma lista de pessoas que estão trabalhando no projeto Debian/m68k.
Ela também inclui alguns(mas) contribuidores(as) significantes que
partiram para fazer outras coisas. Nos informe se você estiver ausente
desta lista!

<dl>

<dt>Frank Neumann
<dd>
Lançou o porte m68k do Debian.

<dt>Martin "Joey" Schulze
<dd>
Forneceu a infraestrutura na Infodrom para "kullervo", a construção primária do
daemon, para ser conectada à internet.  Também ajudou a organizar encontros
hacker do Linux em Oldenburg.

<dt>Roman Hodek
<dd>
Com James Troup, criou o <tt>buildd</tt>, o daemon de construção automatizado
para o porte m68k. Agora, o buildd também é utilizado por outras arquiteturas.

<dt>James Troup
<dd>
Escreveu o <tt>quinn-diff</tt> e outros utilitários para automatizar a
construção de pacotes.

<dt>David Huggins-Daines
<dd>
Manteve o suporte ao m68k no time boot-floppies.  Também apoiou o <a
href="http://www.mac.linux-m68k.org/">código original do kernel Mac</a>.

<dt>Michael Schmitz
<dd>
Construiu e testou o sistema de instalação para 2.1.

<dt>Christian T. Steigies
<dd>
Mantém os pacotes do kernel Debian/68k.

<dt>Stephen R. Marenka
<dd>
Junto com Wouter Verhelst, portou o debian-installer (o sistema de
instalação para o Debian 3.1 e posteriores) para a arquitetura m68k.

<dt>Wouter Verhelst
<dd>
Junto com Stephen Marenka, portou o debian-installer (o sistema de
instalação para o Debian 3.1 e posteriores) para a arquitetura m68k.

<dt>Thorsten Glaser
<dd>
Reuniu correções dos(as) mantenedores(as) do Debian/m68k, desenvolvedores(as)
Linux m68k e outras pessoas; levou o Debian/m68k através da transição entre
linuxthreads para NPTL com TLS ao integrá-lo nos pacotes do Debian e
tornando-se o equivalente humano ao buildd por um período longo o suficiente
para novamente fazer bootstrap no Sid.
Finn Thain, Andreas Schwab e Geert Uytterhoeven forneceram dados valiosos para
isso, além daquelas pessoas já mencionadas acima.

</dl>


<toc-add-entry name="contact">Informações para contato</toc-add-entry>

<p>
A lista de discussão para este projeto é
<email "debian-68k@lists.debian.org" />.
Para se inscrever, envie uma mensagem contendo a palavra "subscribe"
no assunto para <email "debian-68k-request@lists.debian.org" />, ou use a
<a href="https://lists.debian.org/debian-68k/">página web da lista de discussão</a>.
Você também pode navegar e fazer pesquisas nos
<a href="https://lists.debian.org/debian-68k/">arquivos da lista</a>.

<p>A lista de discussão das pessoas que portam o m68k ficava em <email
"m68k-build@nocrew.org"/>.  Isto também era usado como o endereço de contato
para o sistema de autoconstrução do m68k. Contudo, para contatar a equipe de
porte do m68k hoje, o procedimento preferido é usar debian-68k@lists.debian.org
com a tag [buildd] no campo assunto.</p>

<p>Por favor, envie comentários sobre essas páginas web para
<a href="mailto:debian-68k@lists.debian.org">a lista de discussão do
Debian/m68k</a>.</p>

<toc-add-entry name="links">Links</toc-add-entry>

<p>
Uma página dedicada reúne alguns <a href="links">links relacionados ao porte do m68k</a>.
</p>
