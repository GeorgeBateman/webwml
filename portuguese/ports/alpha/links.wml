#use wml::debian::template title="Porte Alpha -- Links" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/alpha/menu.inc"
#use wml::debian::translation-check translation="b9cb4ebb36a3cc48be8b8b2ac3b06f113491d26f"

<h1>Links Alpha</h1>

<ul>

<li><a href="http://www.alphalinux.org/">AlphaLinux.org</a><br />
Esta página contém praticamente tudo que você quer ou precisa saber
sobre rodar Linux em Alphas. Inclui inúmeros links e uma excelente
seção de notícias. Este site é também o local onde estão os
<a href="http://www.alphalinux.org/docs/">FAQs e HOWTOs</a>
oficiais relacionados ao Linux em Alphas.</li>

<li><a href="http://www.alphalinux.org/docs/alpha-howto.html">\
Introdução aos Sistemas Alpha</a><br />
Documento um pouco antigo, mas ainda informativo para usuários(as) novos(as) ou
experientes. Ele descreve as diferenças entre Alphas e outras arquiteturas
assim como descreve sistemas diferentes. <b>Se você tem um sistema antigo e
realmente precisa saber o que seu sistema é, apesar do que está escrito no
gabinete, veja
<a href="http://www.alphalinux.org/docs/alpha-howto.html#The%20Systems">
esta subseção</a>.</b></li>

<li><a href="http://www.alphalinux.org/faq/FAQ.html">FAQ Linux/Alpha</a><br />
Uma boa fonte de informação, a FAQ Linux/Alpha foi originalmente
reunida por usuários(as) Red Hat, mas tem se expandido para incluir mais
informações gerais e ainda é uma ótima referência.</li>

<li><a href="http://www.alphalinux.org/faq/SRM-HOWTO/index.html">HOWTO do
Firmware SRM</a><br />
O HOWTO oficial para sistemas que usam o firmware SRM. Se você precisa usar o
<a href="https://sourceforge.net/projects/aboot/">aboot</a> para iniciar seu
Alpha, esta página é para você.
#Este HOWTO também está incluído na
#última versão do
#<a href="https://packages.qa.debian.org/a/aboot.html">pacote Debian</a>.
</li>

<li><a href="http://alphalinux.org/faq/MILO-HOWTO/t1.html">HOWTO do MILO</a><br />
O HOWTO oficial para MILO. Note que o
<a href="$(HOME)/devel/debian-installer/">instalador do Debian</a> não tem
suporte ao MILO. SRM é altamente recomendado, mas se você não puder migrar e
estiver interessado(a) em suporte ao MILO para versões pós Woody do Debian, leia
<a href="https://lists.debian.org/debian-alpha/2004/debian-alpha-200402/msg00003.html">\
esta mensagem para debian-alpha</a> e inscreva-se na lista de discussão <a
href="https://lists.debian.org/debian-boot/">debian-boot</a>
para adicionar suporte ao MILO. Os últimos locais conhecidos são
as páginas de <a href="http://www.suse.de/~stepan/">Stepan Rainauers</a>,
<a href="ftp://genie.ucd.ie/pub/alpha/milo/">Nikita Schmidt</a>
e os trabalhos feitos pelo <a
href="http://dev.gentoo.org/~taviso/milo/">Gentoo</a>.</li>

<li><a href="http://www.alphalinux.org/faq/alphabios-howto.html">HOWTO do
Firmware AlphaBIOS</a><br />
O HOWTO oficial para sistemas que usam o firmware AlphaBIOS.</li>

<li><a href="https://digital.com/about/dec/">\
Antiga documentação da biblioteca da digital</a> <!-- com um <a
href="ftp://ftp.unix-ag.org/user/nils/Alpha-Docs/">espelho de Nils
Faerber</a>--></li>

<!-- <li><a href="http://www.alphanews.net/">alphanews.net</a><br />
Algumas notícias relacionadas ao Alpha são colocadas aqui, para muitos
sistemas operacionais que rodam ou rodavam em alphas.</li> -->

<li><a href="http://www.helgefjell.de/browser.php">Navegadores rodando
no Alpha em Linux</a><br />
Se você tem problemas com o seu navegador em um ambiente de 64&nbsp;bits
(não deveria ser o caso hoje em dia) ou quer tentar um novo,
você pode encontrar uma lista de navegadores que funcionam (e que
não funcionam) aqui.</li>

<li><a href="http://alphacore.info/wiki/">Página Wiki do AlphaCore</a><br />
Esta Wiki atualmente tem como foco principal o AlphaCore (Fedora Core no
Alpha) e também se propõe a coletar informações de utilidade geral relacionadas
ao Alpha.</li>
</ul>

<p>
Obrigado Nils Faerber por me permitir colocar partes de
sua coleção de links aqui.
</p>

<h1><a name="lists">Listas de discussão</a></h1>

<ul>

<li>A lista de discussão debian-alpha<br />
Envie uma mensagem com o assunto 'subscribe'
<email "debian-alpha-request@lists.debian.org"> para se inscrever.
<a href="https://lists.debian.org/debian-alpha/">Os arquivos estão disponíveis</a>.</li>

<li>Lista Linux/Alpha do Red Hat

<p>
Essa lista é voltada para usuários(as) Red Hat Linux/Alpha, mas também tem
dicas valiosas para questões gerais do Linux-Alpha. Para se inscrever,
vá na <a href="https://www.redhat.com/mailman/listinfo/axp-list">página
de informações da lista</a>. Os <a
href="https://www.redhat.com/archives/axp-list/">arquivos</a> dessa lista
também estão disponíveis. Um histórico alternativo (com suporte à pesquisa) que
pode ser encontrado em <url "http://www.lib.uaa.alaska.edu/axp-list/" />.
</p></li>

</ul>
