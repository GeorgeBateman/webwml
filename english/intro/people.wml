#use wml::debian::template title="People: who we are, what we do"
#include "$(ENGLISHDIR)/releases/info"

# translators: some text is taken from /intro/about.wml

<h2>Developers and contributors</h2>
<p>Debian is produced by almost a thousand active
developers spread
<a href="$(DEVEL)/developers.loc">around the world</a> who volunteer
in their spare time.
Few of the developers have actually met in person.
Communication is done primarily through e-mail (mailing lists at
lists.debian.org) and IRC (#debian channel at irc.debian.org).
</p>

<p>The complete list of official Debian members can be found on
<a href="https://nm.debian.org/members">nm.debian.org</a>, where membership is
managed. A broader list of Debian contributors can be found on
<a href="https://contributors.debian.org">contributors.debian.org</a>.</p>

<p>The Debian Project has a carefully <a href="organization">organized
structure</a>. For more information on how Debian looks from the inside,
please feel free to browse the <a href="$(DEVEL)/">developers' corner</a>.</p>

<h3><a name="history">How'd it all get started?</a></h3>

<p>Debian was begun in August 1993 by Ian Murdock, as a new distribution
which would be made openly, in the spirit of Linux and GNU. Debian was meant
to be carefully and conscientiously put together, and to be maintained and
supported with similar care. It started as a small, tightly-knit group of
Free Software hackers, and gradually grew to become a large, well-organized
community of developers and users. See
<a href="$(DOC)/manuals/project-history/">the detailed history</a>.

<p>Since many people have asked, Debian is pronounced /&#712;de.bi.&#601;n/. It
comes from the names of the creator of Debian, Ian Murdock, and his wife,
Debra.
  
<h2>Individuals and organizations supporting Debian</h2>

<p>Many other individuals and organizations are part of the Debian community:
<ul>
  <li><a href="https://db.debian.org/machines.cgi">Hosting and hardware sponsors</a></li>
  <li><a href="../mirror/sponsors">Mirror sponsors</a></li>
  <li><a href="../partners/">Development and service partners</a></li>
  <li><a href="../consultants">Consultants</li>
  <li><a href="../CD/vendors">Vendors of Debian installation media</a></li>
  <li><a href="distrib/pre-installed">Computer vendors that pre-install Debian</a></li>
  <li><a href="../events/merchandise">Merchandise vendors</a></li>
</ul>

<h2><a name="users">Who uses Debian?</a></h2>

<p>Although no precise statistics are available (since Debian does not
require users to register), evidence is quite strong that Debian is
used by a wide range of organizations, large and small, as well as
many thousands of individuals. See our <a href="../users/">Who's
using Debian?</a> page for a list of high-profile organizations which
have submitted short descriptions of how and why they use Debian.
