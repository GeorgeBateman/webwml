<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Harry Sintonen from F-Secure Corporation discovered multiple vulnerabilities in
OpenSSH, an implementation of the SSH protocol suite. All the vulnerabilities
are in found in the scp client implementing the SCP protocol.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20685">CVE-2018-20685</a>

    <p>Due to improper directory name validation, the scp client allows servers to
    modify permissions of the target directory by using empty or dot directory
    name.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6109">CVE-2019-6109</a>

    <p>Due to missing character encoding in the progress display, the object name
    can be used to manipulate the client output, for example to employ ANSI
    codes to hide additional files being transferred.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6111">CVE-2019-6111</a>

    <p>Due to scp client insufficient input validation in path names sent by
    server, a malicious server can do arbitrary file overwrites in target
    directory. If the recursive (-r) option is provided, the server can also
    manipulate subdirectories as well.</p>

    <p>The check added in this version can lead to regression if the client and
    the server have differences in wildcard expansion rules. If the server is
    trusted for that purpose, the check can be disabled with a new -T option to
    the scp client.</p></li>

</ul>

<p>For the stable distribution (stretch), these problems have been fixed in
version 1:7.4p1-10+deb9u5.</p>

<p>We recommend that you upgrade your openssh packages.</p>

<p>For the detailed security status of openssh please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssh">\
https://security-tracker.debian.org/tracker/openssh</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4387.data"
# $Id: $
