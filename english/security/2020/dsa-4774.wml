<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to the execution of arbitrary code, privilege escalation,
denial of service or information leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12351">CVE-2020-12351</a>

    <p>Andy Nguyen discovered a flaw in the Bluetooth implementation in the
    way L2CAP packets with A2MP CID are handled. A remote attacker in
    short distance knowing the victim's Bluetooth device address can
    send a malicious l2cap packet and cause a denial of service or
    possibly arbitrary code execution with kernel privileges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12352">CVE-2020-12352</a>

    <p>Andy Nguyen discovered a flaw in the Bluetooth implementation. Stack
    memory is not properly initialised when handling certain AMP
    packets. A remote attacker in short distance knowing the victim's
    Bluetooth device address can retrieve kernel stack information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25211">CVE-2020-25211</a>

    <p>A flaw was discovered in netfilter subsystem. A local attacker
    able to inject conntrack Netlink configuration can cause a denial
    of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25643">CVE-2020-25643</a>

    <p>ChenNan Of Chaitin Security Research Lab discovered a flaw in the
    hdlc_ppp module. Improper input validation in the ppp_cp_parse_cr()
    function may lead to memory corruption and information disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25645">CVE-2020-25645</a>

    <p>A flaw was discovered in the interface driver for GENEVE
    encapsulated traffic when combined with IPsec. If IPsec is
    configured to encrypt traffic for the specific UDP port used by the
    GENEVE tunnel, tunneled data isn't correctly routed over the
    encrypted link and sent unencrypted instead.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 4.19.152-1. The vulnerabilities are fixed by rebasing to the new
stable upstream version 4.19.152 which includes additional bugfixes.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4774.data"
# $Id: $
