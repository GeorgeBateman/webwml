<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were discovered in Netty, a Java NIO
client/server socket framework:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-0193">CVE-2014-0193</a>

    <p>WebSocket08FrameDecoder allows remote attackers to cause a denial
    of service (memory consumption) via a TextWebSocketFrame followed
    by a long stream of ContinuationWebSocketFrames.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-3488">CVE-2014-3488</a>

    <p>The SslHandler allows remote attackers to cause a denial of
    service (infinite loop and CPU consumption) via a crafted
    SSLv2Hello message.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16869">CVE-2019-16869</a>

    <p>Netty mishandles whitespace before the colon in HTTP headers (such
    as a "Transfer-Encoding : chunked" line), which leads to HTTP
    request smuggling.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20444">CVE-2019-20444</a>

    <p>HttpObjectDecoder.java allows an HTTP header that lacks a colon,
    which might be interpreted as a separate header with an incorrect
    syntax, or might be interpreted as an "invalid fold."</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20445">CVE-2019-20445</a>

    <p>HttpObjectDecoder.java allows a Content-Length header to be
    accompanied by a second Content-Length header, or by a
    Transfer-Encoding header.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7238">CVE-2020-7238</a>

    <p>Netty allows HTTP Request Smuggling because it mishandles
    Transfer-Encoding whitespace (such as a
    [space]Transfer-Encoding:chunked line) and a later Content-Length
    header.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.9.0.Final-1+deb8u1.</p>

<p>We recommend that you upgrade your netty-3.9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2110.data"
# $Id: $
