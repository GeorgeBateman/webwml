<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p><a href="https://security-tracker.debian.org/tracker/CVE-2020-13663">CVE-2020-13663</a> - Drupal SA 2020-004</p>

  <p>The Drupal core Form API does not properly handle certain form
  input from cross-site requests, which can lead to other vulnerabilities.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
7.32-1+deb8u19.</p>

<p>We recommend that you upgrade your drupal7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2263.data"
# $Id: $
