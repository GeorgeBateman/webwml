<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Isaac Boukris and Andrew Bartlett discovered that the S4U2Self Kerberos
extension used in Samba's Active Directory support was susceptible to
man-in-the-middle attacks caused by incomplete checksum validation.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2:4.2.14+dfsg-0+deb8u13.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1788.data"
# $Id: $
