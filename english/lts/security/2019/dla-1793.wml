<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a read overflow vulnerability in the dhcpcd5 network management protocol client.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11579">CVE-2019-11579</a>

    <p>dhcp.c in dhcpcd before 7.2.1 contains a 1-byte read overflow with DHO_OPTSOVERLOADED.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
6.0.5-2+deb8u1. Thanks to Roy Marples &lt;roy@marples.name&gt;.</p>

<p>We recommend that you upgrade your dhcpcd5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1793.data"
# $Id: $
