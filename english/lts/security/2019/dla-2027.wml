<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were found in Ruby that also affected
Debian's JRuby package, a pure-Java implementation of Ruby. Attackers
were able to call arbitrary Ruby methods, cause a denial-of-service or
inject input into HTTP response headers when using the WEBrick module.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.5.6-9+deb8u2.</p>

<p>We recommend that you upgrade your jruby packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2027.data"
# $Id: $
