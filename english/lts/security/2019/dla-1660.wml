<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>More vulnerabilities were found by Nick Cleaton in the rssh code that
could lead to arbitrary code execution under certain circumstances.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3463">CVE-2019-3463</a>

    <p>reject rsync --daemon and --config command-line options; arbitrary
    command execution</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3464">CVE-2019-3464</a>

    <p>prevent popt to load a ~/.popt configuration file, leading to
    arbitrary command execution</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.3.4-4+deb8u2.</p>

<p>We recommend that you upgrade your rssh packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1660.data"
# $Id: $
