<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a heap buffer overflow vulnerability
in curl, the library and command-line tool for transferring data over the
internet.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5482">CVE-2019-5482</a>

    <p>TFTP small blocksize heap buffer overflow</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
7.38.0-4+deb8u16.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1917.data"
# $Id: $
