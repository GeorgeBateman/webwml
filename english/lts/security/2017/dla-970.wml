<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The Qualys Security team discovered that sudo, a program designed to
provide limited super user privileges to specific users, does not
properly parse "/proc/[pid]/stat" to read the device number of the tty
from field 7 (tty_nr). A sudoers user can take advantage of this flaw on
an SELinux-enabled system to obtain full root privileges.</p>

<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version
1.8.5p2-1+nmu3+deb7u3.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.8.10p3-1+deb8u4.</p>

<p>We recommend that you upgrade your sudo packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-970.data"
# $Id: $
