<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Immediately after the previous update to graphicsmagick, two more security
issues were identified. These updates are included here.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13737">CVE-2017-13737</a>

    <p>Incorrect rounding up resulted in scrambling the heap beyond the
    allocation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15277">CVE-2017-15277</a>

    <p>Left the palette uninitialized when processing a GIF
    file that has neither a global nor local palette.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.16-1.1+deb7u11.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1140.data"
# $Id: $
