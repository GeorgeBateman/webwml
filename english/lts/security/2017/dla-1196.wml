<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>optipng, an advanced PNG (Portable Network Graphics) optimizer, has
been found vulnerable to a buffer overflow which allows remote
attackers to cause a denial-of-service attack or other unspecified
impact with a maliciously crafted GIF format file, related to an
uncontrolled loop in the LZWReadByte function of the gifread.c file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.6.4-1+deb7u4.</p>

<p>We recommend that you upgrade your optipng packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1196.data"
# $Id: $
