#use wml::debian::ddp title="Debian 用户手册"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/user-manuals.defs"
#use wml::debian::translation-check translation="3c8a13b77e9d8711624bf06e84d584eb25b58276"

<document "Debian GNU/Linux FAQ" "faq">

<div class="centerblock">
<p>
用户常问的问题。

<doctable>
  <authors "Susan G. Kleinmann, Sven Rudolph, Santiago Vila, Josip Rodin, Javier Fernández-Sanguino Peña">
  <maintainer "Javier Fernández-Sanguino Peña">
  <status>
  已完成
  </status>
  <availability>
  <inpackage "debian-faq"><br>
  <inddpvcs-debian-faq>
  </availability>
</doctable>
</div>

<hr />

<document "Debian 安装手册" "install">

<div class="centerblock">
<p>
Debian GNU/Linux 发行版的安装指导。\
本手册描述了使用 Debian 安装程序进行安装的过程，\
Debian 安装程序作为 Debian 的安装系统，最早随 \
<a href="$(HOME)/releases/sarge/">Sarge</a>\
（Debian GNU/Linux 3.1）一同发布。<br>
关于安装过程的更多信息，可以在 \
<a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian 安装程序 FAQ</a>
和 <a href="https://wiki.debian.org/DebianInstaller">Debian 安装程序维基页面</a>中找到。

<doctable>
  <authors "Debian 安装程序团队">

  <maintainer "Debian 安装程序团队">
  <status>
本手册还不完美。针对当前和未来的 Debian 发布版本的修改工作正在活跃进行中。\
我们欢迎您的帮助，尤其是非 x86 架构的安装过程和翻译工作。联系
<a href="mailto:debian-boot@lists.debian.org?subject=Install%20Manual">debian-boot@lists.debian.org</a>
以获得更多信息。
  </status>
  <availability>
  <insrcpackage "installation-guide">
  <br><br>
  <a href="$(HOME)/releases/stable/installmanual">\
针对“稳定版”的已发布版本</a>
  <br>
在<a href="$(HOME)/CD/">官方完整 CD 和 DVD 映像</a>\
的 <tt>/doc/manual/</tt> 目录可以找到。
  <br><br>
  <a href="$(HOME)/releases/testing/installmanual">为下一个稳定发行版本（目前是 testing）准备的版本</a>
  <br><br>
  <a href="https://d-i.debian.org/manual/">开发版本</a>
  <br>
  <inddpvcs-installation-guide>
  </availability>
</doctable>

<p>
先前的发布版本（可能也包括下一发布版本）的安装手册可以在相应版本的\
<a href="$(HOME)/releases/">发布页面</a>找到链接。

</div>

<hr />

<document "Debian 发行说明" "relnotes">

<div class="centerblock">
<p>
本文档包含了当前的 Debian GNU/Linux 发行版的新变化，以及给旧版 Debian 用户\
的完整升级说明。

<doctable>
  <authors "Adam Di Carlo, Bob Hilliard, Josip Rodin, Anne Bezemer, Rob Bradford, Frans Pop, Andreas Barth, Javier Fernández-Sanguino Peña, Steve Langasek">
  <status>
开发工作在 Debian 新版本发布时最为活跃。联系
<a href="mailto:debian-doc@lists.debian.org?subject=Release%20Notes">debian-doc@lists.debian.org</a>
以获得更多信息。问题报告和补丁应当作为
<a href="https://bugs.debian.org/release-notes">release-notes 伪软件包\
的缺陷</a>提交。
  </status>
  <availability>
  <a href="$(HOME)/releases/stable/releasenotes">已发布版本</a>
  <br>
在<a href="$(HOME)/CD/">官方完整 CD 和 DVD 映像</a>\
的 <tt>/doc/release-notes/</tt> 目录可以找到。
#  <br>
#  Also available at <a href="$(HOME)/mirror/list">ftp.debian.org and all mirrors</a>
#  in the <tt>/debian/doc/release-notes/</tt> directory.
#  <br>
#  <a href="$(HOME)/releases/testing/releasenotes">Version being prepared
#  for the next stable (in testing)</a>

  <inddpvcs-release-notes>
  </availability>
</doctable>
</div>

<hr />

<document "Debian 参考卡" "refcard">

<div class="centerblock">
<p>
本卡用一张纸的篇幅，包含了在 Debian GNU/Linux \
下工作时最重要的一些命令，以供 Debian GNU/Linux 的新用户参考。\
读者最少要了解关于计算机、文件、目录和命令行的基本（或更多的）知识。

<doctable>
  <authors "W. Martin Borgert">
  <maintainer "W. Martin Borgert">
  <status>
已发布；活跃开发中
  </status>
  <availability>
  <inpackage "debian-refcard"><br>
  <inddpvcs-refcard>
  </availability>
</doctable>
</div>

<hr />

<document "Debian 管理员手册" "debian-handbook">

<div class="centerblock">
<p>
Debian 管理员手册囊括了任何想要成为高效且能独立工作的 Debian GNU/Linux \
管理员的人所需要了解的必备知识。

<doctable>
  <authors "Raphaël Hertzog, Roland Mas">
  <status>
已发布；活跃开发中
  </status>
  <availability>
  <inpackage "debian-handbook"><br>
  <inddpvcs-debian-handbook>
  </availability>
</doctable>
</div>

<hr />

<document "Debian 参考手册" "quick-reference">

<div class="centerblock">
<p>
本 Debian GNU/Linux 参考手册以 shell 命令为例，涵盖了系统管理\
的方方面面。本书的主题包括系统安装、Debian 软件包管理、Debian 下的 \
Linux 内核、系统调优、搭建网关、文本编辑器、版本控制系统、编程和 GnuPG，\
针对这些主题提供了基本的教程、提示和其他信息。

   <p>曾叫做“快速参考指南”。

<doctable>
  <authors "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <maintainer "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <status>
已发布；活跃开发中
  </status>
  <availability>
  <inpackage "debian-reference"><br>
  <inddpvcs-debian-reference>
  </availability>
</doctable>
</div>

<hr />

<document "Debian 安全手册" "securing">

<div class="centerblock">
<p>
本手册阐述了 Debian GNU/Linux 操作系统和 Debian 项目内部的安全相关\
问题。它从（手动或自动地）加固默认的 Debian GNU/Linux 安装开始，涵盖了\
设定安全的用户及网络环境过程中会遇到的一些常见任务，提供了关于可用的安全\
工具的相关信息，系统被攻破之前和之后需要做的事情，以及 Debian 安全团队如何\
保护 Debian 的安全。本文档包括了一份手把手的加固指南，并且在附录中，包含了\
如何用 Debian GNU/Linux 设置入侵检测系统和网桥防火墙的详细信息。

<doctable>
  <authors "Alexander Reelsen, Javier Fernández-Sanguino Peña">
  <maintainer "Javier Fernández-Sanguino Peña">
  <status>
已发布；仍在开发中，但变更较少。部分内容可能不是最新的。
  </status>
  <availability>
  <inpackage "harden-doc"><br>
  <inddpvcs-securing-debian-manual>
  </availability>
</doctable>
</div>

<hr />

<document "aptitude 用户指南" "aptitude">

<div class="centerblock">
<p>
一本介绍 aptitude 软件包管理器的手册，含有完整的命令行参考指南。


<doctable>
  <authors "Daniel Burrows">
  <status>
已发布；活跃开发中
  </status>
  <availability>
  <inpackage "aptitude-doc"><br>
  <inddpvcs-aptitude>
  </availability>
</doctable>
</div>

<hr />

<document "APT 用户指南" "apt-guide">

<div class="centerblock">
<p>
本文档提供了如何使用 APT 软件包管理器的综述。


<doctable>
  <authors "Jason Gunthorpe">
  <status>
已发布；开发略停滞
  </status>
  <availability>
  <inpackage "apt-doc"><br>
  <inddpvcs-apt-guide>
  </availability>
</doctable>
</div>

<hr />

<document "离线使用 APT" "apt-offline">

<div class="centerblock">
<p>
本文档阐述了如何在无网络连接的环境下使用 APT，具体地说，如何使用\
“蹭网”法进行更新。

<doctable>
  <authors "Jason Gunthorpe">
  <status>
已发布；开发略停滞
  </status>
  <availability>
  <inpackage "apt-doc"><br>
  <inddpvcs-apt-offline>
  </availability>
</doctable>
</div>

<hr />

<document "Debian Java FAQ" "java-faq">

<div class="centerblock">
<p>
本 FAQ 的目的是成为开发人员和用户寻找关于 Debian 中的 Java 的各种问题的\
解答的地方，包括许可证问题、可用的开发软件包以及构建一个由自由软件构成\
的 Java 环境所需要的程序。

<doctable>
  <authors "Javier Fernández-Sanguino Peña, Torsten Werner, Niels Thykier, Sylvestre Ledru">
  <status>
已发布；活跃开发中，虽然有些内容可能不是最新的。
  </status>
  <availability>
  <inpackage "java-common"><br>
  <inddpvcs-debian-java-faq>
  </availability>
</doctable>
</div>

<hr />

<document "Debian Hamradio 維護者指南" "hamradio-maintguide">

<div class="centerblock">
<p>
Debian Hamradio 維護者指南列出了 Deian Hamradio 套件維護團隊的政策與最佳實踐準則。

<doctable>
  <authors "Iain R. Learmonth">
  <status>
  已發佈；活躍開發中。
  </status>
  <availability>
  <inpackage "hamradio-maintguide"><br>
  <inddpvcs-hamradio-maintguide>
  </availability>
</doctable>
</div>
