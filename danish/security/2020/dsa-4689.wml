#use wml::debian::translation-check translation="139ced0522f792565594fd4bc65bf27ae29bd20d" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder blev opdaget i BIND, en DNS-serverimplementering.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6477">CVE-2019-6477</a>

    <p>Man opdagede at TCP-pipelinede forespørgsler kunne omgå 
    tcp-client-begrænsninger, medførende lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8616">CVE-2020-8616</a>

    <p>Man opdagede at BIND ikke på tilstrækkelig vis begrænsede antallet af 
    udførte fetches, når der behandles referrals.  En angriber kunne drage nytte 
    af fejlen til at forårsage et lammelsesangreb (forringelse af ydeevne) eller 
    anvende rekursionsserveren i et reflection-angreb med en høj 
    forstærkelsesfaktor.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8617">CVE-2020-8617</a>

    <p>Man opdagede at en logisk fejl i koden, der kontrollerer 
    TSIG-gyldighed, kunne anvendes til at udløse en assertion failure, 
    medførende lammelsesangreb.</p></li>

</ul>

<p>I den gamle stabile distribution (stretch), er disse problemer rettet
i version 1:9.10.3.dfsg.P4-12.3+deb9u6.</p>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 1:9.11.5.P4+dfsg-5.1+deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine bind9-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende bind9, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4689.data"
