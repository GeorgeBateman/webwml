#use wml::debian::template title="Personer: Hvem vi er og hvad vi laver"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="1805de5ed4f7d703378f493828cf3055b1044eb2"

# translators: some text is taken from /intro/about.wml

<h2>Udviklere og bidragydere</h2>

<p>Debian fremstilles af næsten et tusinde aktive udviklere spredt 
<a href="$(DEVEL)/developers.loc">over hele kloden</a>, 
som frivilligt stiller deres fritid til rådighed.  Få af udviklerne har mødt 
hinanden ansigt til ansigt.  Primært anvendes e-mail (postlister på 
lists.debian.org) til at kommunikere med hinanden, samt IRC (#debian-kanalen på 
irc.debian.org).</p>

<p>Den komplette liste over officielle Debian-medlemmer findes på 
<a href="https://nm.debian.org/members">nm.debian.org</a>, hvor medlemskab 
administreres.  En bredere liste over bidragydere finder man på 
<a href="https://contributors.debian.org">contributors.debian.org</a>.</p>

<p>Debian-projektet har en omhyggeligt <a href="organization">organiseret
struktur</a>.  For flere oplysninger om hvordan Debian ser ud indefra, kan man
besøge <a href="$(DEVEL)/">udviklerhjørnet</a>.</p>


<h3><a name="history">Hvordan begyndte det hele?</a></h3>

<p>I august 1993 blev Debian påbegyndt af Ian Murdock, som en ny distribution, 
der skulle fremstilles i fuld offentlighed, i Linux' og GNU's ånd. Debian skulle 
sammensættes omhyggeligt og samvittighedsfuldt, og den skulle vedligeholdes 
og der skulle ydes brugerhjælp, med samme omhu.  Det begyndte som en lille, 
sammentømret gruppe af fri software-folk, der gradvist voksede til et stort, 
velorganiseret fællesskab af udviklere og brugere.  Læs 
<a href="$(DOC)/manuals/project-history/">den detaljerede historie</a>.</p>

<p>Da mange har spurgt - Debian udtales: &#712;de.bi.&#601;n/ (eller 
"deb ii n").  Det kommer af navnet på Debians grundlægger, Ian Murdock, og hans 
hustru, Debra.</p>
  

<h2>Enkeltpersoner og organisationer som støtter Debian</h2>

<p>Mange andre enkeltpersoner og organisationer er en del af Debians 
fællesskab:</p>

<ul>
  <li><a href="https://db.debian.org/machines.cgi">Hosting- og hardwaresponsorer</a></li>
  <li><a href="../mirror/sponsors">Filspejlssponsorer</a></li>
  <li><a href="../partners/">Udviklings- og tjenesteydelsespartnere</a></li>
  <li><a href="../consultants">Konsulenter</li>
  <li><a href="../CD/vendors">Forhandlere af Debian-installeringsmedier</a></li>
  <li><a href="../distrib/pre-installed">Computerforhandlere som præ-instalerer Debian</a></li>
  <li><a href="../events/merchandise">Forhandlere af reklameartikler</a></li>
</ul>


<h2><a name="users">Hvem bruger Debian?</a></h2>

<p>Selv om der ikke findes præcise statistikker (da Debian ikke kræver at 
brugerne lade sig registrere), er der stærke beviser for, at Debian anvendes af 
en bred vifte af organisationer, store og små, foruden mange tusinde private
brugere.  Se vores side om <a href="../users/">hvem der bruger Debian</a>, for 
en liste over kendte organisationer, der har indsendt korte beskrivelser af 
hvordan og hvorfor, de anvender Debian.</p>
