#use wml::debian::template title="Υλοποιήσεις"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::toc
#use wml::debian::translation-check translation="5a1b0ae1bf020f2ce5587fb743650f3991922238" maintainer="galaxico"

<toc-display/>

<toc-add-entry name="intro">Εισαγωγή</toc-add-entry>
<p>
 Όπως ξέρετε οι περισσότεροι/ες από σας, το <a 
href="https://www.kernel.org/">Linux</a>
 είναι απλά ένας πυρήνας. Και για πολύ καιρό, ο πυρήνας Linux έτρεχε μόνο σε 
μηχανήματα της σειράς Intel x86, από την αρχιτεκτονική 386 και μεταγενέστερες.
</p>
<p>
 Αυτό δεν είναι πια αλήθεια. Ο πυρήνας του Linux έχει τώρα υλοποιηθεί σε μια μεγάλη και αυξανόμενη λίστα από αρχιτεκτονικές. Και με πολύ μικρή χρονική απόσταση, έχουμε υλοποιήσει το Debian σ' αυτές τις αρχιτεκτονικές. Γενικά, αυτή είναι μια διαδικασία που κολλά αρκετά στο ξεκίνημα (καθώς προσπαθούμε να κάνουμε την libc και το πρόγραμμα δυναμικής σύνδεσης να δουλεύουν ομαλά), και στη συνέχεια μια σχετικά ρουτινιάρικη αν και μακρόσυρτη δουλειά της προσπάθειας επαναμεταγλώττισης όλων των πακέτων μας στις καινούριες αρχιτεκτονικές.
</p>
<p>
 Το Debian είναι ένα λειτουργικό σύστημα (ΛΣ), όχι ένας πυρήνας (στην πραγματικότητα, είναι κάτι παραπάνω από ένα ΛΣ αφού περιλαμβάνει χιλιάδες προγράμματα εφαρμογών). Έτσι, αν και οι περισσότερες υλοποιήσεις του Debian βασίζονται στον πυρήνα Linux, υπάρχουν και υλοποιήσεις που βασίζονται στους πυρήνες FreeBSD, NetBSD και Hurd.
</p>

<div class="important">
<p>
 Αυτή είναι μια σελίδα υπό επεξεργασία. Δεν υπάρχουν ακόμα σελίδες για όλες τις υλοποιήσεις και οι περισσότερες από αυτές που υπάρχουν είναι σε εξωτερικούς ιστότοπους. Εργαζόμαστε για να συλλέξουμε πληροφορίες για όλες τις υλοποιήσεις και τον "κατοπτρισμό" τους στον ιστότοπο του Debian. Περισσότερες υλοποιήσεις μπορούν να βρεθούν <a href="https://wiki.debian.org/CategoryPorts">καταγεγγραμένες</a> στο Wiki.
</p>
</div>

<toc-add-entry name="portlist-released">Λίστα επίσημων υλοποιήσεων</toc-add-entry>
<br />

<table class="tabular" summary="">
<tbody>
<tr>
<th>Υλοποίηση</th>
<th>Αρχιτεκτονική</th>
<th>Περιγραφή</th>
<th>Κατάσταση</th>
</tr>
<tr>
<td><a href="amd64/">amd64</a></td>
<td>64-bit PC (amd64)</td>
<td>Κυκλοφόρησε πρώτη φορά με την έκδοση Debian 4.0. Υλοποίηση σε 64-μπιτους επεξεργαστές x86. Ο στόχος είναι η υποστήριξη τόσο του 32-μπιτου όσο και του 64-μπιτου χώρου χρήστη (userland) σ' αυτή την αρχιτεκτονική. Αυτή η υλοποίηση υποστηρίζει τους 64-μπιτους επεξεργαστές Opteron, Athlon και Sempron της AMD και τους επεξεργαστές της Intel με υποστήριξη της πλατφόρμας Intel 64, συμπεριλαμβανομένων της σειράς Pentium D και διαφόρων σειρών Xeon κσι Core.</td>
<td><a href="$(HOME)/releases/stable/amd64/release-notes/">κυκλοφορεί</a></td>
</tr>
<tr>
<td><a href="arm/">arm64</a></td>
<td>64-bit ARM (AArch64)</td>
<td>Η έκδοση 8 της αρχιτεκτονικής ARM περιελάμβανε την πλατφόρμα AArch64, ένα καινούριο σύνολο εντολών (instruction set). Από την έκδοση Debian 8.0, η υλοποίηση arm64 έχει συμπεριληφθεί στο Debian για να υποστηρίξει αυτό το καινούριο σύνολο εντολών σε επεξεργαστές όπως οι Applied Micro X-Gene, AMD Seattle και Cavium ThunderX.</td>
<td><a href="$(HOME)/releases/stable/arm64/release-notes/">κυκλοφορεί</a></td>
</tr>
<tr>
<td><a href="arm/">armel</a></td>
<td>EABI ARM</td>
<td>Η παλιότερη από τις τρέχουσες υλοποιήσεις του Debian ARM υποστηρίζει επεξεργαστές ARM little-endian συμβατούς με το σύνολο εντολών v4t.</td>
<td><a href="$(HOME)/releases/stable/armel/release-notes/">κυκλοφορεί</a></td>
</tr>
<tr>
<td><a href="arm/">armhf</a></td>
<td>Hard Float ABI ARM</td>
<td>Αρκετές σύγχρονες 32-μπιτες μητρικές ARM και συσκευές κυκλοφορούν με μια μονάδα κινητής υποδιαστολής (floating-point unit, FPU), αλλά η υλοποίηση Debian armel δεν την εκμεταλλεύεται ιδιαίτερα. Η υλοποίηση armhf ξεκίνησε ώστε να βελτιώσει την κατάσταση σ' αυτό το σημείο καθώς και να εκμεταλλευτεί άλλα χαρακτηριστικά των νεώετερων επεξεργαστών ARM. Η υλοποίηση Debian armhf απαιτεί τουλάχιστον έναν επεξεργαστή ARMv7 με υποστήριξη κινητής υποδιαστολής Thumb-2 και VFP3-D16.</td>
<td><a href="$(HOME)/releases/stable/armhf/release-notes/">κυκλοφορεί</a></td>
</tr>
<tr>
<td><a href="i386/">i386</a></td>
<td>32-bit PC (i386)</td>
<td>Η πρώτη αρχιτεκτονική και όχι αυστηρά μια "υλοποίηση". Ο πυρήνας Linux αναπτύχθηκε αρχικά για επεξεργαστές Intel 386, εξ ου και το μικρό όνομα. Το Debian υποστηρίζει όλους τους επεξεργαστές αρχιτεκτονικής IA-32 από την Intel (συμπεριλαμβανομένων όλων των σειρών Pentium και κάποια πιο πρόσφατα μηχανήματα Core Duo σε 32-μπιτη λειτουργία), τους επεξεργαστές της AMD (K6, όλες τις σειρές Athlon και τη σειρά Athlon64 σε 32-bit λειτουργία), Cyrix και επεξεργαστές άλλων κατασκευαστών.</td>
<td><a href="$(HOME)/releases/stable/i386/release-notes/"></a>κυκλοφορεί</td>
</tr>
<tr>
<td><a href="mips/">mips</a></td>
<td>MIPS (κατάσταση big-endian)</td>
<td>Ξεκινώντας με την έκδοση Debian 3.0. το Debian έχει υλοποιηθεί στην αρχιτεκτονική MIPS που χρησιμοποιείται σε μηχανήματα της SGI (debian-mips — big-endian) και Digital DECstations (debian-mipsel — little-endian).</td>
<td><a href="$(HOME)/releases/stable/mips/release-notes/">κυκλοφορεί</a></td>
</tr>
<tr>
<td><a href="mips/">mipsel</a></td>
<td>MIPS (little-endian mode)</td>
<td>Ξεκινώντας με την έκδοση Debian 3.0. το Debian έχει υλοποιηθεί στην αρχιτεκτονική MIPS που χρησιμοποιείται σε μηχανήματα SGI (debian-mips —
big-endian) και Digital DECstations (debian-mipsel — little-endian).</td>
<td><a href="$(HOME)/releases/stable/mipsel/release-notes/">κυκλοφορεί</a></td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/mips64el">mips64el</a></td>
<td>MIPS (64-bit little-endian mode)</td>
<td>
Αυτή η έκδοση υποστηρίζει την κατάσταση little-endian και χρησιμοποιεί τη διεπαφή N64 ABI, την MIPS64r1 ISA και μονάδα κινητής υποδιαστολής βασισμένη σε υλικό. 
Part of the official release since Debian 9.
</td>
<td><a href="$(HOME)/releases/stable/mips64el/release-notes/"></a>κυκλοφορεί</td>
</tr>
<tr>
<td><a href="powerpc/">ppc64el</a></td>
<td>POWER7+, POWER8</td>
<td>Ξεκινώντας με την έκδοση Debian 8.0. έχουμε την υλοποίηση σε Little-endian της αρχιτεκτονικής ppc64, με χρήσης της νέας διεπαφής Open Power ELFv2 ABI.</td>
<td><a href="$(HOME)/releases/stable/ppc64el/release-notes/"></a>κυκλοφορεί</td>
</tr>
<tr>
<td><a href="s390x/">s390x</a></td>
<td>System z</td>
<td>Κυκλοφόρησε πρώτη φορά με την έκδοση Debian 7.0. Μια υλοποίηση 64-μπιτου χώορυς χρήστη (userland) για  κεντρικούς υπολογιστές (mainframe) IBM System z.</td>
<td><a href="$(HOME)/releases/stable/s390x/release-notes/"></a>κυκλοφορεί</td>
</tr>
</tbody>
</table>

<toc-add-entry name="portlist-other">Λίστα άλλων υλοποιήσεων</toc-add-entry>

<div class="tip">
<p>
 Για μερικές από τις ακόλουθες υλοποιήσεις στον σύνδεσμο
 <url "https://cdimage.debian.org/cdimage/ports"/> δεν υπάρχουν επίσημες εικόνες μέσων εγκατάστασης. Οι εικόνες αυτές συντηρούνται από τις αντίστοιχες ομάδες Υλοποίησης του Debian (Debian Port Teams).
</p>
</div>

<table class="tabular" summary="">
<tbody>
<tr>
<th>Υλοποίηση</th>
<th>Αρχιτεκτονική</th>
<th>Περιγραφή</th>
<th>Κατάσταση</th>
</tr>
<tr>
<td><a href="alpha/">alpha</a></td>
<td>Alpha</td>
<td>Κυκλοφόρησε πρώτη φορά επίσημα με την έκδοση Debian 2.1. Δεν ικανοποίησε τα κριτήρια για να περιληφθεί στην έκδοση Debian 6.0 <q>squeeze</q>, και ως αποτέλεσμα αφαιρέθηκε από την αρχειοθήκη.
</td>
<td>έχει διακοπεί</td>
</tr>
<tr>
<td><a href="arm/">arm</a></td>
<td>OABI ARM</td>
<td>Αυτή η υλοποίηση τρέχει σε μια ποικιλία ενσωματωμένων συσκευών, όπως δρομολογητές ή συσκευές δικτυακής αποθήκευσης (NAS). Η υλοποίηση arm κυκλοφόρησε πρώτη φορά με την έκδοση Debian 2.2 και υποστηριζόταν μέχρι και την έκδοση Debian 5.0, όταν και αντικαταστάθηκε από την υλοποίηση armel.
</td>
<td>αντικαταστάθηκε από την armel</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20130326061253/http://avr32.debian.net/">AVR32</a></td>
<td>Atmel 32-bit RISC</td>
<td>Υλοποίηση στην 32-μπιτη RISC αρχιτεκτονική AVR32 της Atmel.</td>
<td>έχει διακοπεί</td>
</tr>
<tr>
<td><a href="hppa/">hppa</a></td>
<td>HP PA-RISC</td>
<td>Έχοντας κυκλοφορήσει πρώτη φορά επίσημα με την έκδοση Debian 3.0 <q>woody</q>, αυτή είναι μια υλοποίηση στην αρχιτεκτονική PA-RISC της Hewlett-Packard. Δεν ικανοποίησε τα κριτήρια για να περιληφθεί στην έκδοση Debian 6.0 <q>squeeze</q>, και ως αποτέλεσμα αφαιρέθηκε από την αρχειοθήκη.
</td>
<td>έχει διακοπεί</td>
</tr>
<tr>
<td><a href="hurd/">hurd-i386</a></td>
<td>32-bit PC (i386)</td>
<td>ΤΟ GNU Hurd είναι ένα καινούριο λειτουργικό σύστημα που έχει συναρμοστεί από την ομάδα του GNU. Το Debian GNU/Hurd πρόκειται να είναι ένα (ίσως το πρώτο) GNU OS. Το σχέδιο αυτή τη στιγμή είναι βασισμένο πάνω στην αρχιτεκτονική i386.
</td>
<td>σε ανάπτυξη</td>
</tr>
<tr>
<td><a href="ia64/">ia64</a></td>
<td>Intel Itanium IA-64</td>
<td>Κυκλοφόρησε πρώτη φορά επίσημα με την έκδοση Debian 3.0. Πρόκειται για μια υλοποίηση στην πρώτη 64-μπιτη αρχιτεκτονική της Intel. Σημείωση: αυτή η υλοποίηση δεν πρέπει να συγχέεται με τις πιο πρόσφατες 64-μπιτες επεκτάσεις της Intel για επεξεργαστές Pentium 4 και Celeron, που ονομάζονται Intel 64· γι' αυτούς δείτε την υλοποίηση AMD64. Με την έκδοση Debian 8 η υλοποίηση ia64 αφαιρέθηκε εξαιτίας ανεπαρκούς υποστήριξης από προγραμματιστές/προγραμματίστριες.</td>
<td>έχει διακοπεί</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-amd64</a></td>
<td>64-bit PC (amd64)</td>
<td>Κυκλοφόρησε πρώτη φορά επίσημα με την έκδοση Debian 6.0 ως προεπισκόπηση τεχνολογίας και η πρώτη υλοποίηση του Debian σε έναν πυρήνα διαφορετικό από τον Linux. Πρόκειται για μια υλοποίηση του συστήματος Debian GNU στον πυρήνα του FreeBSD. Δεν αποτελεί πλέον μέρος της επίσημης κυκλοφορίας μετά την έκδοση  Debian 8.</td>
<td>σε ανάπτυξη</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-i386</a></td>
<td>32-bit PC (i386)</td>
<td>Κυκλοφόρησε πρώτη φορά επίσημα με την έκδοση Debian 6.0 ως προεπισκόπηση τεχνολογίας και η πρώτη υλοποίηση του Debian σε έναν πυρήνα διαφορετικό από τον Linux. Πρόκειται για μια υλοποίηση του συστήματος Debian GNU στον πυρήνα του FreeBSD. Δεν αποτελεί πλέον μέρος της επίσημης κυκλοφορίας μετά την έκδοση  Debian 8.</td>
<td>σε ανάπτυξη</td>
</tr>
<tr>
<td><a href="http://www.linux-m32r.org/">m32</a></td>
<td>M32R</td>
<td>Υλοποίηση στον 32-μπιτο RISC μικροεπεξεργαστή της εταιρείας Renesas Technology.</td>
<td>δεν υπάρχει</td>
</tr>
<tr>
<td><a href="m68k/">m68k</a></td>
<td>Motorola 68k</td>
<td>Κυκλοφόρησε πρώτη φορά επίσημα με την έκδοση Debian 2.0. Η υλοποίηση απέτυχε να ικανοποιήσει τα κριτήρια ώστε να κυκλοφορήσει με την έκδοση Debian 4.0 και ως αποτέλεσμα δεν έχει συμπεριληφθεί στις εκδόσεις από την Etch και μεταγενέστερα και μετά από αυτό έχει μετακινηθεί στην αρχειοθήκη debian-ports. Η υλοποίηση Debian m68k τρέχει σε μια ποικιλία υπολογιστών που βασίζονται στη σειρά επεξεργαστών 68k της Motorola — συγκεκριμένα στη σειρά σταθμών εργασίας Sun3, τους προσωπικούς υπολογιστές Macintosh της Apple και τους προσωπικούς υπολογιστές Atari και Amiga.</td>
<td>σε εξέλιξη</td>
</tr>
<tr>
<td><a href="netbsd/">netbsd-i386</a></td>
<td>32-bit PC (i386)</td>
<td>Μια υλοποίηση του λειτουργικού συστήματος Debian, ολοκληρωμένου με το apt,
το dpkg, και τα προγράμματα χώρου χρήστη του GNU, στον πυρήνα του NetBSD. Η υλοποίηση δεν κυκλοφόρησε ποτέ και εγκαταλείφθηκε.</td>
<td>δεν υπάρχει</td>
</tr>
<tr>
<td><a href="netbsd/alpha/">netbsd-alpha</a></td>
<td>Alpha</td>
<td>Μια υλοποίηση του λειτουργικού συστήματος Debian, ολοκληρωμένου με το apt,
το dpkg, και τα προγράμματα χώρου χρήστη του GNU, στον πυρήνα του NetBSD. Η υλοποίηση δεν κυκλοφόρησε ποτέ και εγκαταλείφθηκε.</td>
<td>δεν υπάρχει</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20150905061423/http://or1k.debian.net/">or1k</a></td>
<td>OpenRISC 1200</td>
<td>Μια υλοποίηση στην CPU ανοιχτού κώδικα <a href="https://openrisc.io/">OpenRISC</a> 1200.</td>
<td>δεν υπάρχει</td>
</tr>
<tr>
<td><a href="powerpc/">powerpc</a></td>
<td>Motorola/IBM PowerPC</td>
<td>Κυκλοφόρησε επίσημα πρώτη φορά με την έκδοση Debian 2.2. Αυτή η υλοποίηση τρέχει σε πολλά μοντέλα Macintosh PowerMac της Apple, και σε μηχανήματα ανοιχτής αρχιτεκτονικής CHRP και PReP. Δεν αποτελεί πλέον μέρος της επίσημης έκδοσης μετά την έκδοση Debian 9.</td>
<td>έχει διακοπεί</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/PowerPCSPEPort">powerpcspe</a></td>
<td>PowerPC Signal Processing Engine</td>
<td>
Μια υλοποίηση στο υλικό "Signal Processing Engine" που υπάρχει σε χαμηλής κατανάλωσης 32-μπιτες CPU FreeScale και IBM "e500".
</td>
<td>σε εξέλιξη</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/RISC-V">riscv64</a></td>
<td>RISC-V (64-bit little endian)</td>
<td>Υλοποίηση για την <a href="https://riscv.org/">RISC-V</a>, μια ελεύθερη/ανοιχτού κώδικα Αρχιτεκτονική συνόλου εντολών (ISA), συγκεκριμένα την 64-μπιτη little-endian εκδοχή.</td>
<td>σε εξέλιξη</td>
</tr>
<tr>
<td><a href="s390/">s390</a></td>
<td>S/390 και zSeries</td>
<td>Κυκλοφόρησε πρώτη φορά επίσημα με την έκδοση Debian 3.0. Πρόκειται για μια υλοποίηση στους εξυπηρετητές S/390 της IBM. Αντικαταστάθηκε από την υλοποίηση s390x με την έκδοση Debian 8.</td>
<td>αντικαταστάθηκε από την s390x</td>
</tr>
<tr>
<td><a href="sparc/">sparc</a></td>
<td>Sun SPARC</td>
<td>Κυκλοφόρησε πρώτη φορά επίσημα με την έκδοση Debian 2.1. Αυτή η υλοποίηση τρέχει στη σειρά σταθμών εργασίας  Sun UltraSPARC, καθώς και σε μερικά από τα διάδοχα συστήματα με τις αρχιτεκτονικές sun4. Μετά την κυκλοφορία της έκδοσης Debian 8 η Sparc δεν ήταν πλέον μια αρχιτεκτονική για συμπερίληψη στις επίσημες εκδόσεις εξαιτίας ανεπαρκούς υποστήριξη από προγραμματιστές/προγραμματίστριες. Παρ' όλα αυτά, θα αντικατασταθεί από την υλοποίηση Sparc64.
</td>
<td>θα αντικατασταθεί από την sparc64</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/Sparc64">sparc64</a></td>
<td>64-bit SPARC</td>
<td>
Μια 64-μπιτη υλοποίηση για επεξεργαστές SPARC.
</td>
<td>σε ανάπτυξη</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/SH4">sh4</a></td>
<td>SuperH</td>
<td>
Μια υλοποίηση στου επεξεργαστές SuperH της Hitachi. Υποστηρίζει επίσης τον επεξεργαστή ανοιχτού λογισμικού
<a href="http://j-core.org/">J-Core</a>.
</td>
<td>σε ανάπτυξη</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/X32Port">x32</a></td>
<td>64-bit PC με 32-μπιτους δείκτες</td>
<td>
Η X32 είναι μια διεπαφή ABI για amd64/x86_64 CPU που χρησιμοποιούν 32-μπιτους δείκτες (pointers). Η ιδέα είναι ο συνδυασμός του μεγαλύτερου συνόλου καταγραφής x86_64 με το μικρότερο αποτύπωμα μνήμης και cache που προκύπτει από 32-μπιτους δείκτες.
</td>
<td>σε εξέλιξη</td>
</tr>
</tbody>
</table>



<div class="note">
<p>Πολλά από τα παραπάνω ονόματα υπολογιστών και επεξεργαστών είναι εμπορικά σήματα και καταχωρημένα σήματα των κατασκευαστών τους.
</p>
</div>
